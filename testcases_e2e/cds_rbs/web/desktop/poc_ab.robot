*** Settings ***
Resource        ${CURDIR}/../../../../imports/web_imports.robot
Test Setup      common_desktop_keywords.Open desktop web browser
Test Teardown   Run Keywords    CommonWebKeywords.Test Teardown    AND   SeleniumLibrary.Close Browser

*** Test Cases ***
Order product (member) with cart price rule subtotal+fixed discount + T1 redeem and paid via Credit Card
    [Tags]    poc_ab
    Verify Experiment Id From Cookies    ${PLP_BADGE_EXP}    ${PLP_BADGE_EXP_DEFAULT}
    header_common_fragment.Input text on search bar    CDS16044708
    header_common_fragment.Press Enter key on search bar
    Verify 3hr Badge Icon    CDS16044708
