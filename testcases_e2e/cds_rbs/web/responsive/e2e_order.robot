*** Settings ***
Resource        ${CURDIR}/../../../../imports/web_imports.robot
Test Setup      common_responsive_keywords.Open responsive web browser
Test Teardown   Run Keywords    CommonWebKeywords.Test Teardown    AND   SeleniumLibrary.Close Browser

***Variables***
${store}    ${bu}_${language}

*** Test Cases ***
Order product (member) with cart price rule subtotal+fixed discount + T1 redeem and paid via Credit Card
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_00.id}    | ${QAAT_ROBOT_E2E_00.description}  | ${QAAT_ROBOT_E2E_00.type} | ${QAAT_ROBOT_E2E_00.coupon}   | ${QAAT_ROBOT_E2E_00.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_00.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_00.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_00.specific_sku.free.qty}  |
    ...                | ${QAAT_ROBOT_E2E_03.id}    | ${QAAT_ROBOT_E2E_03.description}  | ${QAAT_ROBOT_E2E_03.type} | ${QAAT_ROBOT_E2E_03.coupon}   | ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_03.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_03.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_03.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1002640    e2e7
    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}
    Run Keyword And Ignore Error    customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_06}    ${test_user.password}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait
   
    # 1. tribe: landing and info
    login_responsive_keywords.Login on header by email, password successfully    ${test_user.mobweb_user_06}    ${test_user.password}    ${test_user.information.firstname}
    
    # 2. tribe: discovery
    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    # 3. tribe: consideration
    # No action, validation keyword for consideration

    # 4. tribe: cart and checkout
    # 4.1 cart
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_03.specific_sku.buy.qty}

    # 4.2 shopping bag
    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Apply coupon, verify 'coupon fixed discount' should be applied in cart correctly    ${QAAT_ROBOT_E2E_03.coupon}    ${${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}.price}    ${QAAT_ROBOT_E2E_03.discount}
    shopping_bag_common_page.Click secure checkout button
    # 4.3 checkout: delivery details
    delivery_responsive_keywords.Member, home delivery with default address, standard delivery
    delivery_common_page.Click continue to payment button
    # 4.4 checkout: payment
    payment_responsive_keywords.Redeem point    ${40}    ${t1c_information_01.email}   ${t1c_information_01.password}
    payment_responsive_keywords.Payment, credit card full payment successfully
    # 4.5 thankyou
    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=${discount_amount}    redeem=${5}
    thank_you_common_page.Get order ID, set test variable

    # 5. tribe: post purchase
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${order_id}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${order_id}
    fms_request.Verify order is created successfully in FMS via ESB    ${order_id}
    wms_request.Verify order is created successfully in WMS via ESB    ${order_id}
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait

Order product (member) with cart price rule (fix coupon)+Freebie, paid via 123 bank transfer
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_01.id}    | ${QAAT_ROBOT_E2E_01.description}  | ${QAAT_ROBOT_E2E_01.type} | ${QAAT_ROBOT_E2E_01.coupon}   | ${QAAT_ROBOT_E2E_01.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_01.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_01.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_01.specific_sku.free.qty}  |
    ...                | ${QAAT_ROBOT_E2E_03.id}    | ${QAAT_ROBOT_E2E_03.description}  | ${QAAT_ROBOT_E2E_03.type} | ${QAAT_ROBOT_E2E_03.coupon}   | ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_03.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_03.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_03.specific_sku.free.qty}  |
    ...                | ${QAAT_ROBOT_E2E_05.id}    | ${QAAT_ROBOT_E2E_05.description}  | ${QAAT_ROBOT_E2E_05.type} | ${QAAT_ROBOT_E2E_05.coupon}   | ${QAAT_ROBOT_E2E_05.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_05.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : ${QAAT_ROBOT_E2E_01.specific_sku.buy.sku}
    ...                - *Buy SKU - 2*      : ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Bank transfer
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1002641    e2e3

    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}
    Run Keyword And Ignore Error     customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_03}    ${test_user.password}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait

    login_responsive_keywords.Login on header by email, password successfully    ${test_user.mobweb_user_03}    ${test_user.password}    ${test_user.information.firstname}

    search_responsive_keywords.Search by sku, go to product page on search result page    ${QAAT_ROBOT_E2E_01.specific_sku.buy.sku}    
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${QAAT_ROBOT_E2E_01.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_01.specific_sku.buy.qty}
    
    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_03.specific_sku.buy.qty}
    
    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${QAAT_ROBOT_E2E_01.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Apply coupon, verify 'coupon fixed discount' should be applied in cart correctly    ${QAAT_ROBOT_E2E_03.coupon}    ${${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}.price}    ${QAAT_ROBOT_E2E_03.discount}
    shopping_bag_responsive_keywords.Verify free gift with this item purchase should be display correctly    ${QAAT_ROBOT_E2E_01.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_01.specific_sku.free.sku}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Member, home delivery with default address, standard delivery
    delivery_common_page.Click continue to payment button

    payment_responsive_keywords.Payment, bank transfer

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=(${QAAT_ROBOT_E2E_05.discount}+${discount_amount})
    thank_you_common_page.Get order ID, set test variable

    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait

Order product (Guest) paid via COD
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*          : No
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : Any COD product
    ...                - *Member type*      : Guest
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1007393    e2e4
    
    BuiltIn.Set Test Variable    ${cod_product}    ${${order_specific_data.C1007393.retail_product}}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait

    search_responsive_keywords.Search by sku, go to product page on search result page    ${cod_product.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${cod_product.sku}    ${1}

    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${cod_product.sku}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Guest, home delivery, standard delivery
    delivery_common_page.Click continue to payment button

    payment_responsive_keywords.Payment, cash on delivery

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}
    thank_you_common_page.Get order ID, set test variable

    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait

Order product with mix products (3PL type) + cart price rule (percent discount) + free item, paid via COD + request full tax invoice
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_07.id}    | ${QAAT_ROBOT_E2E_07.description}  | ${QAAT_ROBOT_E2E_07.type} | ${QAAT_ROBOT_E2E_07.coupon}   | ${QAAT_ROBOT_E2E_07.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_07.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_07.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_07.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : ${QAAT_ROBOT_E2E_04.specific_sku.buy.sku}
    ...                - *Buy SKU - 2*      : Any retail product
    ...                - *Buy SKU - 3*      : Any market place product
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1002645    e2e1

    # Test setup, test variable
    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}    
    BuiltIn.Set Test Variable    ${retail_product}    ${${order_specific_data.C1002645.retail_product}}
    BuiltIn.Set Test Variable    ${market_place_product}    ${${order_specific_data.C1002645.market_place_product}}
    Run Keyword And Ignore Error    customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_01}    ${test_user.password}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait

    login_responsive_keywords.Login on header by email, password successfully    ${test_user.mobweb_user_01}    ${test_user.password}    ${test_user.information.firstname}

    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${retail_product.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${retail_product.sku}    ${1}

    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${market_place_product.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${market_place_product.sku}    ${1}

    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${retail_product.sku}    ${market_place_product.sku}
    shopping_bag_responsive_keywords.Apply coupon, verify 'coupon percent discount' should be applied in cart correctly    ${QAAT_ROBOT_E2E_07.coupon}    ${retail_product.price}    ${QAAT_ROBOT_E2E_07.discount}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Member, home delivery with default address, standard delivery
    delivery_responsive_keywords.Request tax invoice with default address
    delivery_common_page.Click continue to payment button

    payment_responsive_keywords.Payment, cash on delivery

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=${discount_amount}
    thank_you_responsive_keywords.Verify full taxt invoice should be displayed correctly    ${test_user.full_tax_invoice.full_address}    ${test_user.full_tax_invoice.tax_id}
    thank_you_common_page.Get order ID, set test variable
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait    

Order product free item + auto cart discount, paid via COD
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_04.id}    | ${QAAT_ROBOT_E2E_04.description}  | ${QAAT_ROBOT_E2E_04.type} | ${QAAT_ROBOT_E2E_04.coupon}   | ${QAAT_ROBOT_E2E_04.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_04.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_04.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_04.specific_sku.free.qty}  |
    ...                | ${QAAT_ROBOT_E2E_05.id}    | ${QAAT_ROBOT_E2E_05.description}  | ${QAAT_ROBOT_E2E_05.type} | ${QAAT_ROBOT_E2E_05.coupon}   | ${QAAT_ROBOT_E2E_05.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_05.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : ${QAAT_ROBOT_E2E_04.specific_sku.buy.sku}
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : Yes
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : COD
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1101565    e2e2

    # Test setup, test variable
    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}
    BuiltIn.Set Test Variable    ${free_product}    ${QAAT_ROBOT_E2E_04}
    Run Keyword And Ignore Error    customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_02}    ${test_user.password}
    cart.Member add skus to cart, return token and cart id    ${test_user.mobweb_user_02}    ${test_user.password}    ${free_product.specific_sku.buy.sku}    ${free_product.specific_sku.buy.qty}    ${store}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait

    login_responsive_keywords.Login on header by email, password successfully    ${test_user.mobweb_user_02}    ${test_user.password}    ${test_user.information.firstname}

    shopping_bag_responsive_keywords.Go to shopping bag page
    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${free_product.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Verify free items with order purchase should be displayed correctly    ${free_product.specific_sku.free.sku}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Member, home delivery with default address, standard delivery
    delivery_common_page.Click continue to payment button

    payment_responsive_keywords.Payment, cash on delivery

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=${QAAT_ROBOT_E2E_05.discount}
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait    

Order product (login fb) with cart price rule subtotal + percent discount, paid via Credit card
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_02.id}    | ${QAAT_ROBOT_E2E_02.description}  | ${QAAT_ROBOT_E2E_02.type} | ${QAAT_ROBOT_E2E_02.coupon}   | ${QAAT_ROBOT_E2E_02.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_02.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_02.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_02.specific_sku.free.qty}  |
    ...                | ${QAAT_ROBOT_E2E_05.id}    | ${QAAT_ROBOT_E2E_05.description}  | ${QAAT_ROBOT_E2E_05.type} | ${QAAT_ROBOT_E2E_05.coupon}   | ${QAAT_ROBOT_E2E_05.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_05.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : ${QAAT_ROBOT_E2E_02.specific_sku.buy.sku}
    ...                - *Buy SKU - 2*      : Any retail product
    ...                - *Buy SKU - 3*      : Any retail product
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Standard shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit Card
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1003410    e2e9

    # Test setup, test variable
    BuiltIn.Set Test Variable    ${test_user}    ${facebook_customer_data_01}
    BuiltIn.Set Test Variable    ${1st_retail_product}    ${${order_specific_data.C1003410.retail_product.item_1}}
    BuiltIn.Set Test Variable    ${2nd_retail_product}    ${${order_specific_data.C1003410.retail_product.item_2}}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait

    login_responsive_keywords.Login via facebook    ${test_user.email}    ${test_user.password}    ${test_user.information.firstname}
    
    shopping_bag_responsive_keywords.Remove all products in shopping cart

    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${QAAT_ROBOT_E2E_02.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${QAAT_ROBOT_E2E_02.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_02.specific_sku.buy.qty}

    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${2nd_retail_product.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${2nd_retail_product.sku}    ${1}

    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${QAAT_ROBOT_E2E_02.specific_sku.buy.sku}    ${2nd_retail_product.sku}
    shopping_bag_responsive_keywords.Apply coupon, verify 'coupon percent discount' should be applied in cart correctly    ${QAAT_ROBOT_E2E_02.coupon}    ${${QAAT_ROBOT_E2E_02.specific_sku.buy.sku}.price}    ${QAAT_ROBOT_E2E_02.discount}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Member, home delivery with default address, standard delivery
    delivery_common_page.Click continue to payment button

    payment_responsive_keywords.Payment, existing credit card full payment successfully

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=(${QAAT_ROBOT_E2E_05.discount}+${discount_amount})
    thank_you_common_page.Get order ID, set test variable
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait    

Order product with cart price rule subtotal+fixed discount, paid via 2C2P Installment
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | Y     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_05.id}    | ${QAAT_ROBOT_E2E_05.description}  | ${QAAT_ROBOT_E2E_05.type} | ${QAAT_ROBOT_E2E_05.coupon}   | ${QAAT_ROBOT_E2E_05.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_05.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_05.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : Any installment product related to rule id ${QAAT_ROBOT_E2E_05.id}
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Express shipping
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Credit card Installment
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1002650    e2e8

    # Test setup, test variable
    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}
    BuiltIn.Set Test Variable    ${installment_product}    ${${order_specific_data.C1002650.retail_product}}
    Run Keyword And Ignore Error    customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_07}    ${test_user.password}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait

    login_responsive_keywords.Login on header by email, password successfully   ${test_user.mobweb_user_07}    ${test_user.password}    ${test_user.information.firstname}
    
    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${installment_product.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${installment_product.sku}    ${1}

    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${installment_product.sku}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Member, home delivery with default address, express delivery
    delivery_common_page.Click continue to payment button

    payment_responsive_keywords.Payment, credit card installment successfully    ${order_specific_data.C1002650.installment_plan.bank}    ${order_specific_data.C1002650.installment_plan.plan}

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=${QAAT_ROBOT_E2E_05.discount}    shipping=${150}
    thank_you_common_page.Get order ID, set test variable
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait    

Order product with cart price rule (fix coupon), paid via pay at store
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=                  | =Description=                     | =Type=                    | =Coupon=                      | =Buy specific SKU=                                                                            | =Free specific SKU=                                                                            |
    ...                | ${QAAT_ROBOT_E2E_03.id}    | ${QAAT_ROBOT_E2E_03.description}  | ${QAAT_ROBOT_E2E_03.type} | ${QAAT_ROBOT_E2E_03.coupon}   | ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}, Qty :  ${QAAT_ROBOT_E2E_03.specific_sku.buy.qty}   | ${QAAT_ROBOT_E2E_03.specific_sku.free.sku}, Qty :  ${QAAT_ROBOT_E2E_03.specific_sku.free.qty}  |
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Click & Collect
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : No
    ...                - *Payment type*     : Pay at store
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1002653    e2e5

    # Test setup, test variable
    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}
    Run Keyword And Ignore Error    customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_04}    ${test_user.password}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait
    
    login_responsive_keywords.Login on header by email, password successfully   ${test_user.mobweb_user_04}    ${test_user.password}    ${test_user.information.firstname}

    search_responsive_keywords.Search by sku, go to product page on search result page    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}    ${QAAT_ROBOT_E2E_03.specific_sku.buy.qty}

    shopping_bag_responsive_keywords.Verify total price with quantity, summarize multiple products    ${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}
    shopping_bag_responsive_keywords.Apply coupon, verify 'coupon fixed discount' should be applied in cart correctly    ${QAAT_ROBOT_E2E_03.coupon}    ${${QAAT_ROBOT_E2E_03.specific_sku.buy.sku}.price}    ${QAAT_ROBOT_E2E_03.discount}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Memeber, click and collect, standard pickup    ${order_specific_data.C1002653.store}
    delivery_common_page.Click continue to payment button
    
    payment_responsive_keywords.Payment, pay at store

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${total_price_with_quantity}    discount=${discount_amount}
    thank_you_common_page.Get order ID, set test variable
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait    

Order product, paid via T1 Full redeem (Zero pay)
    [Documentation]    - *Support BU, ENV, LANGUAGE*
    ...                | =BU=   | =UAT= | =SIT= | =PROD=    | =TH=  | =EN=  |
    ...                | CDS    | N     | Y     | N         | Y     | N     |
    ...                | RBS    | N     | Y     | N         | Y     | N     |
    ...                - *Rule ID*    : The expected cart price rule, Should be able to apply.
    ...                | =Rule ID=          : No
    ...                - *BU*               : ${BU.lower()}
    ...                - *ENV*              : ${ENV.lower()}
    ...                - *Buy SKU - 1*      : Any retail product, price should lower than 50B
    ...                - *Member type*      : Membership
    ...                - *Delivery type*    : Click & Collect
    ...                - *Full tax invoice* : No
    ...                - *Redeem point*     : Yes
    ...                - *Payment type*     : Full redeem point
    [Tags]    cds_web_regression    rbs_web_regression    testrailid:C1025703    e2e6

    # Test setup, test variable
    BuiltIn.Set Test Variable    ${test_user}    ${e2e_customer_data_03}
    BuiltIn.Set Test Variable    ${retail_product}    ${${order_specific_data.C1025703.retail_product}}
    Run Keyword And Ignore Error    customer_details.Remove customer cart and generate new cart    ${test_user.mobweb_user_05}    ${test_user.password}
    Run Keyword And Ignore Error    SeleniumLibrary.Disable Testability Automatic Wait
    
    login_responsive_keywords.Login on header by email, password successfully   ${test_user.mobweb_user_05}    ${test_user.password}    ${test_user.information.firstname}

    search_responsive_keywords.Search by sku, go to product page on search suggestion    ${retail_product.sku}
    shopping_bag_responsive_keywords.Add product to cart, add qty on shopping bag page    ${retail_product.sku}    ${1}

    shopping_bag_common_page.Click secure checkout button

    delivery_responsive_keywords.Memeber, click and collect, standard pickup    ${order_specific_data.C1025703.store}
    delivery_common_page.Click continue to payment button
    
    payment_responsive_keywords.Payment, Full redeem point    ${t1c_information_01.email}    ${t1c_information_01.password}

    thank_you_responsive_keywords.Verify place order successfully, order total should be displayed correctly    ${0}
    thank_you_common_page.Get order ID, set test variable
    Run Keyword And Ignore Error    SeleniumLibrary.Enable Testability Automatic Wait    