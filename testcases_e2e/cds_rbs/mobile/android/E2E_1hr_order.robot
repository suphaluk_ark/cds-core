*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../imports/mobile_imports.robot

*** Test Cases ***
Verify the order create successfully on Pickingtool when checkout by 2HR pickup
    [Documentation]    - *Member Type*              : Membership
    ...                - *Payment Type*             : Pay at store
    ...                - *Delivery Type*            : 2hr pickup
    ...                - *Product from source*      : Pickingtool
    [Tags]    high    testrailid:C1019748    e2e    E2E_1hr_order
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.e2e_1hr_order}
    search_filter_android_keywords.Search product by product sku    ${CDS13961466.sku}
    product_pdp_android_keywords.Add product to cart, continue to checkout details
    delivery_android_keywords.Membership, delivery successfully by 1 hour pickup, self pickup
    payment_android_keywords.Payment successfully by pay at store
    ${order_number}    checkout_completed_common_page.Get non-split order number
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${order_number}
    picking_tool_request.Retry until order is created successfully in Pickingtool via ESB    ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application