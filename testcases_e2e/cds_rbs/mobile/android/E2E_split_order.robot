*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../imports/mobile_imports.robot

*** Test Cases ***
Verify the split order create successfully on Pickingtool, WMS ,and FMS
    [Documentation]    - *Member Type*              : Member
    ...                - *Payment Type*             : Credit card full payment
    ...                - *Delivery Type*            : 1hr pickup + standard shipping
    ...                - *Product from source*      : Pickingtool - CDS13961466 + WMS - CDS94322835
    [Tags]    high    testrailid:C1013369    E2E_split_order
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Membership    ${personal_android_app.e2e_split_order}
    search_filter_android_keywords.Search product by product sku    ${CDS94322835.sku}
    product_pdp_android_keywords.Add product to cart, and back to previous page
    search_filter_android_keywords.Search product by product sku    ${CDS13961466.sku}    home_page=${False}
    product_pdp_android_keywords.Add product to cart, continue to checkout details
    delivery_android_keywords.Membership delivery successfully 1hr and standard delivery
    payment_android_keywords.Payment successfully with saved credit card    process_otp=yes
    ${std_order_number}    checkout_completed_android_page.Get order number that supports 'Standard Delivery'
    ${1hr_order_number}    checkout_completed_common_page.Get order number that supports '1 Hour Pickup'
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${1hr_order_number}
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${std_order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${1hr_order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${std_order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${1hr_order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${std_order_number}
    picking_tool_request.Retry until order is created successfully in Pickingtool via ESB     ${1hr_order_number}
    wms_request.Verify order is created successfully in WMS via ESB    ${std_order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
