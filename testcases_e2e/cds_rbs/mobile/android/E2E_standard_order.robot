*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../imports/mobile_imports.robot

*** Test Cases ***
Verify the order create successfully on WMS when checkout by standard delivery
    [Documentation]    - *Member Type*              : Guest
    ...                - *Payment Type*             : COD
    ...                - *Delivery Type*            : Standard shipping
    ...                - *Product from source*      : WMS
    [Tags]    high    testrailid:C1019749    E2E_standard_order
    [Setup]    common_android_mobile_app_keywords.android Setup CDS Application by Guest
    search_filter_android_keywords.Search product by product sku    ${CDS94322835.sku}
    product_pdp_android_keywords.Add product to cart, continue to checkout details
    delivery_android_keywords.Guest, delivery successfully by shipping to address, standard delivery  ${guest_data_01.information.shipping_post_code}
    payment_android_keywords.Payment successfully by cash on delivery
    ${order_number}    checkout_completed_common_page.Get non-split order number
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${order_number}
    wms_request.Verify order is created successfully in WMS via ESB    ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application