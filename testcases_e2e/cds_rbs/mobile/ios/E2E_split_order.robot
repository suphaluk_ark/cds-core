*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../imports/mobile_imports.robot

*** Test Cases ***
Verify the split order create successfully on Pickingtool, WMS ,and FMS
    [Documentation]    - *Member Type*              : Member
    ...                - *Payment Type*             : Credit card full payment
    ...                - *Delivery Type*            : 1hr pickup + standard shipping
    ...                - *Product from source*      : Pickingtool - CDS12863273 + WMS - CDS9465749
    [Tags]    high    testrailid:C1013369    e2e    E2E-split-order
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Membership    ${personal_ios_app.e2e_split_order}
    search_filter_ios_keywords.Search product by product sku    ${CDS94322835.sku}
    product_pdp_ios_keywords.Add product to cart, and back to previous page
    search_filter_ios_keywords.Search product by product sku    ${CDS13961466.sku}    home_page=${False}
    product_pdp_ios_keywords.Add product to cart, continue to checkout details
    delivery_ios_keywords.Membership delivery successfully 1hr and standard delivery
    payment_ios_keywords.Payment successfully with saved credit card    process_otp=yes
    ${std_order_number}    checkout_completed_ios_page.Get order number that supports 'Standard Delivery'
    ${1hr_order_number}    checkout_completed_common_page.Get order number that supports '1 Hour Pickup'
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${1hr_order_number}
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${std_order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${1hr_order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${std_order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${1hr_order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${std_order_number}
    picking_tool_request.Retry until order is created successfully in Pickingtool via ESB     ${1hr_order_number}
    wms_request.Verify order is created successfully in WMS via ESB    ${std_order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application
