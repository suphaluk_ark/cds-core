*** Settings ***
Suite Setup    common_mobile_app_keywords.Open CDS Application with additional caps
Suite Teardown    common_mobile_app_keywords.Close CDS Application

Resource    ${CURDIR}/../../../../imports/mobile_imports.robot

*** Test Cases ***
Verify the order create successfully on WMS when checkout by standard delivery
    [Documentation]    - *Member Type*              : Guest
    ...                - *Payment Type*             : COD
    ...                - *Delivery Type*            : Standard shipping
    ...                - *Product from source*      : WMS
    [Tags]    high    testrailid:C1019749    e2e    E2E-standard-order
    [Setup]    common_ios_mobile_app_keywords.ios Setup CDS Application by Guest
    search_filter_ios_keywords.Search product by product sku    ${CDS94322835.sku}
    product_pdp_ios_keywords.Add product to cart, continue to checkout details
    delivery_ios_keywords.Guest, delivery successfully by shipping to address, standard delivery  ${guest_data_01.information.shipping_post_code}
    payment_ios_keywords.Payment successfully by cash on delivery
    ${order_number}    checkout_completed_common_page.Get non-split order number
    mcom_keywords.Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM    ${order_number}
    mdc_keywords.Verify order status should be "Processing/ READYTOSHIP" on MDC    ${order_number}
    fms_request.Verify order is created successfully in FMS via ESB    ${order_number}
    wms_request.Verify order is created successfully in WMS via ESB    ${order_number}
    [Teardown]    common_mobile_app_keywords.Teardown CDS Application