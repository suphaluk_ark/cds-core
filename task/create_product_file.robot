*** Settings ***
Resource    ${CURDIR}/../imports/task_imports.robot
Documentation    Reqired arguments: *BU, ENV, IS_TRIBE*
...    - default value:  cds-core/resources/test_configs.yaml
...    - *false*:        for support cds-core repository
...    - *true* :        for support tribe repository
...    Example command run task: robot -v BU:CDS -v ENV:SIT -v IS_TRIBE:True create_product_file.robot

*** Variables ***
${products_file_path_cds_core}      ${CURDIR}${/}..${/}testdata${/}${BU.lower()}${/}${ENV.lower()}${/}products_data.yaml
${products_file_path_tribe}         ${CURDIR}${/}..${/}..${/}testdata${/}${BU.lower()}${/}${ENV.lower()}${/}products_data.yaml

*** Keywords ***
Task - Set Product Special Price To Price
    [Arguments]    ${sku_list}
    FOR    ${sku}    IN    @{sku_list}
        ${result}=    Set Variable    ${${sku}}
        ${sale_price}=    Run Keyword And Ignore Error    Set Variable    ${${sku}}[special_price]

        ${sale_price}=    Set Variable If    'FAIL' == '${sale_price[0]}'    ${${sku}}[price]
        ...    'PASS' == '${sale_price[0]}' and ${${sku}}[special_price] > ${${sku}}[price]    ${${sku}}[price]    # 21 DEC 2020 Added logic for one project - price service
        ...    ${${sku}}[special_price]

        Set To Dictionary    ${result}    ${attribute_field}[price]    ${sale_price}
        ${result}=    Convert To Dictionary    ${result}
        FileOperation.Write Product Data To File    ${attribute_field}[sku]    ${products_data_file_path}    ${result}
    END

*** Tasks ***
create product data file and get customer profile
    ${products_data_file_path}    Set Variable If    ${IS_TRIBE}    ${products_file_path_tribe}    ${products_file_path_cds_core}
    Import Variables    ${products_data_file_path}
    Set Test Variable   ${products_data_file_path}
    
    set_product_file.Set Product Dictionary Data To File    ${product_sku_list}    ${products_data_file_path}    ${store}
    Task - Set Product Special Price To Price    ${product_sku_list}