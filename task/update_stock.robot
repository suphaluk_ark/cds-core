*** Setting ***
Resource    ${CURDIR}/../imports/task_imports.robot
Documentation    Reqired arguments: *BU, ENV, IS_TRIBE*
...    - default value:  cds-core/resources/test_configs.yaml
...    - *false*:        for support cds-core repository
...    - *true* :        for support tribe repository
...    Example command run task: robot -v BU:CDS -v ENV:SIT -v IS_TRIBE:True update_stock.robot

*** Variables ***
${products_stock_file_path_cds_core}      ${CURDIR}${/}..${/}testdata${/}${BU.lower()}${/}${ENV.lower()}${/}products_stock.yaml
${products_stock_file_path_tribe}         ${CURDIR}${/}..${/}..${/}testdata${/}${BU.lower()}${/}${ENV.lower()}${/}products_stock.yaml

*** keywords ***
Create oauth token with client_id and client_secret
    ${body}=    Create Dictionary    grant_type=client_credentials    client_id=${mcom_client_id}    client_secret=${mcom_client_secret}
    ${headers}=    Create Dictionary    Content-Type=application/json
    ${response}=    REST.Post    endpoint=${mcom_oauth_root}/oauth/token    body=${body}    headers=${headers}
    REST.Integer    response status    201
    
    ${access_token_cds}=    JSONLibrary.Get Value From Json    ${response}    $..access_token
    Set Test Variable    ${access_token_cds}    ${access_token_cds}[0]
    [Return]     ${response}

Update Stock MCOM
    [Arguments]    ${source_id}    ${qty}    ${sku}
    ${update_stock_body}=    JSONLibrary.Load JSON From File    ${CURDIR}/resources/update_stock.json
    JSONLibrary.Update Value To Json    ${update_stock_body}    $..source_id    ${source_id}
    JSONLibrary.Update Value To Json    ${update_stock_body}    $..quantity    ${qty}
    JSONLibrary.Update Value To Json    ${update_stock_body}    $..sku    ${sku}

    ${headers}=    Create Dictionary    Authorization=Bearer ${access_token_cds}    Content-Type=application/json
    ${response}=    REST.Post    endpoint=${mcom_api_root_update_stock}    body=${update_stock_body}    headers=${headers}
    REST.Integer    response status    200

*** Task ***
UPDATE STOCK
    ${products_stock_file_path}    Set Variable If    ${IS_TRIBE}    ${products_stock_file_path_tribe}    ${products_stock_file_path_cds_core}
    Import Variables    ${products_stock_file_path}

    Create oauth token with client_id and client_secret
    FOR    ${sku}    IN    @{update_products_stock}
        ${source_id}    Set Variable    ${update_products_stock}[${sku}][source]
        ${qty}    Set Variable    ${update_products_stock}[${sku}][qty]
        Update Stock MCOM    ${source_id}    ${qty}    ${sku}
        ${is_rbs_stock}=    Run Keyword And Return Status    Should Start With    ${source_id}    RBS
        Continue For Loop If    ${is_rbs_stock}
        Run Keyword If    '${source_id}' != 'CDS_SO_10138'    Update Stock MCOM    CDS_SO_10138    ${0}    ${sku}
    END
    Delete All Sessions