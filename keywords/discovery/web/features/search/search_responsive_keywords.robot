*** Keywords ***
Search by sku, go to product page on search result page
    [Arguments]    ${product_sku}
    Run Keyword And Ignore Error    header_common_fragment.Click x button on search bar
    header_common_fragment.Input text on search bar    ${product_sku}
    header_common_fragment.Press Enter key on search bar
    product_plp_common_fragment.Click product by product sku    ${product_sku}

Search by sku, go to product page on search suggestion
    [Arguments]    ${product_sku}
    Run Keyword And Ignore Error    header_common_fragment.Click x button on search bar
    header_common_fragment.Input text on search bar    ${product_sku}
    header_common_fragment.Click product on search suggestion by product sku    ${product_sku}

Search by brand name, go to plp page
    [Arguments]    ${brand_name}
    Run Keyword And Ignore Error    header_common_fragment.Click x button on search bar
    header_common_fragment.Input text on search bar    ${brand_name}
    header_common_fragment.Press Enter key on search bar

Search product for go to PLP page
    [Arguments]    ${text}
    Run Keyword And Ignore Error    header_common_fragment.Click x button on search bar
    header_common_fragment.Input text on search bar    ${text}
    header_common_fragment.Press Enter key on search bar