*** Keywords ***
Verify product plp ui - 'product brand name and sorting price low to high' should be displayed correctly via api
    [Arguments]    ${brand_name}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Sort 'Price Low-High'    ${brand_name}    ${language}    ${BU}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_product_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].brand_name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    product_plp_common_fragment.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        product_plp_common_fragment.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        product_plp_common_fragment.Verify 'product brand name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_brand_name}[${index}]
        product_plp_common_fragment.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product price label' should be displayed   ${response}    ${result_list_of_sku}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify product plp ui - 'product price label' should be displayed
    [Arguments]    ${response}    ${product_sku}
    ${type_id}=        JSONLibrary.Get Value From Json    ${response}     $..products[?(@.sku=='${product_sku}')].type_id
    ${type_id}=    Set Variable    ${type_id}[0]
    ${product_sku_get_price}=    Run Keyword If    '${type_id}'=='simple'    Set Variable   ${product_sku}
    ...     ELSE IF    '${type_id}'=='configurable'    product_plp_common_fragment.Get display configurable's simple product by configurable product sku    ${product_sku}
    ...     ELSE        Fail    Incorrect 'type_id'

    ${dict_product_sku_get_price}=    JSONLibrary.Get Value From Json    ${response}     $..*[?(@.sku=='${product_sku_get_price}')]
    ${dict_product_price}=    product_api.Get product price data by product sku        ${dict_product_sku_get_price}[0]

    Run Keyword If    ${dict_product_price}[is_product_no_discount]    verify_product_plp_responsive_keywords.Verify product plp ui - 'product price no discount' should be displayed correctly    ${product_sku}    ${dict_product_price}
    ...    ELSE    verify_product_plp_responsive_keywords.Verify product plp ui - 'product price with discount' should be displayed correctly    ${product_sku}    ${dict_product_price}

Verify product plp ui - 'product price with discount' should be displayed correctly
    [Arguments]    ${product_sku}    ${dict_product_price}
    product_plp_common_fragment.Verify 'product sell price' should be displayed correctly    ${product_sku}    ${dict_product_price}[special_price]
    Run Keyword If    '${TEST_PLATFORM}' == 'web'    product_plp_common_fragment.Verify 'product before discount price' should be displayed correctly    ${product_sku}    ${dict_product_price}[price]
    product_plp_common_fragment.Verify 'product save price' should be displayed correctly    ${product_sku}    ${dict_product_price}[evaluate_save_price]
    product_plp_common_fragment.Verify 'product save price percent' should be displayed    ${product_sku}    ${dict_product_price}[evaluate_percent_discount]

Verify product plp ui - 'product tag' should be displayed correctly
    [Arguments]    ${response}    ${product_sku}
    ${type_id}=        JSONLibrary.Get Value From Json    ${response}     $..products[?(@.sku=='${product_sku}')].type_id
    ${type_id}=    Set Variable    ${type_id}[0]
    ${product_sku_get_tag}=    Run Keyword If    '${type_id}'=='simple'    Set Variable   ${product_sku}
    ...     ELSE IF    '${type_id}'=='configurable'    product_plp_common_fragment.Get display configurable's simple product by configurable product sku    ${product_sku}
    ...     ELSE        Fail    Incorrect 'type_id'

    ${dict_product}=    JSONLibrary.Get Value From Json    ${response}     $..*[?(@.sku=='${product_sku_get_tag}')]
    ${dict_product}=    Set Variable    ${dict_product}[0]

    ${promo_tag}=    Set Variable    ${dict_product}[promo_tag]
    ${new_tag}=    Set Variable    ${dict_product}[new_tag]
    Run Keyword If    '${promo_tag}'!='${None}' and '${promo_tag}'!='${SPACE}'    Run Keywords    product_plp_common_fragment.Verify 'product promo tag' should be displayed correctly    ${product_sku}    ${promo_tag}
    ...    AND    product_plp_common_fragment.Verify 'product new tag' should not be displayed    ${product_sku}
    ...   ELSE IF    ${new_tag} == ${1}   product_plp_common_fragment.Verify 'product new tag' should be displayed    ${product_sku}
    ...   ELSE     product_plp_common_fragment.Verify 'product new tag' should not be displayed    ${product_sku}

    ${only_at_tag}=    Set Variable    ${dict_product}[only_at_tag]
    ${online_exclusive_tag}=    Set Variable    ${dict_product}[online_exclusive_tag]
    Run Keyword If    ${only_at_tag} == ${1}   Run Keywords   product_plp_common_fragment.Verify 'product only at tag' should be displayed    ${product_sku}
    ...   AND    product_plp_common_fragment.Verify 'product online exclusive tag' should not be displayed    ${product_sku_get_tag}
    ...   ELSE IF   ${online_exclusive_tag} == ${1}   Run Keywords    product_plp_common_fragment.Verify 'product online exclusive tag' should be displayed    ${product_sku}
    ...   AND    product_plp_common_fragment.Verify 'product only at tag' should not be displayed    ${product_sku}
    ...   ELSE    Run Keywords    product_plp_common_fragment.Verify 'product online exclusive tag' should not be displayed    ${product_sku}
    ...   AND    product_plp_common_fragment.Verify 'product only at tag' should not be displayed    ${product_sku}

Verify product plp ui - 'product price no discount' should be displayed correctly
    [Arguments]    ${product_sku}    ${dict_product_price}
    product_plp_common_fragment.Verify 'product sell price' should be displayed correctly    ${product_sku}    ${dict_product_price}[price]
    product_plp_common_fragment.Verify 'product save price' should not be displayed    ${product_sku}
    product_plp_common_fragment.Verify 'product save price percent' should not be displayed    ${product_sku}

Verify product plp ui - 'product keyword' should be displayed correctly via api
    [Arguments]    ${keyword}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    filter_sort_plp_common_fragment.Verify 'search keyword label' should be displayed correctly    ${keyword.lower()}
    product_plp_common_fragment.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        product_plp_common_fragment.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        product_plp_common_fragment.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product price label' should be displayed   ${response}   ${result_list_of_sku}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify product plp ui - 'product keyword with filter color' should be displayed correctly via api
    [Arguments]    ${keyword}    ${color}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Keyword And Filter Color    ${keyword}    ${color}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    filter_sort_plp_common_fragment.Verify 'search keyword label' should be displayed correctly    ${keyword.lower()}
    product_plp_common_fragment.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        product_plp_common_fragment.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        product_plp_common_fragment.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product price label' should be displayed   ${response}   ${result_list_of_sku}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END

Verify product plp ui - 'product brand name' should be displayed correctly via api
    [Arguments]    ${brand_name}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${brand_name}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${result_list_of_product_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].name
    ${result_list_of_product_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].brand_name
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    product_plp_common_fragment.Verify 'product found label' should be displayed correctly    ${result_total_search_product}
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        product_plp_common_fragment.Verify 'product img' should be displayed correctly    ${result_list_of_sku}[${index}]
        product_plp_common_fragment.Verify 'product brand name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_brand_name}[${index}]
        product_plp_common_fragment.Verify 'product name label' should be displayed correctly   ${result_list_of_sku}[${index}]    ${result_list_of_product_name}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product price label' should be displayed   ${response}    ${result_list_of_sku}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product tag' should be displayed correctly   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
    
Verify product plp ui - 'product brand name sort by discount high-low' should be displayed with discount correctly via api
    [Arguments]    ${brand_name}    ${range_of_product}=${5}
    ${response}=    search_falcon.Api Search Product By Brand Name And Filter Sort 'Discount High-Low'    ${brand_name}    ${language}    ${BU.lower()}    ${range_of_product}
    ${result_list_of_sku}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].sku
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..total
    ${result_total_search_product}=    Set Variable    ${result_total_search_product}[0]
    ${range_of_product}=    Set Variable If    ${result_total_search_product} < ${range_of_product}    ${result_total_search_product}   ${range_of_product}

    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    FOR    ${index}    IN RANGE    0    ${range_of_product}
        product_plp_common_fragment.Verify 'product with discount' should be displayed    ${result_list_of_sku}[${index}]
        verify_product_plp_responsive_keywords.Verify product plp ui - 'product price label' should be displayed   ${response}   ${result_list_of_sku}[${index}]
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
