*** Variables ***
&{dict_normal_brand_page}
...    btn_filter_arrow_down=xpath=(//*[@id='sel-formFilter-sort'])[1]/img[contains(@src,'arrow-down')]
...    opt_sorter=xpath=(//*[@id='sel-formFilter-sort'])[1]/following-sibling::*//*[text()='{$text}']
...    brand_name=xpath=//a[@to='/{$brand_name}']
...    lbl_brand_name=xpath=//a[text()='{$brand_name}']
...    lbl_shop_by_brands=css=a[to='shopbybrand']