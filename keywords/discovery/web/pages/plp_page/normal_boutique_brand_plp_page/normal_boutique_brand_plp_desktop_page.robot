*** Variables ***
&{dict_normal_brand_page}
...    btn_filter_arrow_down=xpath=(//*[@id='sel-formFilter-sort']/img[@src='/icons/arrow-down.svg'])[2]
...    opt_sorter=xpath=(//div[@id='sel-formFilter-sort']/following-sibling::div//span[text()='{$text}'])[2]
...    brand_name=xpath=//a[@to='/{$brand_name}']
...    lbl_brand_name=xpath=//a[text()='{$brand_name}']
...    lbl_shop_by_brands=css=a[to='shopbybrand']