*** Keywords ***
Open Shop By Brand page via URL
    [Arguments]    ${brand_path}
    Open web browser with specific URL path    ${brand_path}

Go to Shop By Brand page
    Run Keyword If    '${TEST_PLATFORM}'=='${common_test_platform.responsive}'    Click on hamburger menu
    home_page_web_keywords.Click on brand

Click on sorter arrow-down button
    CommonWebKeywords.Click Element    ${dict_normal_brand_page}[btn_filter_arrow_down]

Select an option from sorter
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${dict_normal_brand_page}[opt_sorter]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}

Select sort price low-high
    normal_boutique_brand_plp_common_page.Click on sorter arrow-down button
    normal_boutique_brand_plp_common_page.Select an option from sorter  ${plp_dto.sorter.lbl_price_low_high}

Click on brand
    [Arguments]    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_normal_brand_page}[brand_name]    $brand_name=${brand_name.lower()}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify product in What's new section is shown correctly
    [Arguments]    ${brand_name}
    ${brand_name}=    Convert To Upper Case    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_normal_brand_page}[lbl_brand_name]    $brand_name=${brand_name}
    Scroll down to fetch data until element is contained in page    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click on shop by brands category
    CommonWebKeywords.Click Element    ${dict_normal_brand_page}[lbl_shop_by_brands]

Click on Shop All Product link
    [Arguments]    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_standard_brand_page}[lnk_shop_all_product]    $brand_name=${brand_name}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify correct brand name on PLP
    [Arguments]    ${brand_name}
    ${brand_name}=    Convert To Upper Case    ${brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_standard_brand_page}[lbl_brand_name]    $brand_name=${brand_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}