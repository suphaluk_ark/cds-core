*** Variables ***
&{dictHomePage}    
...    img_user_profile=android=UiSelector().resourceIdMatches(".*id/imageViewProfile$")
...    lbl_product_name=android=UiSelector().text("{product_name}")
...    img_backpage=android=UiSelector().description("${search_page_dto.btn_back}")
...    img_stores=android=UiSelector().description("Stores")

*** Keywords ***
Click icon back homepage
    CommonMobileKeywords.Click Element    ${dictHomePage}[img_backpage]    timeout=${GLOBALTIMEOUT}