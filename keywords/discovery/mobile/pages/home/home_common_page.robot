*** Keywords ***
Click user img
    CommonMobileKeywords.Click Element    ${dictHomePage}[img_user_profile]    timeout=${GLOBALTIMEOUT}

Click icon stores
    CommonMobileKeywords.Click Element    ${dictHomePage}[img_stores]    timeout=${GLOBALTIMEOUT}

Verify that home screen is shown
    AppiumLibrary.Wait Until Element Is Visible    ${dictHomePage}[img_user_profile]
