*** Variables ***
&{dictHomePage}
...    img_user_profile=chain=**/XCUIElementTypeOther[$name=="ic search"$]/XCUIElementTypeOther/XCUIElementTypeButton[1]
...    lbl_product_name=chain=**/XCUIElementTypeStaticText[`name == "{product_name}"`]
...    btn_cancel=chain=**/XCUIElementTypeButton[`name == 'search_cancel_button'`]

*** Keywords ***
Click Cancel button on search box
    CommonMobileKeywords.Click Element    ${dictHomePage}[btn_cancel]    timeout=${GLOBALTIMEOUT}