*** Keywords ***
Verify the delete icon should be displayed on search box
    AppiumLibrary.Wait Until Element Is Visible    ${dict_search_product_page}[icon_delete]

Verify message displays correctly at not found result page
    CommonMobileKeywords.Verify Elements Are Visible    ${dict_search_product_page}[msg_not_found_anything]    ${dict_search_product_page}[msg_chat_with_assistant]    ${dict_search_product_page}[btn_chat_now]

Click icon delete clear text
    CommonMobileKeywords.Click Element    ${dict_search_product_page}[icon_delete]

Click search box
    CommonMobileKeywords.Click Element    ${dict_search_product_page}[txt_search]

Input text into search box
    [Arguments]    ${sku}
    CommonMobileKeywords.Verify Elements Are Visible    ${dict_search_product_page}[txt_search_after_click]
    AppiumLibrary.Clear Text    ${dict_search_product_page}[txt_search_after_click]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dict_search_product_page}[txt_search_after_click]    ${sku}

Click icon search
    CommonMobileKeywords.Click Element    ${dict_search_product_page}[btn_search_from_result_page]