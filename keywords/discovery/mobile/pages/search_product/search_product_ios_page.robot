*** Variables ***
&{dict_search_product_page}
...    icon_delete=chain=**/XCUIElementTypeButton[`name == "Clear text"`]
...    msg_not_found_anything=chain=**/XCUIElementTypeStaticText[`name == "${no_result_found_page_dto.msg_not_find_anything}"`]
...    msg_chat_with_assistant=chain=**/XCUIElementTypeStaticText[`name CONTAINS[cd] "${no_result_found_page_dto.msg_chat_with_assistant}"`]
...    btn_chat_now=chain=**/XCUIElementTypeButton[`name CONTAINS[cd] "${no_result_found_page_dto.btn_chat_now}"`]
...    txt_search=chain=**/XCUIElementTypeTextField[`value BEGINSWITH "${search_page_dto.txt_search}"`]
...    txt_search_after_click=chain=**/XCUIElementTypeTextField[`label == "search_text_field"`]
...    btn_search_from_result_page=chain=**/XCUIElementTypeButton[` name == "ic search" `]