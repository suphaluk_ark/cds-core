*** Variables ***
&{dict_search_product_page}
...    icon_delete=android=UiSelector().resourceIdMatches(".*id/search_close_btn$")
...    msg_not_found_anything=android=UiSelector().text("${no_result_found_page_dto.msg_not_find_anything}")
...    msg_chat_with_assistant=android=UiSelector().text("${no_result_found_page_dto.msg_chat_with_assistant}")
...    btn_chat_now=android=UiSelector().resourceIdMatches(".*id/buttonChat$")
...    txt_search=android=UiSelector().resourceIdMatches(".*id/buttonSearch$")
...    txt_search_after_click=android=UiSelector().resourceIdMatches(".*id/search_src_text$")
...    btn_search_from_result_page=android=UiSelector().resourceIdMatches(".*id/search_src_text$")
