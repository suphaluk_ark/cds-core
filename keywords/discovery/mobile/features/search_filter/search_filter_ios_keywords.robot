*** Keywords ***
Verify result of search API when filtering by delivery options
    [Arguments]    ${keyword}    ${selected_shipping_method}    ${range_of_product}=${30}
    ${response}=    app_search_falcon.Api Search Product By Keyword and Filter By Delivery Option    "${keyword}"    ${range_of_product}
    ${result_total_search_product}=    JSONLibrary.Get Value From Json    ${response}    $..totalCount
    ${result_total_search_product}=     Convert JSON To String    ${result_total_search_product}[0]
    ${result_list_of_shipping_method}=    JSONLibrary.Get Value From Json    ${response}    $..products[*].shippingMethod
    ${result}=    Create List  
    FOR    ${shipping_method}    IN    @{result_list_of_shipping_method}
        ${is_status}=    Run Keyword And Return Status   Should Be True    "${selected_shipping_method}" in ${shipping_method}
        Append To List    ${result}    ${is_status}
    END
    List Should Not Contain Value    ${result}    ${False}
    Set Test Variable     ${total_item}    ${result_total_search_product}

Verify the total item on UI displays correctly according to API result
    [Arguments]     ${total_item}    ${Keyword}    ${filter_type}    ${selected_filter_option}
    search_filter_ios_keywords.Search product by Keyword    ${Keyword}
    product_plp_common_page.Verify the total items label is visible
    search_filter_ios_keywords.Apply filter    ${filter_type}    ${selected_filter_option}
    product_plp_common_page.Verify the total items displayed    ${total_item}
    ${actual_total}=    product_plp_common_page.Get text total items
    Should Be True    "${total_item}" in "${actual_total}"

Search product by Keyword
    [Arguments]    ${Keyword}
    search_product_common_page.Input text into search box    ${Keyword}
    common_ios_mobile_app_keywords.Click Search key on the virtual keyboard

Apply filter
    [Arguments]    ${filter_type}    ${selected_filter_option}
    product_plp_common_page.Click on filter icon
    product_plp_common_page.Select filter type    ${filter_type}
    product_plp_common_page.Select filter option    ${selected_filter_option}
    product_plp_common_page.Click apply filter button

Search product by product sku
    [Arguments]    ${sku}    ${home_page}=${True}
    Run Keyword If    ${home_page}    search_product_common_page.Click search box
    ...    ELSE    search_product_common_page.Click icon search
    search_product_common_page.Input text into search box     ${sku}
    common_ios_mobile_app_keywords.Click Search key on the virtual keyboard
    product_plp_common_page.Verify the total items label is visible
    product_plp_common_page.Click on the product name    ${${sku}}[name]

Retry search product by product sku
    [Arguments]    ${sku}
    BuiltIn.Wait Until Keyword Succeeds    1 x    1 sec    BuiltIn.Run Keywords
    ...    search_product_common_page.Click search box
    ...    AND    search_product_common_page.Input text into search box     ${sku}
    ...    AND    common_ios_mobile_app_keywords.Click Search key on the virtual keyboard
    ...    AND    product_plp_common_page.Verify the total items label is visible
    ...    AND    product_plp_common_page.Click on the product name    ${${sku}}[name]