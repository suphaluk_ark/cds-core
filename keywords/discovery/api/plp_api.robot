*** Keywords ***
Get random product by category id
    [Documentation]     *Arguments*
    ...                 - category_id
    ...                 - product_type : simple, configurable
    ...                 *Return:* 
    ...                 - data dictionary of first configurable product data by category id
    ...                 - _*fail*_ if has no configurable product data by category id.
    [Arguments]    ${category_id}    ${product_type}
    ${response}=    search_falcon.Api Search Product By Category Id    ${category_id}    ${language}    ${BU}     ${50}
    ${list_product}=    Run Keyword If    '${product_type}' == 'configurable'    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.type_id=='configurable')]
    ...    ELSE IF    '${product_type}' == 'simple'    JSONLibrary.Get Value From Json    ${response}    $..products[?(@.type_id=='simple')]
    ...    ELSE    Fail    Incorrect product_type
    BuiltIn.Should Not Be Empty    ${list_product}    msg=Could not find any ${product_type} product data by category id : ${category_id}
    ${list_product}=    Evaluate    random.choice(${list_product})    random
    ${result}=    BuiltIn.Create Dictionary
    Collections.Set To Dictionary    ${result}    name    ${list_product}[name]
    Collections.Set To Dictionary    ${result}    sku    ${list_product}[sku]
    Collections.Set To Dictionary    ${result}    url_key    ${list_product}[url_key]
    [Return]    ${result}