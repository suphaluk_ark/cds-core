*** Keywords ***
Verify consent marketing and PDPA consent text should be displayed in registration page
    register_ios_keywords.Open register page
    register_common_page.Scroll from 'Top' to 'lbl PDPA consent text'
    AppiumLibrary.Wait Until Element Is Visible    ${dictRegisterPage}[lbl_marketing_consent_description]    ${GLOBALTIMEOUT}
    AppiumLibrary.Wait Until Element Is Visible    ${dictRegisterPage}[lbl_marketing_consent_description_on_checkbox]    ${GLOBALTIMEOUT}
    AppiumLibrary.Wait Until Element Is Visible    ${dictRegisterPage}[txt_PDPA_consent]    ${GLOBALTIMEOUT}

Verify consent marketing and PDPA consent text should be displayed in checkout step 1 for guest
    delivery_common_page.Scroll from 'Top' to 'lbl consent marketing description text'
    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_marketing_consent_description]    ${GLOBALTIMEOUT}
    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_marketing_consent_description_on_checkbox]    ${GLOBALTIMEOUT}
    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_PDPA_consent_text]    ${GLOBALTIMEOUT}

Verify PDPA consent text should be displayed in checkout step 1 for member when 'consent_privacy_status' equal to null
    [Arguments]    ${customer_email}    ${customer_password}
    ${consent_privacy_status}    common_mobile_app_keywords.Get consent info from check consent privacy status info response    ${customer_email}    ${customer_password}
    ${is_found_PDPA_consent}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_PDPA_consent_text]
    BuiltIn.Should Be True    ${consent_privacy_status} == ${None} and ${is_found_PDPA_consent} == ${True}

Verify PDPA consent text should not be displayed in checkout step 1 for member when 'consent_privacy_status' equal to true
    [Arguments]    ${customer_email}    ${customer_password}
    ${consent_privacy_status}    common_mobile_app_keywords.Get consent info from check consent privacy status info response    ${customer_email}    ${customer_password}
    ${is_found_PDPA_consent}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_PDPA_consent_text]
    BuiltIn.Should Be True    ${consent_privacy_status} == ${True} and ${is_found_PDPA_consent} == ${False}

Verify consent marketing consent text should be displayed
    [Arguments]    ${customer_email}    ${customer_password}
    ${consent_marketing_status}    common_mobile_app_keywords.Get consent info from check consent marketing status info response    ${customer_email}    ${customer_password}
    ${is_found_marketing_consent}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_marketing_consent_description_on_checkbox]
    BuiltIn.Should Be True    ${consent_marketing_status} == ${None} and ${is_found_marketing_consent} == ${True}

Verify consent marketing consent text should not be displayed
    [Arguments]    ${customer_email}    ${customer_password}
    ${consent_marketing_status}    common_mobile_app_keywords.Get consent info from check consent marketing status info response    ${customer_email}    ${customer_password}
    ${is_found_marketing_consent}    Run Keyword And Return Status    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_marketing_consent_description_on_checkbox]
    BuiltIn.Should Be True    ${consent_marketing_status} == ${True} and ${is_found_marketing_consent} == ${False}