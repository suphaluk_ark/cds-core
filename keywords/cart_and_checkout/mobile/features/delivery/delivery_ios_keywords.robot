*** Keywords ***
Guest, delivery successfully by standard pickup, self pickup
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    common_ios_mobile_app_keywords.Click keyboard done
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button
    
Membership, delivery successfully by standard pickup, self pickup
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by 1 hour pickup, self pickup
    delivery_common_page.Verify that customer information should be visible
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by shipping to address with the default address, standard delivery
    delivery_common_page.Click Home Delivery
    delivery_common_page.Verify that default address should be visible
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Guest, delivery successfully by shipping to address, standard delivery
    [Arguments]    ${post_code}
    ...    ${first_name}=${guest_data_01.information.firstname}
    ...    ${last_name}=${guest_data_01.information.lastname}
    ...    ${email}=${guest_data_01.email}
    ...    ${tel}=${guest_data_01.information.tel}
    ...    ${shipping_building}=${guest_data_01.information.shipping_building}
    ...    ${shipping_address}=${guest_data_01.information.shipping_address}
    delivery_common_page.Verify that customer information should be visible
    delivery_common_page.Input first name    ${first_name}
    delivery_common_page.Input last name    ${last_name}
    delivery_common_page.Input email address    ${email}
    delivery_common_page.Input phone no    ${tel}
    delivery_common_page.Click Home Delivery
    delivery_common_page.Input place building    ${shipping_building}
    delivery_common_page.Input house no street    ${shipping_address}
    delivery_common_page.Input postcode    ${post_code}
    delivery_ios_page.Select district by 1st index
    delivery_ios_page.Select subdistrict by 1st index
    delivery_common_page.Select Standard Delivery  
    delivery_common_page.Click continue to payment button

Guest, delivery sucessfully by 1hr and standard delivery
    [Arguments]    ${post_code}  
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Input place building    ${guest.guest_1.place_buidling}
    common_ios_mobile_app_keywords.Click keyboard done
    delivery_common_page.Input house no street    ${guest.guest_1.house_no_street}
    common_ios_mobile_app_keywords.Click keyboard done
    delivery_common_page.Input postcode    ${post_code}
    common_ios_mobile_app_keywords.Click keyboard done
    delivery_ios_page.Select district by 1st index
    delivery_ios_page.Select subdistrict by 1st index
    delivery_common_page.Click continue to payment button

Guest, delivery sucessfully by 1hr and standard pickup
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button

Membership, delivery sucessfully by 1hr and standard pickup
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by shipping to address with the default address, next day delivery
    delivery_common_page.Click Home Delivery
    delivery_common_page.Select Next Day Delivery
    delivery_common_page.Click continue to payment button

Membership delivery successfully 1hr and standard delivery
    delivery_common_page.Verify that customer information should be visible
    delivery_common_page.Click Click & Collect
    delivery_ios_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button