*** Keywords ***
Guest, delivery successfully by standard pickup, self pickup
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Click & Collect
    # self pickup -- BUG * SHOULD BE 'ON' 20200625
    # delivery_common_page.Click self pickup switch
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button

Guest, delivery successfully by shipping to address, standard delivery
    [Arguments]    ${post_code}  
    delivery_common_page.Input first name    ${guest_data_01.information.firstname}
    delivery_common_page.Input last name    ${guest_data_01.information.lastname}
    delivery_common_page.Input email address    ${guest_data_01.email}
    delivery_common_page.Input phone no    ${guest_data_01.information.tel}
    delivery_common_page.Click Home Delivery
    delivery_common_page.Input place building    ${guest_data_01.information.shipping_building}
    delivery_common_page.Input house no street    ${guest_data_01.information.shipping_address}
    delivery_common_page.Input postcode    ${post_code}
    delivery_android_page.Select district by 1st index
    delivery_android_page.Select subdistrict by 1st index
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Guest, delivery sucessfully by 1hr and standard delivery
    [Arguments]    ${post_code}  
    delivery_common_page.Input first name    ${guest.guest_1.first_name}
    delivery_common_page.Input last name    ${guest.guest_1.last_name}
    delivery_common_page.Input email address    ${guest.guest_1.email}
    delivery_common_page.Input phone no    ${guest.guest_1.phone}
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Input place building    ${guest.guest_1.place_buidling}
    delivery_common_page.Input house no street    ${guest.guest_1.house_no_street}
    delivery_common_page.Input postcode    ${post_code}
    delivery_android_page.Select district by 1st index
    delivery_android_page.Select subdistrict by 1st index
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by shipping to address with the default address, standard delivery
    delivery_common_page.Click Home Delivery
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by standard pickup, self pickup
    delivery_common_page.Click Click & Collect
    # delivery_common_page.Click self pickup switch
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click continue to payment button

Membership delivery successfully 1hr and standard delivery
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Select Standard Delivery
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by 1 hour pickup, self pickup
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click continue to payment button

Membership delivery successfully by 1hr pickup and standard pickup
    delivery_common_page.Click Click & Collect
    delivery_android_page.Select 'Automate - Test Store Information'
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click Standard Pickup
    delivery_common_page.Click 1 Hour Pickup or Next Day Pickup
    delivery_common_page.Click continue to payment button

Membership, delivery successfully by shipping to address with the default address, next day delivery
    delivery_common_page.Click Home Delivery
    delivery_common_page.Select Next Day Delivery
    delivery_common_page.Click continue to payment button