*** Keywords ***
Search product by product sku, add to cart, checkout
    [Arguments]    ${product_sku}
    checkout_android_keywords.Retry search product by product sku    ${product_sku}
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    product_pdp_common_page.Wait until click add to cart button with product successfully
    shopping_cart_common_page.Click checkout product button

Checkout completed with verifying price
    [Arguments]    ${expected_price}
    ${order_number}=    checkout_completed_common_page.Get non-split order number
    ${price_on_thankyou_page}=    checkout_completed_common_page.Get total price
    Should Be Equal As Numbers    ${price_on_thankyou_page}    ${expected_price}    msg=Price is not discounted correctly. Expected price = ฿${expected_price}
    checkout_completed_common_page.Click continue shopping
    [Return]    ${order_number}

Retry search product by product sku
    [Arguments]    ${product_sku}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    BuiltIn.Run Keywords
    ...    common_mobile_app_keywords.Input text into search box    ${${product_sku}}[sku]
    ...    AND    common_android_mobile_app_keywords.Click Search key on the virtual keyboard
    ...    AND    product_plp_common_page.Verify the total items label is visible
    ...    AND    product_plp_common_page.Click on the product name    ${${product_sku}}[name]

Search product by product sku
    [Arguments]    ${product_sku}
    checkout_android_keywords.Retry search product by product sku    ${product_sku}

Search product by product sku, add to cart and go back to homepage
    [Arguments]    ${product_sku}
    checkout_android_keywords.Retry search product by product sku    ${product_sku}
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    product_pdp_common_page.Wait until click add to cart button with product successfully
    shopping_cart_common_page.Click icon back page
    product_pdp_common_page.Click icon back page
    home_android_page.Click icon back homepage
    home_android_page.Click icon back homepage
    home_common_page.Verify that home screen is shown

Verify user can input coupon code and display correctly
    [Arguments]    ${coupon_code}=${promotions.rule_1023.coupon_code}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    shopping_cart_common_page.Input coupon code    ${coupon_code}
    ...    AND    shopping_cart_common_page.Click apply button
    ...    AND    shopping_cart_common_page.Verify coupon code displayed correctly    ${coupon_code}