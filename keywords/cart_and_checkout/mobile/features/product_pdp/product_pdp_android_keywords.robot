*** Keywords ***
Add product to cart
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    product_pdp_common_page.Wait until click add to cart button with product successfully

Add product to cart, continue to checkout details
    product_pdp_android_keywords.Add product to cart
    shopping_cart_common_page.Click checkout product button

Add product to cart, and back to previous page
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    Wait Until Keyword Succeeds    3x    1s    Run Keywords    product_pdp_common_page.Click 'Add to bag' button
    ...    AND    AppiumLibrary.Wait Until Element Is Visible    ${dictProductDetailPage}[icon_cart_with_product]    timeout=${GLOBALTIMEOUT}
    product_pdp_common_page.Click icon back page
