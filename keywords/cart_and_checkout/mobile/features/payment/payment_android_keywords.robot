*** Keywords ***
Payment successfully with credit card without OTP
    [Documentation]    After input credit card 4381741603113325, it will go to Thank You page
    payment_common_page.Click payment credit card
    payment_common_page.Input credit card no    ${payment.credit_card.card_number_without_OTP}
    payment_common_page.Input expired date    ${payment.credit_card.expired_date}
    payment_common_page.Input cvv    ${payment.credit_card.cvv}
    payment_common_page.Input name customer    ${payment.credit_card.full_name_on_card}
    payment_common_page.Click button pay now
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Payment successfully with credit card with OTP
    [Documentation]    After input credit card 4111111111111111, it will redirect to 2C2P page to input OTP
    payment_common_page.Click payment credit card
    payment_common_page.Input credit card no    ${payment.credit_card.card_number_with_OTP}
    payment_common_page.Input expired date    ${payment.credit_card.expired_date}
    payment_common_page.Input cvv    ${payment.credit_card.cvv}
    payment_common_page.Input name customer    ${payment.credit_card.full_name_on_card}
    payment_common_page.Click button pay now
    payment_android_page.Process OTP successfully on 2C2P page
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Payment fail via credit card with invalid OTP
    [Documentation]    After input credit card 4111111111111111, it will redirect to 2C2P page to input OTP
        ...    User input invalid OTP to make payment failed
    payment_common_page.Click payment credit card
    payment_common_page.Input credit card no    ${payment.credit_card.card_number_with_OTP}
    payment_common_page.Input expired date    ${payment.credit_card.expired_date}
    payment_common_page.Input cvv    ${payment.credit_card.cvv}
    payment_common_page.Input name customer    ${payment.credit_card.full_name_on_card}
    payment_common_page.Click button pay now
    payment_android_page.Process invalid OTP on 2C2P page
    checkout_completed_common_page.Verify that 'Sorry, payment failed!' should be displayed

Repayment successfully via credit card with OTP
    [Documentation]    After input credit card 4111111111111111, it will redirect to 2C2P page to input OTP
    payment_android_page.Click 'PAY NOW' to show repayment form
    payment_common_page.Verify 'REPAY WITH NEW CARD' should be displayed
    payment_common_page.Input credit card no for repayment    ${payment.credit_card.card_number_with_OTP}
    payment_android_page.Input expired date for repayment    ${payment.credit_card.expired_date_with_slash}
    payment_common_page.Input cvv    ${payment.credit_card.cvv}
    payment_android_page.Input name customer for repayment    ${payment.credit_card.full_name_on_card}
    payment_android_page.Click 'PAY NOW' for repayment
    payment_android_page.Process OTP successfully on 2C2P page
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Payment successfully with saved credit card
    [Documentation]    Depend on selected credit card from save card, it will go to Thank You page or 2C2P page to input OTP
                 ...   To enable OTP, set OTP = yes
    [Arguments]    ${process_otp}=no
    payment_common_page.Click payment credit card
    payment_common_page.Verify that 'saved card' should be visible
    payment_common_page.Input cvv saved card    ${credit_card_data.cvv}
    payment_common_page.Click button pay now
    Run Keyword If    '${process_otp}'=='yes'    Wait Until Keyword Succeeds    3x    10s    payment_android_page.Process OTP successfully on 2C2P page
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Payment successfully by cash on delivery
    payment_common_page.Click pay on delivery
    payment_common_page.Click button pay now
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Payment by bank transfer
    payment_common_page.Click payment bank
    payment_android_page.Select bank by full name
    payment_android_page.Select channel by name
    payment_common_page.Click button pay now
    payment_common_page.Verify that 'QRCODE' should be visible
    payment_common_page.Click continue button
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Payment successfully by pay at store
    payment_common_page.Click pay at store
    payment_common_page.Click button pay now
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible

Full Redeem T1 points
    payment_common_page.Connect T1 link
    payment_common_page.Login to T1C account    ${t1c_information}[email]    ${t1c_information}[password]
    T1_android_keywords.Redeem full point after login T1C

Partially Redeem T1 points
    [Arguments]    ${number_of_points}
    payment_common_page.Connect T1 link
    payment_common_page.Login to T1C account    ${t1c_information}[email]    ${t1c_information}[password]
    ${thb_discount}    T1_android_keywords.Calculate T1 point to THB    ${number_of_points}
    ${price_after_redeem}    T1_android_keywords.Partially redeem point after login T1C    ${thb_discount}    ${number_of_points}
    [Return]    ${price_after_redeem}

Payment by credit card unsuccessfully
    payment_common_page.Click payment credit card
    payment_common_page.Click button pay now

Verify does not complete the payment form
    payment_common_page.Verify error textfield card number
    payment_common_page.Verify error textfield expired date
    payment_common_page.Verify error textfield cvv
    payment_common_page.Verify error textfield fullname on card

Verify does not complete cvv
    payment_common_page.Verify error textfield cvv

Verify text credit card expired is displayed and promotion is inactive
    payment_common_page.Click payment credit card
    payment_android_page.Verify display text "Your default card is unavailable" at the bottom of the card
    payment_android_page.Verify credit card promotion option is inactive

Payment successfully by installment with OTP
    [Documentation]    After input credit card 4546230000000006, it will redirect to 2C2P page to input OTP
    payment_common_page.Click payment Installment
    payment_common_page.Click Bank Installment    ${bank.TBank}
    payment_common_page.Input credit card no    ${payment.credit_card.card_number_installment}
    payment_common_page.Input expired date    ${payment.credit_card.expired_date}
    payment_common_page.Input cvv    ${payment.credit_card.cvv}
    payment_common_page.Input name customer    ${payment.credit_card.full_name_on_card}
    payment_common_page.Click button pay now
    payment_android_page.Process OTP successfully on 2C2P page
    checkout_completed_common_page.Verify that 'Your order is successful' should be visible
