*** Keywords ***
Verify that 'Your order is successful' should be visible
    AppiumLibrary.Wait Until Element Is Visible    ${dict_checkout_completed_page}[lbl_ordered_successfully]    timeout=60

Verify that 'Sorry, payment failed!' should be displayed
    AppiumLibrary.Wait Until Element Is Visible    ${dict_checkout_completed_page}[lbl_ordered_failed]    timeout=${GLOBALTIMEOUT}

Get non-split order number
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dict_checkout_completed_page}[lbl_ordered_number]
    ${order_number}=    AppiumLibrary.Get Text    ${dict_checkout_completed_page}[lbl_ordered_number]
    [Return]    ${order_number}

Click continue shopping
    CommonMobileKeywords.Click Element    ${dict_checkout_completed_page}[btn_continue_shopping]

Verify order total is displayed correctly
    [Arguments]    ${actual_price}
    ${total_price}=    checkout_completed_common_page.Get total price
    BuiltIn.Should Be Equal As Integers    ${actual_price}    ${total_price}

Get total price
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dict_checkout_completed_page}[lbl_total]
    CommonMobileKeywords.Verify Elements Are Visible    ${dict_checkout_completed_page}[lbl_total]
    ${text}=    AppiumLibrary.Get Text    ${dict_checkout_completed_page}[lbl_total]
    ${total_price}=    CommonKeywords.Convert price to number format    ${text}
    [Return]    ${total_price}

Get order number that supports 'Standard Pickup'
    [Documentation]    Split order only
    ${order_number}=    Get order number through shipping method    ${thank_you_page_dto.lbl_standard_pickup}
    [Return]    ${order_number}

Get order number that supports '1 Hour Pickup'
    [Documentation]    Split order only
    ${order_number}=    Get order number through shipping method    ${thank_you_page_dto.lbl_1_hr_pickup}
    [Return]    ${order_number}
