*** Variables ***
&{dict_checkout_completed_page}
...    view_scrollable_locator=chain=**/XCUIElementTypeTable
...    lbl_ordered_successfully=chain=**/XCUIElementTypeStaticText[`label=="${thank_you_page_dto.lbl_ordered_successfully}"`]
...    lbl_ordered_failed=chain=**/XCUIElementTypeStaticText[`value == "${thank_you_page_dto.lbl_ordered_failed}"`]
...    lbl_ordered_number=chain=**/XCUIElementTypeStaticText[`name == "orderInfoIncrementIdLabel0"`]
...    btn_continue_shopping=chain=**/XCUIElementTypeButton[`label=="${checkout_delivery_page_dto.btn_continue_shopping}"`]
...    lbl_total=chain=**/XCUIElementTypeStaticText[`name == 'checkoutOrderTotalLabel0'`][1]
...    lbl_shipping_method=chain=**/XCUIElementTypeStaticText[`label BEGINSWITH "{shipping_method}"`]
...    lbl_order_number=chain=**/XCUIElementTypeStaticText[`name == "orderInfoIncrementIdLabel{index}"`]

*** Keywords ***
Get order number through shipping method
    [Arguments]    ${shipping_method}
    ${lbl_shipping_method_locator}=    CommonKeywords.Format Text    ${dict_checkout_completed_page}[lbl_shipping_method]    shipping_method=${shipping_method}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${lbl_shipping_method_locator}
    ${order_number_attribute_name}=    AppiumLibrary.Get Element Attribute    ${lbl_shipping_method_locator}    name
    ${index}=    String.Get Regexp Matches    ${order_number_attribute_name}    \\d+
    ${length}=    BuiltIn.Get Length    ${index}
    ${index}    Run Keyword If    ${length}>0    BuiltIn.Set Variable    ${index}[0]
    ${lbl_order_number_locator}=    CommonKeywords.Format Text    ${dict_checkout_completed_page}[lbl_order_number]    index=${index}
    ${order_number}=   AppiumLibrary.Get Text    ${lbl_order_number_locator}
    [Return]    ${order_number}

Get order number that supports 'Standard Delivery'
    [Documentation]    Split order only
    ${order_number}=    checkout_completed_ios_page.Get order number through shipping method    ${thank_you_page_dto.lbl_standard_delivery}
    [Return]    ${order_number}