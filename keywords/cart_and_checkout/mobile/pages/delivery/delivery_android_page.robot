*** Variables ***
&{dictCheckoutDeliveryPage}
...    view_scrollable_locator=android=UiSelector().resourceIdMatches(".*id/recyclerView$")
...    txt_firstname=android=UiSelector().resourceIdMatches(".*id/editTextFirstName$")
...    lbl_customer_info=android=UiSelector().resourceIdMatches(".*id/editTextFirstName$")
...    lbl_phoneno=android=UiSelector().resourceIdMatches(".*id/editTextPhoneNo$")
...    txt_lastname=android=UiSelector().resourceIdMatches(".*id/editTextLastName$")
...    txt_email=android=UiSelector().resourceIdMatches(".*id/editTextEmail$")
...    txt_phoneno=android=UiSelector().resourceIdMatches(".*id/editTextPhoneNo$")
...    rdo_home_delivery=android=UiSelector().text("${checkout_delivery_page_dto.rdo_home_delivery}")
...    rdo_standard_pickup=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").text("${checkout_delivery_page_dto.rdo_standard_pickup}").index(2)
...    lbl_store_name=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").text("{store_name}")
...    btn_select_store=android=UiSelector().resourceIdMatches(".*id/buttonSelected$")
...    btn_continue_payment=android=UiSelector().resourceIdMatches(".*id/buttonContinue$")
...    swt_self_pickup=android=UiSelector().resourceIdMatches(".*id/switchButton$")
...    lbl_delivery_option=android=UiSelector().text("${checkout_delivery_page_dto.lbl_delivery_option}")
...    lbl_select_store=android=UiSelector().text("${checkout_delivery_page_dto.lbl_select_store}")
...    lbl_default_address=android=UiSelector().resourceIdMatches(".*id/layoutAddress$")
...    txt_place_building=android=UiSelector().resourceIdMatches(".*id/editTextBuilding$")
...    txt_house_no_street=android=UiSelector().resourceIdMatches(".*id/editTextStreet$")
...    txt_postcode=android=UiSelector().resourceIdMatches(".*id/editTextPostcode$")
...    ddl_district=android=UiSelector().resourceIdMatches(".*id/editTextDistrict$")
...    rdo_bkk_district=android=UiSelector().text("${checkout_delivery_page_dto.rdo_bkk_district}")
...    ddl_subdistrict=android=UiSelector().resourceIdMatches(".*id/editTextSubDistrict$")
...    rdo_bkk_subdistrict=android=UiSelector().text("${checkout_delivery_page_dto.rdo_bkk_subdistrict}")
...    lbl_shipping_option=android=UiSelector().resourceIdMatches(".*id/textView$")
...    view_page=android=UiSelector().resourceIdMatches(".*id/recyclerView$")
...    lbl_PDPA_consent_text=android=UiSelector().resourceIdMatches(".*id/textViewPrivacy$")
...    lbl_marketing_consent_description=android=UiSelector().resourceIdMatches(".*id/materialTextView$")
...    lbl_marketing_consent_description_on_checkbox=android=UiSelector().resourceIdMatches(".*id/textViewCheckbox$")
...    lbl_secure_checkout=android=UiSelector().text("${checkout_delivery_page_dto.txt_secure_checkout}")
...    txt_search_store=android=UiSelector().resourceIdMatches(".*id/autoTextSearch$")
...    rdo_click_and_collect=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").text("${checkout_delivery_page_dto.rdo_click_collect}")
...    rdo_standard_delivery=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").text("${checkout_delivery_page_dto.txt_standard_delivery}")
...    rdo_1hour_pickup=android=UiSelector().textMatches("${checkout_delivery_page_dto.regex_lead_time_today}").fromParent(UiSelector().textMatches("${checkout_delivery_page_dto.rdo_1hour_pickup}"))
...    rdo_next_day_pickup=android=UiSelector().textMatches("${checkout_delivery_page_dto.regex_lead_time_tomorrow}").fromParent(UiSelector().textMatches("${checkout_delivery_page_dto.rdo_next_day_pickup}"))
...    rdo_next_day_delivery=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").text("${checkout_delivery_page_dto.rdo_next_day_delivery}").index(2)

&{dict_checkout_delivery_page_android}
...    ddl_district_list_1st_index=android=UiSelector().resourceIdMatches(".*id/layoutContent$").index(0)
...    ddl_subdistrict_list_1st_index=android=UiSelector().resourceIdMatches(".*id/layoutContent$").index(0)

*** Keywords ***
Search for a store name
    [Arguments]    ${store_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_search_store]
    AppiumLibrary.Input Text    ${dictCheckoutDeliveryPage}[txt_search_store]    ${store_name}
    ${locator}=    CommonKeywords.Format Text    ${dictCheckoutDeliveryPage}[lbl_store_name]    store_name=${store_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${locator}

Click select this store
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[btn_select_store]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[btn_select_store]

Select 'Automate - Test Store Information'
    [Documentation]    Store name: Automate - Test Store Information
    Search for a store name    ${checkout_delivery_page_dto.lbl_automate_test_store}
    Click select this store

Select district by 1st index
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[ddl_district]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element    ${dictCheckoutDeliveryPage}[ddl_district]
    CommonMobileKeywords.Click Element    ${dict_checkout_delivery_page_android}[ddl_district_list_1st_index]

Select subdistrict by 1st index
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[ddl_subdistrict]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[ddl_subdistrict]
    CommonMobileKeywords.Click Element  ${dict_checkout_delivery_page_android}[ddl_subdistrict_list_1st_index]