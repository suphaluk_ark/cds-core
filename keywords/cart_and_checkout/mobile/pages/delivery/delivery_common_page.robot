*** Keywords ***
Scroll from 'Top' to 'lbl consent marketing description text'
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[lbl_marketing_consent_description_on_checkbox]

Input first name
    [Arguments]    ${firstname}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_firstname]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    0.3
    AppiumLibrary.Wait Until Element Is Visible     ${dictCheckoutDeliveryPage}[txt_firstname]    timeout=${GLOBALTIMEOUT}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element  ${dictCheckoutDeliveryPage}[txt_firstname]    ${firstname}

Input last name
    [Arguments]    ${lastname}
    # TODO: 20201026 - workaround Input Lastname
    CommonMobileKeywords.Click Element    ${dictCheckoutDeliveryPage}[txt_lastname]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictCheckoutDeliveryPage}[txt_lastname]    ${lastname}

Input email address
    [Arguments]    ${email}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_email]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    0.3
    # TODO: 20201026 - workaround Input email address
    CommonMobileKeywords.Click Element    ${dictCheckoutDeliveryPage}[txt_email]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element   ${dictCheckoutDeliveryPage}[txt_email]    ${email}

Input phone no
    [Arguments]    ${phoneno}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_phoneno]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element    ${dictCheckoutDeliveryPage}[txt_phoneno]
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element   ${dictCheckoutDeliveryPage}[txt_phoneno]    ${phoneno}

Click Home Delivery
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_home_delivery]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_home_delivery]

Click Standard Pickup
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_standard_pickup]    ${GLOBAL_SCROLL_TIMES}*2    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_standard_pickup]

Click Click & Collect
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_click_and_collect]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_click_and_collect]

Click 1 Hour Pickup or Next Day Pickup
    [Arguments]    ${hour_pickup_cut_off_time}=18:00:00
    ${current_date}    Get Current Date    result_format=datetime
    ${store_cut_off_time}    Convert Date    ${current_date.year}-${current_date.month}-${current_date.day} ${hour_pickup_cut_off_time}    date_format=%Y-%m-%d %H:%M:%S    result_format=epoch
    ${current_date}    Convert Date    ${current_date}    result_format=epoch

    Run Keyword If    ${current_date} < ${store_cut_off_time}    Click 1 Hour Pickup
    ...    ELSE    Click Next Day Pickup

Click 1 Hour Pickup
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_1hour_pickup]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_1hour_pickup]

Click Next Day Pickup
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_next_day_pickup]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_next_day_pickup]

Click self pickup switch
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[swt_self_pickup]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[swt_self_pickup]
    ${isChecked}    CommonMobileKeywords.Get Element Attribute    ${dictCheckoutDeliveryPage}[swt_self_pickup]    ${common_attr.checked}
    ${isChecked}    BuiltIn.Convert To Boolean    ${isChecked}
    BuiltIn.Should Be True    ${isChecked}

Click continue to payment button
    AppiumLibrary.Wait Until Element Is Visible   ${dictCheckoutDeliveryPage}[btn_continue_payment]
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[btn_continue_payment]

Input place building
    [Arguments]    ${place_building}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_house_no_street]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element   ${dictCheckoutDeliveryPage}[txt_place_building]    ${place_building}

Input house no street
    [Arguments]    ${house_no_street}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_house_no_street]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    0.3
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element   ${dictCheckoutDeliveryPage}[txt_house_no_street]    ${house_no_street}

Input postcode
    [Arguments]    ${postcode}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[txt_postcode]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    AppiumLibrary.Input Text    ${dictCheckoutDeliveryPage}[txt_postcode]    ${postcode}

Verify that default address should be visible
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[lbl_default_address]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    0.3
    AppiumLibrary.Wait until element is visible    ${dictCheckoutDeliveryPage}[lbl_default_address]

Verify the title page is visible
    AppiumLibrary.Wait until element is visible    ${dictCheckoutDeliveryPage}[lbl_title_secure_checkout] 

Select Standard Delivery
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_standard_delivery]    ${GLOBAL_SCROLL_TIMES}*2    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_standard_delivery]

Select Next Day Delivery
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictCheckoutDeliveryPage}[view_scrollable_locator]    ${dictCheckoutDeliveryPage}[rdo_next_day_delivery]    ${GLOBAL_SCROLL_TIMES}*2    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictCheckoutDeliveryPage}[rdo_next_day_delivery]

Verify that customer information should be visible
    Wait Until Keyword Succeeds    3x    10s    Run Keywords    AppiumLibrary.Wait Until Page Contains Element    ${dictCheckoutDeliveryPage}[lbl_customer_information]    timeout=${GLOBALTIMEOUT}
    ...    AND    AppiumLibrary.Wait Until Element Is Visible    ${dictCheckoutDeliveryPage}[lbl_customer_information]    timeout=${GLOBALTIMEOUT}