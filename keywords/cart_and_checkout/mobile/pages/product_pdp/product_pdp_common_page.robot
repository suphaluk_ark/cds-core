*** Keywords ***
Click 'Add to bag' button
    CommonMobileKeywords.Click Element   ${dictProductDetailPage}[btn_add_cart]
    Verify 'Add to bag' label changes to 'Added'

Wait until click add to cart button with product successfully 
    Wait Until Keyword Succeeds    3x    1s    Click 'Add to bag' button with product successfully 

Click 'Add to bag' button with product successfully 
    Wait Until Keyword Succeeds    3x    1s    Click 'Add to bag' button
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductDetailPage}[icon_cart_with_product]    timeout=${GLOBALTIMEOUT}
    common_mobile_app_keywords.Click icon cart
    shopping_cart_common_page.Verify the title "SHOPPING BAG" should be displayed

Click icon back page
    CommonMobileKeywords.Click Element  ${dictCommonPage}[icon_back_page]    timeout=${GLOBALTIMEOUT}


Verify 'Add to bag' label changes to 'Added'
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductDetailPage}[lbl_added]    timeout=${GLOBALTIMEOUT}

Verify 'Add to bag' label should be visible
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductDetailPage}[lbl_add_to_bag]    timeout=${GLOBALTIMEOUT}