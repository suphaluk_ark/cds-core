*** Variables ***
&{dictProductDetailPage}
...    btn_add_cart=android=UiSelector().resourceIdMatches(".*id/buttonAddToCart$") 
...    icon_cart=android=UiSelector().resourceIdMatches(".*id/imageButton$")
...    icon_cart_with_product=android=UiSelector().resourceIdMatches(".*id/bgBadge$")
...    icon_wishlist=android=UiSelector().resourceIdMatches(".*id/buttonWishList$")
...    lbl_added=android=UiSelector().resourceIdMatches(".*id/buttonAddToCart$").text("${product_detail_page.lbl_added}")
...    lbl_add_to_bag=android=UiSelector().resourceIdMatches(".*id/buttonAddToCart$").text("${product_detail_page.lbl_add_to_bag}")

*** Keywords ***
Click wishlist button
    ${is_selected}=   AppiumLibrary.Get Element Attribute    ${dictProductDetailPage}[icon_wishlist]    selected
    ${is_selected}    Convert To Boolean    ${is_selected}
    BuiltIn.Run Keyword If    ${is_selected} == ${False}    CommonMobileKeywords.Click Element  ${dictProductDetailPage}[icon_wishlist]

Check wishlist button is clicked
    ${isChecked}    CommonMobileKeywords.Get Element Attribute    ${dictProductDetailPage}[icon_wishlist]    selected
    ${isChecked}    BuiltIn.Convert To Boolean    ${isChecked}
    BuiltIn.Should Be True    ${isChecked}

Click heart icon to remove wishlist item
    ${is_selected}=   AppiumLibrary.Get Element Attribute    ${dictProductDetailPage}[icon_wishlist]    selected
    ${is_selected}    Convert To Boolean    ${is_selected}
    BuiltIn.Run Keyword If    ${is_selected} == ${True}    CommonMobileKeywords.Click Element  ${dictProductDetailPage}[icon_wishlist]

Add product to shopping cart
    [Arguments]    ${product_sku}
    search_filter_android_keywords.Retry search product by product sku    ${product_sku}
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    product_pdp_common_page.Wait until click add to cart button with product successfully

Add product after searching
    [Arguments]    ${product_sku}
    search_filter_android_keywords.Retry Go to PDP after searching product    ${product_sku}
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    product_pdp_common_page.Wait until click add to cart button with product successfully