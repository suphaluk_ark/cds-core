*** Variables ***
&{dictProductDetailPage}
...    btn_add_cart=chain=**/XCUIElementTypeButton[`name == 'productDetailMainAddToCartButton'`]
...    icon_cart=chain=**/XCUIElementTypeButton[$name=="ic cart"$]
...    icon_cart_with_product=chain=**/XCUIElementTypeOther[$name =='ic cart' and type=='XCUIElementTypeButton'$]/XCUIElementTypeStaticText[2]
...    icon_wishlist=chain=**/XCUIElementTypeButton[`name == 'productDetailMainAddToWishlistButton'`]
...    icon_wishlist_active=chain=**/XCUIElementTypeButton[`name == 'productDetailMainAddToWishlistButton' and value == '1'`]
...    lbl_added=chain=**/XCUIElementTypeButton[`name == 'productDetailMainAddToCartButton'`]/XCUIElementTypeStaticText[`label CONTAINS[cd] '${product_detail_page.lbl_added}'`]
...    lbl_add_to_bag=chain=**/XCUIElementTypeButton[`name == 'productDetailMainAddToCartButton'`]/XCUIElementTypeStaticText[`label CONTAINS[cd] '${product_detail_page.lbl_add_to_bag}'`]

*** Keywords ***
Click wishlist button
    BuiltIn.Wait Until Keyword Succeeds    3x    3s    Verify wishlist is selected
    
Verify wishlist is selected
    CommonMobileKeywords.Click Element    ${dictProductDetailPage}[icon_wishlist]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductDetailPage}[icon_wishlist_active]    timeout=${GLOBALTIMEOUT}

Click heart icon to remove wishlist item
    BuiltIn.Wait Until Keyword Succeeds    3x    3s    Verify wishlist is not selected

Verify wishlist is not selected
    product_pdp_common_page.Verify 'Add to bag' label should be visible    #workaround: Adding this keyword Will help the PDP shows fully before click on Heart Icon
    CommonMobileKeywords.Click Element    ${dictProductDetailPage}[icon_wishlist_active]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Wait Until Element Is Visible    ${dictProductDetailPage}[icon_wishlist]    timeout=${GLOBALTIMEOUT}

Add product to shopping cart
    [Arguments]    ${product_sku}
    search_filter_ios_keywords.Retry search product by product sku    ${product_sku}
    product_pdp_common_page.Verify 'Add to bag' label should be visible
    product_pdp_common_page.Wait until click add to cart button with product successfully