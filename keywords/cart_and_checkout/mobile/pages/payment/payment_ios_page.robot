*** Variables ***
&{dictPaymentPage}
...    view_scrollable_locator=chain=**/XCUIElementTypeTable
...    view_repayment_scrollable_locator=chain=**/XCUIElementTypeScrollView
...    rdo_bank=chain=**/XCUIElementTypeStaticText[`label == '${payment_page_dto.rdo_bank_tranfer}'`]
...    rdo_credit_card=chain=**/XCUIElementTypeStaticText[`value == "${payment_page_dto.rdo_credit_card}" AND visible == 1`]    #visible == 1 to avoid mutiple layers overriding your current layer
...    txt_credit_card_no=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$]/XCUIElementTypeTextField[4]
...    txt_expired_date=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$]/XCUIElementTypeTextField[1]
...    txt_cvv=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$]/XCUIElementTypeTextField[3]
...    txt_name=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$]/XCUIElementTypeTextField[2]
...    btn_save_to_my_card=chain=**/XCUIElementTypeSwitch[$label == "${payment_page_dto.lbl_save_to_my_card_ios}"$]
...    btn_paynow=chain=**/XCUIElementTypeButton[`name == '${payment_page_dto.btn_pay_now}'`]
...    rdo_pay_on_delivery=chain=**/XCUIElementTypeStaticText[`label == '${payment_page_dto.rdo_pay_on_delivery}'`]
...    ddl_counter_bank=chain=**/XCUIElementTypeButton[`label == "${payment_page_dto.ddl_counter_bank}"`]
...    ddl_payment_channel=chain=**/XCUIElementTypeButton[`label == "${payment_page_dto.ddl_payment_channel}"`]
...    img_qr_code=chain=**/XCUIElementTypeOther[$name CONTAINS[cd] "QR"$]
...    btn_continue=chain=**/XCUIElementTypeButton[`name=="${payment_page_dto.btn_continue}"`]
...    btn_saved_card=chain=**/XCUIElementTypeCell[` label == "${payment_page_dto.lbl_saved_card}" `]
...    txt_cvv_saved_card=chain=**/XCUIElementTypeCell[$ value == "${payment_page_dto.lbl_saved_card}" $]/XCUIElementTypeSecureTextField
...    rdo_pay_at_store=chain=**/XCUIElementTypeStaticText[`label == "${payment_page_dto.rdo_pay_at_store}"`]
...    lbl_error_cardnumber=chain=**/XCUIElementTypeStaticText[`name == "${payment_page_dto.lbl_error_cardnumber}"`]
...    lbl_error_expired_date=chain=**/XCUIElementTypeStaticText[`name == "${payment_page_dto.lbl_error_expired_date}"`]
...    lbl_error_cvv=chain=**/XCUIElementTypeStaticText[`name == "${payment_page_dto.lbl_error_cvv}"`]
...    lbl_error_fullname_card=chain=**/XCUIElementTypeStaticText[`name == "${payment_page_dto.lbl_error_fullname_card}"`]
...    lnk_login_T1=chain=**/XCUIElementTypeStaticText[`name == "${payment_page_dto.lbl_connect_account}"`]
...    txt_email=chain=**/XCUIElementTypeTextField[`value == "${payment_page_dto.lbl_email_account}"`]
...    txt_password=chain=**/XCUIElementTypeSecureTextField[`value == "${payment_page_dto.lbl_pass_account}"`]
...    btn_login_t1c=chain=**/XCUIElementTypeButton[`name == "the1_connect_the1_button"`]
...    btn_redeem_t1c=chain=**/XCUIElementTypeStaticText[`label == "${payment_page_dto.lbl_redeem}"`]
...    ddl_editTextOption_t1c=chain=**/XCUIElementTypeTextField[`value == "${payment_page_dto.ddl_redeem}"`]
...    txt_partially_redeem_t1c=chain=**/XCUIElementTypeStaticText[$label CONTAINS[cd] "${payment_page_dto.lbl_partially_redeem}"$]
...    txt_full_redeem_t1c=chain=**/XCUIElementTypeStaticText[$label CONTAINS[cd] "${payment_page_dto.lbl_full_redeem}"$]
...    btn_apply_t1c_option=chain=**/XCUIElementTypeButton[`label == "${payment_page_dto.lbl_apply}"`]
...    txt_T1_redemption_apply=chain=**/XCUIElementTypeTextField[`name == "pointTextInput"`]
...    msg_T1_remove=chain=**/XCUIElementTypeStaticText[$label CONTAINS[cd] "${payment_page_dto.t1c_remove}"$]
...    txt_edit_T1_points=chain=**/XCUIElementTypeTextField[`name == "pointTextInput"`]
...    lbl_price=chain=**/XCUIElementTypeOther[$type == 'XCUIElementTypeButton' and value == "${payment_page_dto.view_details}"$][-1]/XCUIElementTypeStaticText[$name BEGINSWITH '฿'$]
...    btn_choose_card=chain=**/XCUIElementTypeButton[`name == 'chooseCardButton1'`]
...    txt_topic_bank_promotions=chain=**/XCUIElementTypeStaticText[`name == '${payment_page_dto.txt_topic_bank_promotions}'`]
...    txt_card_t1_red=chain=**/XCUIElementTypeStaticText[`value == '${payment_card.card_t1_red}'`]
...    rdo_installment=chain=**/XCUIElementTypeCell[$type == 'XCUIElementTypeStaticText' AND value == '${payment_page_dto.rdo_installment}'$]
...    bank_installment=chain=**/XCUIElementTypeStaticText[`value == '{bank_name}'`]
...    txt_credit_card_no_installment=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$][-1]/XCUIElementTypeTextField[1]
...    txt_expired_date_installment=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$][-1]/XCUIElementTypeTextField[2]
...    txt_cvv_installment=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$][-1]/XCUIElementTypeTextField[4]
...    txt_name_installment=chain=**/XCUIElementTypeCell[$type == "XCUIElementTypeStaticText" and name == "${payment_page_dto.lbl_card_no} *"$][-1]/XCUIElementTypeTextField[3]
...    txt_otp=chain=**/XCUIElementTypeOther[`label == "${2c2p_page_dto.lbl_name}"`]/XCUIElementTypeSecureTextField
...    btn_Proceed=chain=**/XCUIElementTypeButton[`name == '${payment_page_dto.btn_proceed}'`]
...    btn_Cancel=chain=**/XCUIElementTypeButton[`name == '${payment_page_dto.btn_cancel}'`]
...    msg_error=chain=**/XCUIElementTypeStaticText[`name == '${payment_page_dto.msg_invalid_otp}'`]
...    lbl_repay_with_new_card=chain=**/XCUIElementTypeStaticText[`name == '${payment_page_dto.lbl_repay_with_new_card}'`]
...    txt_repay_card_no=chain=**/XCUIElementTypeOther[`name == 'cardNumberTextView'`]/**/XCUIElementTypeTextField
...    txt_repay_expired_date=chain=**/XCUIElementTypeOther[`name == 'expiredDateTextView'`]/**/XCUIElementTypeTextField
...    txt_repay_CVV=chain=**/XCUIElementTypeOther[`name == 'cvvTextView'`]/**/XCUIElementTypeTextField
...    txt_repay_card_name=chain=**/XCUIElementTypeOther[`name == 'fullNameTextView'`]/**/XCUIElementTypeTextField
...    btn_repay_paynow=chain=**/XCUIElementTypeOther[$type == 'XCUIElementTypeButton' and name == 'footerSaveButton' and enabled == true$][-1]/**/XCUIElementTypeStaticText[`name == '${payment_page_dto.btn_pay_now}'`]
...    lbl_select_payment_method=chain=**/XCUIElementTypeStaticText[`label == "${payment_page_dto.txt_payment_method}"`]

&{dictPaymentPage_ios}
...    ddl_counter_bank_list_1st_index=chain=**/XCUIElementTypeTable/XCUIElementTypeCell[1]
...    ddl_payment_channel_list_1st_index=chain=**/XCUIElementTypeTable/XCUIElementTypeCell[1]

*** Keywords ***
Select counter bank by 1st index
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[ddl_counter_bank]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[ddl_counter_bank]
    CommonMobileKeywords.Click Element    ${dictPaymentPage_ios}[ddl_counter_bank_list_1st_index]

Select payment channel by 1st index
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[ddl_payment_channel]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[ddl_payment_channel]
    CommonMobileKeywords.Click Element    ${dictPaymentPage_ios}[ddl_payment_channel_list_1st_index]

Wait until button 'Apply' changes to 'Remove'
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[msg_T1_remove]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[msg_T1_remove]    timeout=${GLOBALTIMEOUT}

Input points for partial redeeming
    [Arguments]    ${points}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[txt_edit_T1_points]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Clear Text    ${dictPaymentPage}[txt_edit_T1_points]
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_edit_T1_points]    ${points}
    common_ios_mobile_app_keywords.Click keyboard done

Verify display text "Your default card is unavailable" at the bottom of the card
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[btn_choose_card]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Element Text Should Be    ${dictPaymentPage}[btn_choose_card]    ${payment_page_dto.txt_expected_card_bottom_ios}

Verify credit card promotion option is inactive
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_card_t1_red]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Verify Elements Are Visible  ${dictPaymentPage}[txt_topic_bank_promotions]

Switch off the save to my card button
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[btn_save_to_my_card]
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[btn_save_to_my_card]
    ${value}=    AppiumLibrary.Get Element Attribute    ${dictPaymentPage}[btn_save_to_my_card]    value
    BuiltIn.Should Be True    '${value}'=='0'    #0 is switch off

Process OTP successfully on 2C2P page
    [Documentation]    Input OTP '123456' on 2c2p page
    [Arguments]    ${otp}=123456
    payment_common_page.Input OTP 2c2p    ${otp}
    common_ios_mobile_app_keywords.Click keyboard done
    payment_common_page.Click proceed button

Process invalid OTP on 2C2P page
    [Documentation]    Input invalid OTP '666666' on 2c2p page
    payment_common_page.Input OTP 2c2p    ${payment.credit_card.invalid_otp}
    common_ios_mobile_app_keywords.Click keyboard done
    payment_common_page.Click Proceed button
    payment_common_page.Verify 'Invalid OTP code. Please recheck the OTP and try again' should be displayed
    payment_common_page.Click Cancel button

Input expired date for repayment
    [Arguments]    ${expired_date}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_repayment_scrollable_locator]    ${dictPaymentPage}[txt_repay_expired_date]
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[txt_repay_expired_date]    timeout=${GLOBALTIMEOUT}    #workaround
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_repay_expired_date]    ${expired_date}

Input CVV for repayment
    [Arguments]    ${cvv}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_repayment_scrollable_locator]    ${dictPaymentPage}[txt_repay_CVV]
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[txt_repay_CVV]    timeout=${GLOBALTIMEOUT}    #workaround
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictPaymentPage}[txt_repay_CVV]    ${cvv}

Input customer name for repayment
    [Arguments]    ${card_name}
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_repayment_scrollable_locator]    ${dictPaymentPage}[txt_repay_card_name]
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[txt_repay_card_name]    timeout=${GLOBALTIMEOUT}    #workaround
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictPaymentPage}[txt_repay_card_name]    ${card_name}

Click 'Pay Now' button for repayment
    CommonMobileKeywords.Click Element    ${dictPaymentPage}[btn_repay_paynow]    timeout=${GLOBALTIMEOUT}