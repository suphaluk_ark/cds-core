*** Variables ***
&{dictPaymentPage}
...    view_scrollable_locator=android=UiSelector().resourceIdMatches(".*id/recyclerView$")
...    rdo_bank=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").textContains("${payment_page_dto.rdo_bank_tranfer}")
...    rdo_credit_card=android=UiSelector().text("${payment_page_dto.rdo_credit_card}")
...    txt_bank_type=android=UiSelector().resourceIdMatches(".*id/autoCompleteTextViewType$")
...    txt_channel=android=UiSelector().resourceIdMatches(".*id/autoCompleteTextViewChannel$")
...    txt_credit_card_no=android=UiSelector().resourceIdMatches(".*id/editTextCardNumber$")
...    txt_expired_date=android=UiSelector().resourceIdMatches(".*id/editTextExpiredDate$")
...    txt_cvv=android=UiSelector().resourceIdMatches(".*id/editTextCVV$")
...    txt_name=android=UiSelector().resourceIdMatches(".*id/editTextName$")
...    btn_paynow=android=UiSelector().resourceIdMatches(".*id/buttonContinue$")
...    rdo_pay_on_delivery=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").textContains("${payment_page_dto.rdo_pay_on_delivery}")
...    txt_cvv_saved_card=android=UiSelector().resourceIdMatches(".*id/editTextCVV$")
...    btn_saved_card=android=UiSelector().resourceIdMatches(".*id/layoutSavedCard$")
...    view_page=android=UiSelector().resourceIdMatches(".*id/recyclerView$")
...    img_qr_code=android=UiSelector().resourceIdMatches(".*id/textViewQRTitle$")
...    btn_continue=android=UiSelector().resourceIdMatches(".*id/buttonContinue$")
...    lbl_view_cvv_hint=android=UiSelector().resourceIdMatches(".*id/textViewCVVHint$")
...    rdo_pay_at_store=android=UiSelector().text("${payment_page_dto.rdo_pay_at_store}")
...    lnk_login_T1=android=UiSelector().resourceIdMatches(".*id/buttonConnect$")
...    txt_email=android=UiSelector().resourceIdMatches(".*id/editTextEmail$")
...    txt_password=android=UiSelector().resourceIdMatches(".*id/editTextPassword$")
...    btn_login_t1c=android=UiSelector().resourceIdMatches(".*id/buttonConnect$")
...    btn_redeem_t1c=android=UiSelector().resourceIdMatches(".*id/buttonRedeem$")
...    ddl_editTextOption_t1c=android=UiSelector().resourceIdMatches(".*id/editTextOption$")
...    rdo_full_redeem_t1c=android=UiSelector().textContains("${payment_page_dto.lbl_full_redeem}")
...    btn_apply_t1c_option=android=UiSelector().resourceIdMatches(".*id/buttonApply$")
...    txt_T1_redemption_apply=android=UiSelector().resourceIdMatches(".*id/snackbar_text$")
...    txt_partially_redeem_t1c=android=UiSelector().text("${payment_page_dto.lbl_partially_redeem}")
...    txt_edit_T1_points=android=UiSelector().resourceIdMatches(".*id/editTextPoint$")
...    lbl_price=android=UiSelector().resourceIdMatches(".*id/textViewGrandTotal$")
...    btn_secure_checkout=android=UiSelector().text("${payment_page_dto.txt_secure_checkout}")
...    txt_payment_method=android=UiSelector().text("${payment_page_dto.txt_payment_method}")
...    lbl_error_cardnumber=android=UiSelector().textContains("${payment_page_dto.lbl_error_cardnumber}")
...    lbl_error_expired_date=android=UiSelector().textContains("${payment_page_dto.lbl_error_expired_date}")
...    lbl_error_cvv=android=UiSelector().textContains("${payment_page_dto.lbl_error_cvv}")
...    lbl_error_fullname_card=android=UiSelector().textContains("${payment_page_dto.lbl_error_fullname_card}")
...    btn_choose_card=android=UiSelector().resourceIdMatches(".*id/buttonChooseCard$")
...    txt_card_t1_red=android=UiSelector().textContains('${payment_card.card_t1_red}')
...    rdo_installment=android=UiSelector().textContains("${payment_page_dto.rdo_installment}")
...    bank_installment=android=UiSelector().textContains("{bank_name}")
...    txt_otp=android=UiSelector().className("android.widget.EditText")
...    btn_Proceed=android=UiSelector().resourceIdMatches("continue")
...    btn_Cancel=android=UiSelector().resourceIdMatches("dvCancel")
...    msg_error=android=UiSelector().resourceIdMatches("error-message")
...    lbl_repay_with_new_card=android=UiSelector().resourceIdMatches(".*id/textViewTitle$").textContains("${payment_page_dto.lbl_repay_with_new_card}")
...    btn_repayment=android=UiSelector().resourceIdMatches(".*id/buttonRepayment$")
...    btn_paynow_repayment=android=UiSelector().resourceIdMatches(".*id/buttonPayNow$")

*** Keywords ***
Select bank by full name
    [Arguments]    ${bank_name}=Bangkok Bank
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_bank_type]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element  ${dictPaymentPage}[txt_bank_type]
    Click Text    ${bank_name}

Select channel by name
    [Arguments]    ${channel_name}=IBANKING
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_channel]    ${global_scroll_times}    ${global_scale_min}    ${global_scale_medium}
    CommonMobileKeywords.Click Element  ${dictPaymentPage}[txt_channel]
    Click Text    ${channel_name}

Select bank by index
    [Arguments]    ${index}=1
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_bank_type]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictPaymentPage}[txt_bank_type]
    common_android_mobile_app_keywords.Click item of dropdown by position  ${dictPaymentPage}[view_page]  ${dictPaymentPage}[txt_bank_type]  ${index}

Select channel by index
    [Arguments]    ${index}=1
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_channel]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    CommonMobileKeywords.Click Element  ${dictPaymentPage}[txt_channel]
    common_android_mobile_app_keywords.Click item of dropdown by position  ${dictPaymentPage}[view_page]  ${dictPaymentPage}[txt_channel]  ${index}

Input T1 account email
    [Arguments]    ${user}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[txt_email]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Clear Text    ${dictPaymentPage}[txt_email]
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_email]    ${user}

Input T1 account password
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[txt_password]
    AppiumLibrary.Clear Text    ${dictPaymentPage}[txt_password]
    # AppiumLibrary.Input Text    ${dictLoginPage}[txt_password]    ${pwd}
    AppiumLibrary.Input Password    ${dictPaymentPage}[txt_password]    ${pwd}

Verify that 'Your The 1 redemption has been applied' should be visible
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[txt_T1_redemption_apply]

Input points for partial redeeming
    [Arguments]    ${points}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[txt_edit_T1_points]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Clear Text    ${dictPaymentPage}[txt_edit_T1_points]
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_edit_T1_points]    ${points}

Verify display text "Your default card is unavailable" at the bottom of the card
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[btn_choose_card]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}
    AppiumLibrary.Wait Until Element Is Visible    ${dictPaymentPage}[btn_choose_card]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Element Text Should Be    ${dictPaymentPage}[btn_choose_card]    ${payment_page_dto.txt_expected_card_bottom_android}

Verify credit card promotion option is inactive
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dictPaymentPage}[view_scrollable_locator]    ${dictPaymentPage}[txt_card_t1_red]    ${GLOBAL_SCROLL_TIMES}    ${GLOBAL_SCALE_MIN}    ${GLOBAL_SCALE_MEDIUM}

Process OTP successfully on 2C2P page
    [Documentation]    Input OTP '123456' on 2c2p page
    [Arguments]    ${otp}=123456
    payment_common_page.Input OTP 2c2p    ${otp}
    payment_common_page.Click Proceed button

Process invalid OTP on 2C2P page
    [Documentation]    Input invalid OTP '666666' on 2c2p page
    payment_common_page.Input OTP 2c2p    ${payment.credit_card.invalid_otp}
    payment_common_page.Click Proceed button
    payment_common_page.Verify 'Invalid OTP code. Please recheck the OTP and try again' should be displayed
    payment_common_page.Click Cancel button

Click 'PAY NOW' to show repayment form
    [Documentation]    Click PAY NOW to show repayment credit card form
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[btn_repayment]    timeout=${GLOBALTIMEOUT}    #todo

Click 'PAY NOW' for repayment
    [Documentation]    Click PAY NOW to repay
    CommonMobileKeywords.Click Element   ${dictPaymentPage}[btn_paynow_repayment]    timeout=${GLOBALTIMEOUT}

Input name customer for repayment
    [Documentation]    Repayment page does not require scrollable locator
    [Arguments]    ${name}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictPaymentPage}[txt_name]    ${name}

Input expired date for repayment
    [Arguments]    ${expired_date}
    [Documentation]    Repayment page does not require scrollable locator
    AppiumLibrary.Input Text    ${dictPaymentPage}[txt_expired_date]    ${expired_date}