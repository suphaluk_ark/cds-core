*** Keywords ***
Input first name
    [Arguments]    ${first_name}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictRegisterPage}[txt_first_name]    ${first_name}

Input last name
    [Arguments]    ${last_name}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictRegisterPage}[txt_last_name]    ${last_name}

Input email
    [Arguments]    ${email}
    CommonMobileKeywords.Input Text And Verify Input For Mobile Element    ${dictRegisterPage}[txt_email]    ${email}

Input password
    [Arguments]    ${pwd}
    AppiumLibrary.Input Text    ${dictRegisterPage}[txt_pwd]    ${pwd}

Click privacy policy
    CommonMobileKeywords.Click Element    ${dictRegisterPage}[chk_privacy_policy]

Click register button
    CommonMobileKeywords.Click Element    ${dictRegisterPage}[btn_register]

Verify that 'Successfully Registered' is visible
    common_mobile_app_keywords.Element Text Should Be    ${dictRegisterPage}[lbl_successfully_registered]    ${register_dto.lbl_successfully_registered}

Click continue shopping
    CommonMobileKeywords.Click Element    ${dictRegisterPage}[btn_continue_shopping]

Click lbl register
    CommonMobileKeywords.Click Element    ${dictRegisterPage}[lbl_register]

Scroll from 'Top' to 'lbl PDPA consent text'
    CommonMobileKeywords.Scroll Up Until Element Is Found  ${dictRegisterPage}[btn_register]    ${dictRegisterPage}[txt_PDPA_consent]

