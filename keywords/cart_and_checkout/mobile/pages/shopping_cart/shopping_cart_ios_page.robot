*** Variables ***
&{dictShoppingCartPage}
...    img_product=chain=**/XCUIElementTypeImage
...    btn_checkout=chain=**/XCUIElementTypeButton[`name == '${shopping_cart_page_dto.lbl_secure_checkout}'`]
...    lbl_add_wishlist=chain=**/XCUIElementTypeButton[`name == '${shopping_cart_page_dto.lbl_add_wishlist}'`]
...    lbl_promo_header=chain=**/XCUIElementTypeStaticText[`name=="${shopping_cart_page_dto.txt_promo_header}"`]
...    lbl_product_name=chain=**/XCUIElementTypeStaticText[`name=="{product_name}"`]
...    lbl_promotion_item_status=chain=**/XCUIElementTypeCell/XCUIElementTypeStaticText[`name=="${shopping_cart_page_dto.lbl_free}"`]
...    lbl_remove=chain=**/XCUIElementTypeStaticText[`name == "${shopping_cart_page_dto.lbl_remove}"`]
...    btn_continue_shopping=chain=**/XCUIElementTypeButton[`name == "${shopping_cart_page_dto.btn_continue_shopping}"`]
...    txt_coupon_code=chain=**/XCUIElementTypeTextField
...    btn_apply=chain=**/XCUIElementTypeButton[`value =="${shopping_cart_page_dto.btn_apply}"`]
...    lbl_coupon_code=chain=**/XCUIElementTypeStaticText[`name=="{coupon_code}"`]
...    lbl_total=chain=**/XCUIElementTypeOther[$type == "XCUIElementTypeButton" and name == "${shopping_cart_page_dto.lbl_secure_checkout}"$]/XCUIElementTypeStaticText
...    lbl_shopping_bag=chain=**/XCUIElementTypeStaticText[`value BEGINSWITH "${shopping_cart_page_dto.lbl_shopping_bag}"`]
...    lbl_free_item=chain=**/XCUIElementTypeStaticText[`value BEGINSWITH "${shopping_cart_page_dto.lbl_free_item}"`]
