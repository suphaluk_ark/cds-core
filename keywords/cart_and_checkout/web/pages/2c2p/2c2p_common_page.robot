*** Keywords ***
Get OTP from 2c2p page
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_2c2p_page.lal_otp}
    ${otp}=    SeleniumLibrary.Get Text    ${dict_2c2p_page.lal_otp}
    ${otp}=    Get Regexp Matches    ${otp}    =\\s([0-9]+)    1
    [Return]    ${otp}[0]

Input OTP
    [Arguments]    ${otp}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_2c2p_page.txt_otp}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_2c2p_page.txt_otp}    ${otp}

Select proceed button
    CommonWebKeywords.Click Element    ${dict_2c2p_page.btn_proceed}

Click return to merchant now
    Run Keyword And Ignore Error    CommonWebKeywords.Click Element    ${dict_2c2p_page.btn_return_to_merchant}

Click cancel payment
    CommonWebKeywords.Click Element    ${dict_2c2p_page.btn_cancel}