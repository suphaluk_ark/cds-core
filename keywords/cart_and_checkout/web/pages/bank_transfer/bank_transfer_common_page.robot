*** Keywords ***
Payment code should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_bank_transfer_page.txt_ref}

Click return to merchant button
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_bank_transfer_page.btn_back}
    CommonWebKeywords.Scroll And Click Element    ${dict_bank_transfer_page.btn_back}
