*** Keywords ***
Click pay now button
    CommonWebKeywords.Scroll To Element    ${dict_payment_page.btn_pay_now}
    SeleniumLibrary.Wait Until Element Is Enabled    ${dict_payment_page.btn_pay_now}
    CommonWebKeywords.Click Element Using Javascript    ${dict_payment_page.btn_pay_now}
    loader_common_fragment.Wait until page loader is not visible

# T1C 
Click connect to T1C link
    CommonWebKeywords.Click Element    ${dict_payment_page.lnk_connect_t1c}
    loader_common_fragment.Wait until page loader is not visible

Input T1C email 
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_payment_page.txt_t1c_email}    ${email}

Input T1C password
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_payment_page.txt_t1c_password}    ${password}

Click T1C login button
    CommonWebKeywords.Click Element    ${dict_payment_page.btn_t1c_login}

Click redeem list
    CommonWebKeywords.Click Element    ${dict_payment_page.ddl_redeem}

Select redeem for discount
    CommonWebKeywords.Click Element    ${dict_payment_page.lst_redeem_for_discount}

Select full redeem point
    CommonWebKeywords.Click Element    ${dict_payment_page.lst_full_redeem_point}

Input T1C point
    [Arguments]    ${point}
    CommonWebKeywords.Input Text And Verify Input For Web Element   ${dict_payment_page.txt_point}    ${point}

Click T1C apply button 
    CommonWebKeywords.Click Element    ${dict_payment_page.btn_apply_point}

Click pay now full redeem button
    SeleniumLibrary.Wait Until Element Is Enabled    ${dict_payment_page.btn_pay_now_full_redeem}
    CommonWebKeywords.Click Element    ${dict_payment_page.btn_pay_now_full_redeem}

Verify redeem link should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_payment_page.lnk_redeem}

# Credit card full payment
Click credit card, debit cart option 
    CommonWebKeywords.Click Element    ${dict_payment_page.opt_credit_card}
    loader_common_fragment.Wait until page loader is not visible

Select iframe credit card
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_payment_page.iframe_credit_card}     ${GLOBALTIMEOUT}
    SeleniumLibrary.Select Frame    ${dict_payment_page.iframe_credit_card}

Input credit card name
    [Arguments]    ${name}
    CommonWebKeywords.Input Text And Verify Input For Web Element   ${dict_payment_page.txt_credit_card_name}    ${name}

Input credit card number
    [Arguments]    ${card_number}
    SeleniumLibrary.Input Text   ${dict_payment_page.txt_credit_card_number}    ${card_number}

Input credit card expiry date 
    [Arguments]    ${exipry_date}
    SeleniumLibrary.Input Text   ${dict_payment_page.txt_credit_card_expiry_date}    ${exipry_date}

Input credit card cvv
    [Arguments]    ${cvv}
    CommonWebKeywords.Input Text And Verify Input For Web Element   ${dict_payment_page.txt_credit_card_cvv}    ${cvv}

Input default credit card cvv
    [Arguments]    ${cvv}
    CommonWebKeywords.Input Text And Verify Input For Web Element   ${dict_payment_page.txt_default_credit_card_cvv}    ${cvv}

Select save card
    CommonWebKeywords.Scroll To Element    ${dict_payment_page.chk_save_card}
    SeleniumLibrary.Select Checkbox    ${dict_payment_page.chk_save_card}
    SeleniumLibrary.Checkbox Should Be Selected    ${dict_payment_page.chk_save_card}

Unselect save card
    CommonWebKeywords.Scroll To Element    ${dict_payment_page.chk_save_card}
    SeleniumLibrary.Unselect Checkbox    ${dict_payment_page.chk_save_card}
    SeleniumLibrary.Checkbox Should Not Be Selected    ${dict_payment_page.chk_save_card}

Click installment option
    CommonWebKeywords.Click Element    ${dict_payment_page.opt_installment}
    loader_common_fragment.Wait until page loader is not visible

Select installment bank
    [Arguments]    ${bank}
    ${elem}=  CommonKeywords.Format Text    ${dict_payment_page.opt_bank_installment}    $bank=${bank}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Select installment month plan
    [Arguments]    ${months}
    ${elem}=  CommonKeywords.Format Text    ${dict_payment_page.lst_items_plan_installment}    $months=${months}
    ${plan_installment}=    SeleniumLibrary.Get Value    ${elem}
    SeleniumLibrary.Select From List By Value    ${dict_payment_page.ddl_plan_installment}    ${plan_installment}

Select iframe credit card installment
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_payment_page.iframe_credit_card_installment}     ${GLOBALTIMEOUT}
    SeleniumLibrary.Select Frame    ${dict_payment_page.iframe_credit_card_installment}

# Bank transfer
Click bank transfer, service counter option
    CommonWebKeywords.Click Element    ${dict_payment_page.opt_bank_transfer}
    loader_common_fragment.Wait until page loader is not visible

Select bank, service counter
    [Arguments]    ${bank}
    CommonWebKeywords.Select dropdownlist by label    ${dict_payment_page.ddl_bank_group}    ${bank}

Select payment channel
    [Arguments]    ${channel}
    CommonWebKeywords.Select dropdownlist by label    ${dict_payment_page.ddl_payment_channel}    ${channel}

Input phone number
    [Arguments]    ${tel}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_payment_page.txt_phone}    ${tel}

# cash on delivery
Click cash on delivery option
    CommonWebKeywords.Click Element    ${dict_payment_page.opt_cash_on_delivery}
    loader_common_fragment.Wait until page loader is not visible

# pay at store
Click pay at store option
    CommonWebKeywords.Click Element    ${dict_payment_page.opt_pay_at_store}
    loader_common_fragment.Wait until page loader is not visible

Verify repayment should be visibles
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_payment_page.lbl_repayment}    ${dict_payment_page.lbl_repayment_container}