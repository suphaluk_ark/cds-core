*** Keywords ***
Get order ID, set test variable
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_thank_you_page.lbl_order_id}
    ${order_id}=    SeleniumLibrary.Get Text    ${dict_thank_you_page.lbl_order_id}
    Set Test Variable    ${order_id}

Verify order tax invoice address should be displayed correctly
    [Arguments]    ${address}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_thank_you_page.lbl_tax_invoice_address}    ${address}

Verify order tax invoice id should be displayed correctly
    [Arguments]    ${invoice_id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_thank_you_page.lbl_tax_invoice_tax_id}    ${invoice_id}

Click repayment button
    CommonWebKeywords.Click Element    ${dict_thank_you_page.btn_repayment}