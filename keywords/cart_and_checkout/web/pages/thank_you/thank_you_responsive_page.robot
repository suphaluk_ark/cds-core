*** Variables ***
&{dict_thank_you_page}
...    lbl_order_total=xpath=//*[@data-testid='inf-viewThankyouPageOnOrderSummary-grandTotal']
...    lbl_order_id=//*[@data-testid="inf-viewThankyouPageOnOrderDetail-orderId"]
...    lbl_tax_invoice_tax_id=xpath=//*[@data-testid="inf-viewThankyouPageOnOrderTaxInvoice-texId"]
...    lbl_tax_invoice_address=xpath=//*[@data-testid="inf-viewThankyouPageOnOrderTaxInvoice-address"]
...    btn_repayment=css=[data-testid="inf-viewThankyouPage-repaymentNowButton"]