*** Variables ***
&{dict_delivery_page}
...    opt_home_delivery=xpath=//*[@select-delivery="home-delivery"]
...    opt_standard_delivery=xpath=//*[@data-testid="rdo-addShippingMethodFee-main-package-options-standard"]
...    opt_same_day_delivery=xpath=//*[@data-testid="rdo-addShippingMethodFee-main-package-options-same_day"]
...    opt_next_day_delivery=xpath=//*[@data-testid="rdo-addShippingMethodFee-main-package-options-next_day"]
...    btn_continue_to_payment=xpath=//*[@id='btn-viewPayment']
...    txt_customer_firstname=xpath=//*[@name="firstname"]
...    txt_customer_lastname=xpath=//*[@name="lastname"]
...    txt_customer_email=xpath=//*[@name="email"]
...    txt_customer_phone=xpath=//*[@name="telephone"]
...    txt_shipping_address=xpath=//*[@name="address_line"]
...    txt_post_code=xpath=//*[@name="postcode"]
...    chk_request_tax_invoice=xpath=//*[text()='${checkout_dto.do_you_require_a_full_tax_invoice}']/following-sibling::*//*[@role="checkbox"]
...    opt_click_and_collect=xpath=//*[@select-delivery="click-and-collect"]
...    btn_select_store=xpath=//*[contains(@data-testid,"inf-viewPickupStoreDetail")][.//*[text()='{$store}']]/following-sibling::button[@data-testid='btn-set-store']
...    opt_standard_pickup=xpath=//*[@data-testid='inf-viewactiveCircleOnPackageHeader-future_day_pickup']
...    btn_pin_location_guest=xpath=//div[*[@alt='Location Icon']]
...    rdo_main_package_products=css=[data-testid*="rdo-addShippingMethodFee-main-package-product"]
...    rdo_pin_location_require=css=[data-testid*="rdo-addShippingMethodFee-PinLocationRequire"]
...    sel_address_province=css=[name='province']
...    sel_address_district=css=[name='district']
...    sel_address_subdistrict=css=[name='subdistrict']
...    sel_address_province_guest=css=[name='region_id']
...    sel_address_district_guest=css=[name='district_id']
...    sel_address_subdistrict_guest=css=[name='subdistrict_id']
...    lbl_pin_you_location=xpath=//*[text()='${checkout_dto.pin_your_location}']
...    txt_pin_your_location=xpath=//*[*[contains(text(),"${checkout_dto.pin_your_location}")]]//input
...    btn_continue_pin_location=css=[data-testid=btn-savePinYourLocationModal-PinButton]
...    lbl_latitude=css=[name='latitude']
...    lbl_longitude=css=[name='longitude']
...    lbl_3_hour_delivery=xpath=//*[@data-testid="rdo-addShippingMethodFee-main-package-options-ship_from_store"]//*[text()='${shipping_method_label.three_hour_delivery.label}']
...    lbl_3_hours_delivery_price=css=[data-testid="rdo-addShippingMethodFee-main-package-options-ship_from_store"] span
...    lbl_shipping_address_guest=css=[alt='Location Icon'] ~ div > p:last-child
...    lbl_3_hour_shipping_address=css=[data-testid="rdo-addShippingMethodFee-PinLocationRequire"]
...    rdo_3_hour_delivery=css=[data-testid="rdo-addShippingMethodFee-main-package-options-ship_from_store"]
...    btn_address_book=css=[data-testid="btn-editCustomerShippingAddressOnCheckout-AddressBook"]
...    lbl_pin_location=css=[data-testid='inf-viewCustomerShippingAddressOnCheckout-PinLocation']
...    lbl_require_3_hour=css=[data-testid=PinLocationRequire-openModal] > div > p:last-child
...    txt_first_name_member=css=[name="firstName"][data-testid="inp-editAddressFormWithPinLocations-Input"]
...    txt_last_name_member=css=[name="lastName"][data-testid="inp-editAddressFormWithPinLocations-Input"]
...    txt_phone_member=css=[name="phone"][data-testid="inp-editAddressFormWithPinLocations-Input"]
...    txt_address_member=css=[name="address"][data-testid="inp-editAddressFormWithPinLocations-Input"]
...    txt_post_code_member=css=[name="postcode"][data-testid="inp-editAddressFormWithPinLocations-Input"]
...    btn_save_address=xpath=//*[@id="btn-addAddress"][text()="${checkout_dto.shipping_address_popup.save_address}"]
...    lbl_last_address=css=#address-modal-list > div > div:last-child #checkout-address
...    btn_add_address_popup=css=#btn-addAddress
...    btn_change_address=xpath=//*[text()="${checkout_dto.change_address}"]
...    btn_use_selected_address=xpath=//*[text()="${checkout_dto.shipping_address_popup.use_selected_address}"]