*** Keywords ***
Click continue to payment button
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.btn_continue_to_payment}
    loader_common_fragment.Wait until page loader is not visible

Get status same day shipping is unavailable
    ${current_date_time}=    Get Current Date     result_format='%Y-%m-%d %H:%M:%S'
    ${same_day_disable_start}=    Get Current Date     result_format='%Y-%m-%d ${mdc_configuration.same_day_disable_time_start}'
    ${same_day_disable_end}=    Get Current Date     result_format='%Y-%m-%d ${mdc_configuration.same_day_disable_time_end}'
    ${is_same_day_not_available}=    Run Keyword And Return Status    Should Be True    ${same_day_disable_start}<=${current_date_time}<=${same_day_disable_end}
    [Return]    ${is_same_day_not_available}

Get status next day shipping is unavailable
    ${current_date_time}=    Get Current Date     result_format='%Y-%m-%d %H:%M:%S'
    ${next_day_disable_start}=    Get Current Date     result_format='%Y-%m-%d ${mdc_configuration.next_day_disable_time_start}'
    ${next_day_disable_end}=    Get Current Date     result_format='%Y-%m-%d ${mdc_configuration.next_day_disable_time_end}'
    ${is_next_day_not_available}=    Run Keyword And Return Status    Should Be True    ${next_day_disable_start}<=${current_date_time}<=${next_day_disable_end}
    [Return]    ${is_next_day_not_available}

# delivery option - home delivery
Click home delivery option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.opt_home_delivery}
    loader_common_fragment.Wait until page loader is not visible

Click standard delivery option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.opt_standard_delivery}
    loader_common_fragment.Wait until page loader is not visible

Click same day delivery option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.opt_same_day_delivery}
    loader_common_fragment.Wait until page loader is not visible

Click next day delivery option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.opt_next_day_delivery}
    loader_common_fragment.Wait until page loader is not visible

# delivery option - click and collect
Click click and collect option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.opt_click_and_collect}
    loader_common_fragment.Wait until page loader is not visible

Click store
    [Arguments]    ${store}
    ${elem}=    CommonKeywords.Format Text    ${dict_delivery_page.btn_select_store}    $store=${store}
    CommonWebKeywords.Scroll And Click Element    ${elem}
    loader_common_fragment.Wait until page loader is not visible

Click standard pickup option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.opt_standard_pickup}
    loader_common_fragment.Wait until page loader is not visible

# guest customer information
Input customer info first name
    [Arguments]    ${first_name}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_customer_firstname}    ${first_name}

Input customer info last name
    [Arguments]    ${last_name}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_customer_lastname}    ${last_name}

Input customer info email
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_customer_email}    ${email}

Input customer info phone number
    [Arguments]    ${phone_number}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_customer_phone}    ${phone_number}

# guest shipping information
Input shipping address
    [Arguments]    ${address}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_shipping_address}    ${address}

Input shipping post code
    [Arguments]    ${post_code}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_post_code}    ${post_code}
    loader_common_fragment.Wait until page loader is not visible

# member customer information
Member input customer info first name
    [Arguments]    ${first_name}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_first_name_member}    ${first_name}

Member input customer info last name
    [Arguments]    ${last_name}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_last_name_member}    ${last_name}

Member input customer info phone number
    [Arguments]    ${phone}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_phone_member}    ${phone}

Member input shipping address
    [Arguments]    ${address}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_address_member}    ${address}

Member input shipping post code
    [Arguments]    ${post_code}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_delivery_page.txt_post_code_member}    ${post_code}

# tax invoice
Click request tax invoice
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.chk_request_tax_invoice}
    loader_common_fragment.Wait until page loader is not visible

Click 3 hour delivery option
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.rdo_3_hour_delivery}
    Wait until page is completely loaded

Guest, click pin location button
    CommonWebKeywords.Scroll And Click Element    ${dict_delivery_page.btn_pin_location_guest}
    Wait Until Page Loader Is Not Visible

Guest, verify pin location is active
    ${cursor}=    Get Web Element CSS Property Value    ${dict_delivery_page.btn_pin_location_guest}    cursor
    Should be Equal As Strings    ${cursor}    pointer

Guest, verify pin location is not active
    ${cursor}=    Get Web Element CSS Property Value    ${dict_delivery_page.btn_pin_location_guest}    cursor
    Should be Equal As Strings    ${cursor}    not-allowed

Verify home delivery shipping option is not selected
    Verify Web Elements Are Not Visible    ${dict_delivery_page.rdo_main_package_products}    ${dict_delivery_page.rdo_pin_location_require}

Guest, get and set shipping address information
    ${province}=    SeleniumLibrary.Get Selected List Label   ${dict_delivery_page.sel_address_province_guest}
    ${district}=    SeleniumLibrary.Get Selected List Label   ${dict_delivery_page.sel_address_district_guest}
    ${sub_district}=    SeleniumLibrary.Get Selected List Label   ${dict_delivery_page.sel_address_subdistrict_guest}
    Set Test Variable    ${province_storage}    ${province}
    Set Test Variable    ${district_storage}    ${district}
    Set Test Variable    ${sub_district_storage}    ${sub_district}

Member, get and set shipping address information
    ${province}=    SeleniumLibrary.Get Selected List Label    ${dict_delivery_page.sel_address_province}
    ${district}=    SeleniumLibrary.Get Selected List Label   ${dict_delivery_page.sel_address_district}
    ${sub_district}=    SeleniumLibrary.Get Selected List Label   ${dict_delivery_page.sel_address_subdistrict}
    Set Test Variable    ${province_storage}    ${province}
    Set Test Variable    ${district_storage}    ${district}
    Set Test Variable    ${sub_district_storage}    ${sub_district}

Verify pin your location title is display
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_delivery_page.lbl_pin_you_location}

Verify map pin your location address is display
    ${value}=    Get Value    ${dict_delivery_page.txt_pin_your_location}
    ${is_bangkok}=    Run Keyword If    "${province_storage}" == "${region.bkk}"   Run Keyword And Return Status    Should Contain    ${value}    ${province_storage}
    ...               ELSE    Should Contain    ${value}    ${province_storage}
    # If province_storage == Bangkok but use full name of bangkok
    Run Keyword If    '${is_bangkok}'!='${TRUE}'    Should Contain    ${value}    ${region.full_bkk}
    Should Contain    ${value}    ${district_storage}
    Should Contain    ${value}    ${sub_district_storage}

Click pin location confirm button
    CommonWebKeywords.Click Element    ${dict_delivery_page.btn_continue_pin_location}
    Wait Until Element Is Not Visible    ${dict_delivery_page.btn_continue_pin_location}    timeout=${GLOBALTIMEOUT}

Guest, get latitude and longitude
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_delivery_page.lbl_latitude}
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_delivery_page.lbl_longitude}
    ${latitude}=    SeleniumLibrary.Get Element Attribute    ${dict_delivery_page.lbl_latitude}    value
    ${longitude}=    SeleniumLibrary.Get Element Attribute    ${dict_delivery_page.lbl_longitude}    value
    [Return]    ${latitude}    ${longitude}

Verify 3 hour delivery option display
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_delivery_page.lbl_3_hour_delivery}

Verify 3 hour delivery option not display
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_delivery_page.lbl_3_hour_delivery}

Verify pin location is not active with default address
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_delivery_page.lbl_pin_location}    ${checkout_dto.no_pin_location_added}

Verify pin location require for 3 hour delivery
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_delivery_page.lbl_require_3_hour}    ${checkout_dto.require_3_hour_delivery}

Click pin location require for 3 hour delivery
    CommonWebKeywords.Click Element    ${dict_delivery_page.lbl_require_3_hour}

Get 3 hour delivery shipping price
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_delivery_page.lbl_3_hours_delivery_price}
    ${price}=    SeleniumLibrary.Get Text    ${dict_delivery_page.lbl_3_hours_delivery_price}
    ${price}=    CommonKeywords.Convert price to number format  ${price}
    [Return]    ${price}

Verify 3 hour delivery shipping price are display correctly
    [Arguments]    ${3_hour_delivery_price}
    ${shipping_price}=    Get 3 hour delivery shipping price
    Should Be True    ${shipping_price}==${3_hour_delivery_price}

Verify the continue payment button enabled
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_delivery_page.btn_continue_to_payment}
    FOR    ${idx}    IN RANGE    ${${GLOBALTIMEOUT}/6}
        ${cursor}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dict_delivery_page.btn_continue_to_payment}    cursor
        Sleep    0.3 sec
        Exit For Loop If    '${cursor}'=='pointer'
    END
    Should Be Equal As Strings    ${cursor}    pointer

Verify the continue payment button disabled
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_delivery_page.btn_continue_to_payment}
    ${cursor}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dict_delivery_page.btn_continue_to_payment}    cursor
    Should Be Equal As Strings    ${cursor}    not-allowed

Guest, verify pin location address is display correctly
    ${is_bangkok}=    Run Keyword If    "${province_storage}" == "${region.bkk}"   Run Keyword And Return Status    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_shipping_address_guest}    ${province_storage}
    ...               ELSE    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_shipping_address_guest}    ${province_storage}
    # If province_storage == Bangkok but use full name of bangkok
    Run Keyword If    '${is_bangkok}'!='${TRUE}'    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_shipping_address_guest}    ${region.full_bkk}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_shipping_address_guest}    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_shipping_address_guest}    ${sub_district_storage}

Guest, verify pin location address is display correctly when selected 3 hour delivery option
    ${is_bangkok}=    Run Keyword If    "${province_storage}" == "${region.bkk}"   Run Keyword And Return Status    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_3_hour_shipping_address}    ${province_storage}
    ...               ELSE    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_3_hour_shipping_address}    ${province_storage}
    # If province_storage == Bangkok but use full name of bangkok
    Run Keyword If    '${is_bangkok}'!='${TRUE}'    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_3_hour_shipping_address}    ${region.full_bkk}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_3_hour_shipping_address}    ${district_storage}
    CommonWebKeywords.Verify Web Element Text Should Contain    ${dict_delivery_page.lbl_3_hour_shipping_address}    ${sub_district_storage}

Click add address book button
    CommonWebKeywords.Click Element    ${dict_delivery_page.btn_address_book}

Click save address button
    CommonWebKeywords.Click Element    ${dict_delivery_page.btn_save_address}

Click choose last address list
    CommonWebKeywords.Click Element    ${dict_delivery_page.lbl_last_address}

Click use selected address button
    CommonWebKeywords.Click Element    ${dict_delivery_page.btn_use_selected_address}

Click change address button
    CommonWebKeywords.Click Element    ${dict_delivery_page.btn_change_address}
    
Click add address button in popup 
    CommonWebKeywords.Click Element    ${dict_delivery_page.btn_add_address_popup}