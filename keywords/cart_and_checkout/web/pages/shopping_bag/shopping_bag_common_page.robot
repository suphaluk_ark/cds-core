*** Keywords ***
Click secure checkout button
    ${status}=    Run Keyword And Return Status    SeleniumLibrary.Wait Until Element Is Visible    ${dict_shopping_bag_page.btn_close_popup}    ${GLOBALTIMEOUT}
    Run Keyword If    ${status} == ${TRUE}    CommonWebKeywords.Click Element    ${dict_shopping_bag_page.btn_close_popup}
    CommonWebKeywords.Scroll And Click Element    ${dict_shopping_bag_page.btn_secure_checkout}
    loader_common_fragment.Wait until page loader is not visible

Input coupon code
    [Arguments]    ${coupon_code}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_shopping_bag_page.txt_apply_coupon}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_shopping_bag_page.txt_apply_coupon}    ${coupon_code}

Click apply coupon button
    CommonWebKeywords.Scroll And Click Element    ${dict_shopping_bag_page.btn_apply_coupon}
    loader_common_fragment.Wait until page loader is not visible

Verify coupon code display in cart
    [Arguments]    ${coupon_code}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_coupon_code_display_at_cart}    $coupon_code=${coupon_code}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify e-coupon discount in cart page(Baht)
    [Arguments]    ${coupon_code}    ${total_price}    ${discount}
    calculation.Calculate discount(Bath) from original total price    ${total_price}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_coupon_discount}    $coupon_code=${coupon_code}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    -฿${discount_amount_with_format}

Verify e-coupon discount in cart page(%)
    [Arguments]    ${coupon_code}    ${total_price}    ${discount}
    calculation.Calculate discount(%) from original total price    ${total_price}    ${discount}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_coupon_discount}    $coupon_code=${coupon_code}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${elem}    -฿${discount_amount_with_format}

Click 'free gift with this item purchase' link
    [Arguments]    ${purchase_item_sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_free_gift_with_this_item_purchase}    $purchase_item_sku=${purchase_item_sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify 'free gift with this item purchase' should be displayed correctly
    [Arguments]    ${purchase_item_sku}    ${free_gift_product_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_free_gift_with_this_item_purchase_product}    $purchase_item_sku=${purchase_item_sku}    $free_gift_product_name=${free_gift_product_name}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Click 'free items with order purchase' link
    CommonWebKeywords.Scroll And Click Element    ${dict_shopping_bag_page.lbl_free_items_with_order_purchase}

Verify 'free items with order purchase' should be displayed correctly
    [Arguments]    ${product_sku}    ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_free_items_with_order_purchase_product}    $product_sku=${product_sku}    $product_name=${product_name}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Verify delete product button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_shopping_bag_page.btn_delete_product}

Verify delete product button should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_shopping_bag_page.btn_delete_product}