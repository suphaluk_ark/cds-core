*** Variables ***
&{dict_shopping_bag_page}
...    btn_secure_checkout=xpath=(//a[@id='lnk-viewCheckout']//div[text()='${shopping_bag_dto.btn_secure_checkout}'])[1]
...    lbl_quantity=xpath=(//*[@id='sel-addQty-{$product_sku}']//span)[2]
...    txt_apply_coupon=xpath=//input[@placeholder='${shopping_bag_dto.txt_apply_coupon}']
...    btn_apply_coupon=xpath=//*[@id='btn-addCouponCode']
...    lbl_coupon_code_display_at_cart=xpath=//*[@id='cart-summary']//*[contains(text(),'{$coupon_code}')]
...    lbl_coupon_discount=xpath=//*[text()='{$coupon_code}']/..//div
...    lbl_free_gift_with_this_item_purchase=xpath=//*[./child::*/img[@data-pid="{$purchase_item_sku}"]]/following-sibling::*//*[text()='${shopping_bag_dto.lbl_free_gift}']
...    lbl_free_gift_with_this_item_purchase_product=xpath=//*[./child::*/img[@data-pid="{$purchase_item_sku}"]]/following-sibling::*//*[text()='{$free_gift_product_name}']
...    lbl_free_items_with_order_purchase=//*[text()='${shopping_bag_dto.lbl_promo_item_with_order_purchase}' and .//img[contains(@src,'ic-gift-red')]]
...    lbl_free_items_with_order_purchase_product=(//*[text()='${shopping_bag_dto.lbl_free_items}']/parent::*//*[@data-product-id="{$product_sku}"][contains(text(),"{$product_name}")])[1]
...    btn_delete_product=xpath=//div[starts-with(@id,'btn-removeProduct')]
...    btn_close_popup=css=#btn-popup-close svg

*** Keywords ***
Get product quantity by prodcut sku
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_shopping_bag_page.lbl_quantity}    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${quantity}=    SeleniumLibrary.Get Text    ${elem}
    [Return]    ${quantity}
