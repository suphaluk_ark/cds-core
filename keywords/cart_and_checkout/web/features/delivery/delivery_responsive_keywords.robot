*** Keywords ***
Member, home delivery with default address, standard delivery
    delivery_common_page.Click home delivery option
    delivery_common_page.Click standard delivery option

Member, home delivery with default address, express delivery
    delivery_common_page.Click home delivery option
    delivery_responsive_keywords.Click express delivery option following mdc configuration

Memeber, click and collect, standard pickup
    [Arguments]    ${store}
    delivery_common_page.Click click and collect option
    delivery_common_page.Click store    ${store}
    delivery_common_page.Click standard pickup option

Member, input customer contact information
    [Arguments]    ${first_name}    ${last_name}    ${phone}
    delivery_common_page.Member input customer info first name      ${first_name}
    delivery_common_page.Member input customer info last name       ${last_name}
    delivery_common_page.Member input customer info phone number    ${phone}

Member, input shipping address information
    [Arguments]    ${shipping_address}    ${post_code}
    delivery_common_page.Member input shipping address      ${shipping_address}
    delivery_common_page.Member input shipping post code    ${post_code}

Member, add new address from address book without address list in delivery page
    [Arguments]    ${first_name}    ${last_name}    ${phone}    ${shipping_address}    ${post_code}
    delivery_common_page.Click add address book button
    delivery_responsive_keywords.Member, input customer contact information    ${first_name}    ${last_name}    ${phone}
    delivery_responsive_keywords.Member, input shipping address information    ${shipping_address}    ${post_code}
    delivery_common_page.Member, get and set shipping address information
    delivery_common_page.Click save address button

Member, change address from address book with default address in delivery page
    [Arguments]    ${first_name}    ${last_name}    ${phone}    ${shipping_address}    ${post_code}
    delivery_common_page.Click change address button
    delivery_common_page.Click add address button in popup 
    delivery_responsive_keywords.Member, input customer contact information    ${first_name}    ${last_name}    ${phone}
    delivery_responsive_keywords.Member, input shipping address information    ${shipping_address}    ${post_code}
    delivery_common_page.Click save address button

Member, choose last address from address book in delivery page
    delivery_common_page.Click choose last address list
    delivery_common_page.Click use selected address button

Guest, home delivery, standard delivery
    [Arguments]    ${first_name}=${guest_data_01.information.firstname}
    ...    ${last_name}=${guest_data_01.information.lastname}
    ...    ${email}=${guest_data_01.email}
    ...    ${phone_number}=${guest_data_01.information.tel}
    ...    ${shipping_address}=${guest_data_01.information.shipping_address}
    ...    ${shipping_post_code}=${guest_data_01.information.shipping_post_code}
    delivery_responsive_keywords.Guest, input customer infomation    ${first_name}    ${last_name}    ${email}    ${phone_number}
    delivery_common_page.Click home delivery option
    delivery_responsive_keywords.Guest, input shipping address information    ${shipping_address}    ${shipping_post_code}
    delivery_common_page.Click standard delivery option

Guest, input customer infomation
    [Arguments]    ${first_name}=${guest_data_01.information.firstname}
    ...    ${last_name}=${guest_data_01.information.lastname}
    ...    ${email}=${guest_data_01.email}
    ...    ${phone_number}=${guest_data_01.information.tel}
    delivery_common_page.Input customer info first name    ${first_name}
    delivery_common_page.Input customer info last name    ${last_name}
    delivery_common_page.Input customer info email    ${email}
    delivery_common_page.Input customer info phone number    ${phone_number}

Request tax invoice with default address
    delivery_common_page.Click request tax invoice
    common_keywords.Wait until page is completely loaded

Click express delivery option following mdc configuration
    ${is_same_day_not_available}    Get status same day shipping is unavailable
    ${is_next_day_not_available}    Get status next day shipping is unavailable

    Run Keyword If    ${is_same_day_not_available} and ${is_next_day_not_available}    BuiltIn.Fail    msg=Express shipping is unavailable
    ...    ELSE IF    ${is_same_day_not_available}    delivery_common_page.Click next day delivery option
    ...    ELSE    delivery_common_page.Click same day delivery option

Guest, input shipping address information
    [Arguments]    ${shipping_address}    ${post_code}
    delivery_common_page.Input shipping address      ${shipping_address}
    delivery_common_page.Input shipping post code    ${post_code}
    common_keywords.Wait until page is completely loaded

Verify pin your location popup is display
    delivery_common_page.Verify pin your location title is display
    delivery_common_page.Verify map pin your location address is display

Guest, pin location address display correctly after pin location
    delivery_common_page.Guest, verify pin location address is display correctly
    delivery_common_page.Click 3 hour delivery option
    delivery_common_page.Guest, verify pin location address is display correctly when selected 3 hour delivery option
