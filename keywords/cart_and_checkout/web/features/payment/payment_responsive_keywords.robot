*** Keywords ***
Redeem point
    [Arguments]    ${point}    ${t1c_email}=${t1c_information_01.email}    ${t1c_pwd}=${t1c_information_01.password}
    payment_common_page.Click connect to T1C link
    payment_common_page.Input T1C email     ${t1c_email}
    payment_common_page.Input T1C password    ${t1c_pwd}
    payment_common_page.Click T1C login button
    payment_common_page.Click redeem list
    payment_common_page.Select redeem for discount
    payment_common_page.Input T1C point    ${point}
    payment_common_page.Click T1C apply button
    loader_common_fragment.Wait until page loader is not visible
    payment_common_page.Verify redeem link should be displayed

Input credit card
    [Arguments]    ${credit_card_name}
    ...    ${credit_card_no}
    ...    ${credit_card_expiry_date}
    ...    ${credit_card_cvv}
    payment_common_page.Input credit card name    ${credit_card_name}
    payment_common_page.Input credit card number    ${credit_card_no}
    payment_common_page.Input credit card expiry date    ${credit_card_expiry_date}
    payment_common_page.Input credit card cvv    ${credit_card_cvv}

Payment, credit card full payment successfully
    [Arguments]    ${credit_card_name}=${credit_card_data.holder}
    ...    ${credit_card_no}=${credit_card_data.number}
    ...    ${credit_card_expiry_date}=${credit_card_data.month}${credit_card_data.year}
    ...    ${credit_card_cvv}=${credit_card_data.cvv}
    ...    ${is_save_card}=${False}
    payment_common_page.Click credit card, debit cart option 
    payment_common_page.Select iframe credit card
    Input credit card    ${credit_card_name}    ${credit_card_no}    ${credit_card_expiry_date}    ${credit_card_cvv}
    SeleniumLibrary.Unselect Frame
    
    Run Keyword If    ${is_save_card}   payment_common_page.Select save card    ELSE    payment_common_page.Unselect save card

    payment_common_page.Click pay now button
    payment_responsive_keywords.Proceed OTP

Payment, existing credit card full payment successfully
    [Arguments]    ${credit_card_cvv}=${credit_card_data.cvv}
    payment_common_page.Click credit card, debit cart option
    payment_common_page.Input default credit card cvv    ${credit_card_cvv}
    payment_common_page.Click pay now button
    payment_responsive_keywords.Proceed OTP

Payment, credit card full payment unsuccessfully
    [Arguments]    ${credit_card_name}=${credit_card_data.holder}
    ...    ${credit_card_no}=${credit_card_data.number}
    ...    ${credit_card_expiry_date}=${credit_card_data.month}${credit_card_data.year}
    ...    ${credit_card_cvv}=${credit_card_data.cvv}
    payment_common_page.Click credit card, debit cart option 

    payment_common_page.Select iframe credit card
    Input credit card    ${credit_card_name}    ${credit_card_no}    ${credit_card_expiry_date}    ${credit_card_cvv}
    SeleniumLibrary.Unselect Frame

    Run Keyword If    ${FALSE}   payment_common_page.Select save card    ELSE    payment_common_page.Unselect save card
    payment_common_page.Click pay now button

    2c2p_common_page.Click cancel payment
    common_keywords.Wait until page is completely loaded

Payment, credit card installment successfully
    [Arguments]    ${bank}    
    ...    ${installment_month_plan}
    ...    ${credit_card_name}=${credit_card_data.holder}
    ...    ${credit_card_no}=${credit_card_data.number}
    ...    ${credit_card_expiry_date}=${credit_card_data.month}${credit_card_data.year}
    ...    ${credit_card_cvv}=${credit_card_data.cvv}
    payment_common_page.Click installment option 
    payment_common_page.Select installment bank    ${bank}
    payment_common_page.Select installment month plan    ${installment_month_plan}
    payment_common_page.Select iframe credit card installment
    Input credit card    ${credit_card_name}    ${credit_card_no}    ${credit_card_expiry_date}    ${credit_card_cvv}
    SeleniumLibrary.Unselect Frame
    payment_common_page.Click pay now button
    payment_responsive_keywords.Proceed OTP

Proceed OTP
    ${otp}=    2c2p_common_page.Get OTP from 2c2p page
    2c2p_common_page.Input OTP    ${otp}
    2c2p_common_page.Select proceed button
    2c2p_common_page.Click return to merchant now

Payment, bank transfer
    [Arguments]    ${bank_name}=${bank_transfer_data.kbank.name}    ${payment_channel}=${bank_transfer_data.kbank.channel}    ${tel}=${e2e_customer_data_03.information.tel}
    payment_common_page.Click bank transfer, service counter option
    payment_common_page.Select bank, service counter    ${bank_name}
    payment_common_page.Select payment channel    ${payment_channel}    # 22 Jan 2021 - CNC-632: Removed payment channel
    payment_common_page.Input phone number    ${tel}
    payment_common_page.Click pay now button
    bank_transfer_common_page.Payment code should be displayed
    bank_transfer_common_page.Click return to merchant button

Payment, cash on delivery
    payment_common_page.Click cash on delivery option
    payment_common_page.Click pay now button

Payment, pay at store
    payment_common_page.Click pay at store option
    payment_common_page.Click pay now button

Payment, Full redeem point
    [Arguments]    ${t1c_email}=${t1c_information_01.email}    ${t1c_pwd}=${t1c_information_01.password}
    payment_common_page.Click connect to T1C link
    payment_common_page.Input T1C email     ${t1c_email}
    payment_common_page.Input T1C password    ${t1c_pwd}
    payment_common_page.Click T1C login button
    payment_common_page.Click redeem list
    payment_common_page.Select full redeem point
    payment_common_page.Click T1C apply button
    loader_common_fragment.Wait until loader t1c is not visible
    loader_common_fragment.Wait until page loader is not visible
    payment_common_page.Click pay now full redeem button