*** Keywords ***
Verify place order successfully, order total should be displayed correctly
    [Arguments]    ${price}    ${discount}=${0}    ${redeem}=${0}    ${shipping}=${0}
    ${price}=    Convert To Number    ${price}
    calculation.Summarize price with discount, T1redeem and shipping charge    ${price}    ${discount}    ${redeem}    ${shipping}
    CommonWebKeywords.Scroll To Element    ${dict_thank_you_page.lbl_order_total}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_thank_you_page.lbl_order_total}    ฿${order_total_format}

Verify full taxt invoice should be displayed correctly
    [Arguments]    ${full_address}    ${invoice_id}
    thank_you_common_page.Verify order tax invoice address should be displayed correctly    ${full_address}
    thank_you_common_page.Verify order tax invoice id should be displayed correctly    ${invoice_id}