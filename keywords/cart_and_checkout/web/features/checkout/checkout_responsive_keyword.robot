*** Keyword ***
Member, login, add product skus to cart and go to checkout page
    [Arguments]    ${email}    ${password}    ${first_name}    ${skus}    ${quantities}
    ${user_token}=    account_management_falcon.Get customer login token  ${email}    ${password}    false
    ${quote_id}=    shopping_cart_api.Member, clear cart and add multi skus to cart    ${user_token}    ${skus}    ${quantities}
    login_responsive_keywords.Login via falcon successfully  ${email}    ${password}    ${first_name}
    common_keywords.Go to direct url    checkout
    common_keywords.Wait until page is completely loaded
    loader_common_fragment.Wait until page loader is not visible
    [Return]    ${user_token}    ${quote_id}

Guest, add product skus to cart and go to checkout page
    [Arguments]    ${lst_split_skus}    ${lst_qtys}
    ${quote_id}=    Wait Until Keyword Succeeds    3 x    1 sec    common_keywords.Get cookie value from browser    guest
    shopping_cart_falcon.Guest, add multi items to cart    ${quote_id}    ${lst_split_skus}    ${lst_qtys}
    common_keywords.Go to direct url    checkout
    common_keywords.Wait until page is completely loaded
    loader_common_fragment.Wait until page loader is not visible
    [Return]    ${quote_id}
