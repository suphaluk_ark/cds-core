*** Keywords ***
Add product to cart, verify mini cart should be displayed correctly
    [Arguments]    ${product_sku}    ${quantity}=${1}
    pdp_common_page.Add product quantity    ${product_sku}    ${quantity}
    pdp_common_page.Click add to cart button    ${product_sku}    
    common_keywords.Wait until page is completely loaded    
    header_common_fragment.Click mini cart button
    header_common_fragment.Verify product sku should displayed on mini cart correctly    ${product_sku}
    header_common_fragment.Click mini cart button

Go to shopping bag page
    header_common_fragment.Click mini cart button
    header_common_fragment.Click view cart button
    loader_common_fragment.Wait until page loader is not visible

Verify total price with quantity, summarize multiple products
    [Arguments]    @{list_sku}
    ${summary_price}=    Create List
    FOR    ${product_sku}    IN    @{list_sku}
        ${quantity}=    shopping_bag_desktop_page.Get product quantity by prodcut sku    ${product_sku}
        Set Test Variable    ${quantity_${product_sku}}    ${quantity}
        ${product_price}=    Convert To Number    ${${product_sku}}[price]
        Summarize price with quantity    ${quantity_${product_sku}}    ${product_price}
        Append to List    ${summary_price}    ${price_with_quantity}
    END
    ${total_price_with_quantity}=    Evaluate    sum($summary_price)
    ${total_price_with_quantity_format}=    Run Keyword If    ${total_price_with_quantity}.is_integer()    Convert To Integer    ${total_price_with_quantity}
    ...    ELSE IF    Convert price to total amount format    ${total_price_with_quantity}
    Set Test Variable    ${total_price_with_quantity}    ${total_price_with_quantity_format}

Apply coupon, verify 'coupon fixed discount' should be applied in cart correctly
    [Arguments]    ${coupon}    ${total_price}    ${discount}
    shopping_bag_common_page.Input coupon code  ${coupon}
    shopping_bag_common_page.Click apply coupon button
    shopping_bag_common_page.Verify coupon code display in cart    ${coupon}
    shopping_bag_common_page.Verify e-coupon discount in cart page(Baht)    ${coupon}    ${total_price}   ${discount}

Apply coupon, verify 'coupon percent discount' should be applied in cart correctly
    [Arguments]    ${coupon}    ${total_price}    ${discount}
    shopping_bag_common_page.Input coupon code  ${coupon}
    shopping_bag_common_page.Click apply coupon button
    shopping_bag_common_page.Verify coupon code display in cart    ${coupon}
    shopping_bag_common_page.Verify e-coupon discount in cart page(%)    ${coupon}    ${total_price}    ${discount}

Verify free gift with this item purchase should be display correctly
    [Arguments]    ${purchase_item_sku}    @{list_free_items_sku}
    shopping_bag_common_page.Click 'free gift with this item purchase' link    ${purchase_item_sku}
    FOR   ${free_item_sku}   IN    @{list_free_items_sku}
        ${free_gift_product_name}=    Set Variable    ${${free_item_sku}}[name]
        shopping_bag_common_page.Verify 'free gift with this item purchase' should be displayed correctly    ${purchase_item_sku}    ${free_gift_product_name}
    END

Verify free items with order purchase should be displayed correctly
    [Arguments]    @{list_free_items_sku}
    shopping_bag_common_page.Click 'free items with order purchase' link
    FOR   ${free_item_sku}   IN    @{list_free_items_sku}
        ${sku}=     Set Variable    ${${free_item_sku}}[sku]
        ${name}=    Set Variable    ${${free_item_sku}}[name]
        shopping_bag_common_page.Verify 'free items with order purchase' should be displayed correctly   ${sku}    ${name}
    END

Remove all products in shopping cart
    ${is_produtcs_in_cart}    Run Keyword And Return Status    header_common_fragment.Verify number total of products should be displayed in mini cart
    Return From Keyword If    not (${is_produtcs_in_cart})
    shopping_bag_desktop_keywords.Go to shopping bag page
    
    shopping_bag_common_page.Verify delete product button should be displayed
    ${total_btn_delete}    SeleniumLibrary.Get Element Count    ${dict_shopping_bag_page.btn_delete_product}
    FOR    ${ele}    IN RANGE    ${0}    ${total_btn_delete}
        ${btn_delete_product_by_sku}    SeleniumLibrary.Get Element Attribute    ${dict_shopping_bag_page.btn_delete_product}    id
        CommonWebKeywords.Click Element    ${btn_delete_product_by_sku}
        loader_common_fragment.Wait until page loader is not visible
        CommonWebKeywords.Verify Web Elements Are Not Visible    ${btn_delete_product_by_sku}
    END
    shopping_bag_common_page.Verify delete product button should not be displayed

Remove all products in mini cart on header
    ${is_produtcs_in_cart}    Run Keyword And Return Status    header_common_fragment.Verify number total of products should be displayed in mini cart
    Return From Keyword If    not (${is_produtcs_in_cart})
    header_common_fragment.Click mini cart button
    header_desktop_fragment.Verify delete product button should be displayed

    ${total_btn_delete}    SeleniumLibrary.Get Element Count    ${dict_header_fragment.btn_delete_product_mini_cart}
    FOR    ${ele}    IN RANGE    ${0}    ${total_btn_delete}
        ${btn_delete_product_by_sku}    SeleniumLibrary.Get Element Attribute    ${dict_header_fragment.btn_delete_product_mini_cart}    id
        CommonWebKeywords.Click Element    ${btn_delete_product_by_sku}
        CommonWebKeywords.Verify Web Elements Are Not Visible    ${btn_delete_product_by_sku}
    END
    header_desktop_fragment.Verify delete product button should not be displayed
    header_common_fragment.Click mini cart button