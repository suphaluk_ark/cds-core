*** Variables ***
&{dict_header_fragment}
...    mnu_hamburger=xpath=//a[@data-testid='mnu-viewMenu-hamburger']
...    lbl_login_register=xpath=//label[contains(text(),'${login_signup_dto.lbl_login_register}')]
...    lbl_account_name=xpath=//*[./following-sibling::*/*[starts-with(@data-testid, 'mnu-viewMenuOnmobile')]]//*[text()="{$account_name}"]
...    btn_mnu_hamburger_x=xpath=(//*[@id='layout']/div)[4]/*[@viewBox]
...    txt_email=xpath=//*[@name='customerEmail']
...    txt_password=xpath=//*[@name='customerPassword']
...    btn_login=xpath=//*[@type='submit']
...    lnk_register_now=xpath=//a[text()='${login_signup_dto.lnk_register_now}']
...    btn_x_on_search_bar=xpath=(//input[@id='txt-searchProductOnSearchBar'])[2]/following-sibling::*/*[@viewBox]
...    txt_search_bar=xpath=(//input[@id='txt-searchProductOnSearchBar'])[2]
...    lbl_product_sku_on_search_suggestion=//*[@data-testid="inf-viewSearchPopupContainer"]//*[@id="inf-viewProductOnProductSearchPreview-{$product_sku}"]
...    lbl_product_sku_on_mini_cart=(//a[@id='lnk-viewProductNameOnMiniCart-{$product_sku}'])[2]
...    btn_mini_cart=xpath=//a[@to='/cart']
...    btn_view_cart=xpath=//*[@id='lnk-viewCartOnMiniCart']/div
...    lbl_hello_first_name=xpath=//*[starts-with(@data-testid, 'mnu-viewMenu')][1]/../preceding-sibling::*/div
...    lbl_sign_out=xpath=//div[contains(text(),'${login_signup_dto.lbl_sign_out}')]
...    login_register_lbl=xpath=//label[contains(text(),'${login_signup_dto.lbl_login_register}')]
...    lbl_required_email=xpath=//input[@name='email']//../div/div[2]
...    lbl_required_password=xpath=//input[@name='password']//../div/div[2]
...    lbl_err_login=xpath=(//a[@to='/user/forgot-password']//preceding::div//label)[2]
...    lbl_welcome=xpath=(//div[starts-with(text(),'${success_message_dto.login.welcome}')])[2]
...    btn_facebook_login=xpath=//*[contains(text(),"${login_signup_dto.btn_login_facebook}")]
...    lbl_number_total_products_mini_cart=xpath=//*[@to="/cart"]//span
...    btn_back=xpath=//span[text()='${web_common.back}']
...    lbl_product_mini_cart=xpath=(//label[@data-product-id='{$skunumber}' and @data-product-name="{$product_name}"])[2]

*** Keywords ***
Click hamburger menu
    CommonWebKeywords.Click Element    ${dict_header_fragment.mnu_hamburger}

Click x button on hamburger menu
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_mnu_hamburger_x}

Click on Hi First Name label
    CommonWebKeywords.Click Element    ${dict_header_fragment.lbl_hello_first_name}

Click on Sign Out
    CommonWebKeywords.Click Element    ${dict_header_fragment.lbl_sign_out}

Click on Login/Register label
    CommonWebKeywords.Click Element     ${dict_header_fragment.lbl_login_register}

Click Facebook login Button
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_facebook_login}

Email Field Display Error Message
    [Arguments]    ${expect_message}
    #username field index = 1, password field index =2
    Verify Web Element Text Should Be Equal    ${dict_header_fragment.lbl_required_email}    ${expect_message}

Password field Should Be Visible Error
    [Arguments]    ${expect_message}
    #username field index = 1, password field index =2
    Verify Web Element Text Should Be Equal    ${dict_header_fragment.lbl_required_password}    ${expect_message}

Verify Error Message After Login Failed
    [Arguments]    ${expect_message}
    Verify Web Element Text Should Be Equal    ${dict_header_fragment.lbl_err_login}    ${expect_message}

Verify display name with welcome message should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_header_fragment.lbl_welcome}

Click on Back button
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_back}