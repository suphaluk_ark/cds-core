*** Keywords ***
Click login, register label
    CommonWebKeywords.Click Element    ${dict_header_fragment.lbl_login_register}

Click register now link
    CommonWebKeywords.Click Element    ${dict_header_fragment.lnk_register_now}

Input email
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_header_fragment.txt_email}    ${email}

Input password
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_header_fragment.txt_password}    ${password}

Click login button
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_login}
    common_keywords.Wait Until Page Is Completely Loaded

Click on account name label
    [Arguments]    ${display_name}
    ${elem}=    CommonKeywords.Format Text  ${dict_header_fragment.lbl_account_name}    $account_name=${display_name}
    CommonWebKeywords.Click Element    ${elem}

Click on Log out link
    CommonWebKeywords.Click Element    ${dict_header_fragment.lnk_log_out}

Verify register label should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_header_fragment.lbl_login_register}

Verify display name should be displayed correctly
    [Arguments]    ${display_name}
    ${elem}=    CommonKeywords.Format Text  ${dict_header_fragment.lbl_account_name}    $account_name=${display_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Input text on search bar
    [Arguments]    ${text}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_header_fragment.txt_search_bar}    ${text}

Click x button on search bar
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_x_on_search_bar}

Press Enter key on search bar
    SeleniumLibrary.Press Keys    None    RETURN
    common_keywords.Wait until page is completely loaded

Click product on search suggestion by product sku
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text  ${dict_header_fragment.lbl_product_sku_on_search_suggestion}    $product_sku=${product_sku}
    CommonWebKeywords.Click Element    ${elem}

Verify product sku should displayed on mini cart correctly
    [Arguments]     ${product_sku}
    ${elem}=    CommonKeywords.Format Text  ${dict_header_fragment.lbl_product_sku_on_mini_cart}    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}

Click mini cart button
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_mini_cart}

Click view cart button
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_view_cart}

Verify homepage page should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_home_page.home_page_title} 

Click login with facebook button
    CommonWebKeywords.Click Element    ${dict_header_fragment.btn_facebook_login}

Verify number total of products should be displayed in mini cart
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_header_fragment.lbl_number_total_products_mini_cart}

Email Field Display Error Message
    [Arguments]     ${expect_message}
    Verify Web Element Text Should Be Equal    ${dict_header_fragment.lbl_required_email}    ${expect_message}

Password Field Display Error Message
    [Arguments]     ${expect_message}
    Verify Web Element Text Should Be Equal    ${dict_header_fragment.lbl_required_password}    ${expect_message}

Verify Error Message After Login Failed
    [Arguments]    ${expect_message}
    Verify Web Element Text Should Be Equal    ${dict_header_fragment.lbl_err_login}    ${expect_message}

Verify Error Message Not Visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_header_fragment.lbl_err_login}

View cart button should not be visible
    Seleniumlibrary.Wait Until Element Is Not Visible   ${dict_header_fragment.btn_view_cart}

Product should be displayed in mini cart
    [Arguments]     ${sku_number}   ${product_name}
    ${elem}=    CommonKeywords.Format Text  ${dict_header_fragment.lbl_product_mini_cart}    $skunumber=${sku_number}    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible   ${elem}