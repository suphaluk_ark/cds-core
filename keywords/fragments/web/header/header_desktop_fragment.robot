*** Variables ***
&{dict_header_fragment}
...    lbl_login_register=xpath=//*[@id='login-button']
...    lbl_account_name=xpath=//a[@id="btn-viewCustomerMenuOnMainHeader"]//*[contains(text(),"{$account_name}")]
...    txt_email=xpath=//*[@data-testid='inp-formMiniLoginView-EmailCustomer']
...    txt_password=xpath=//*[@data-testid='inp-formMiniLoginView-PasswordCustomer']
...    btn_login=xpath=//*[@type='submit']
...    lnk_register_now=xpath=//a[text()='${login_signup_dto.lnk_register_now}']
...    btn_x_on_search_bar=xpath=(//*[@id='txt-searchProductOnSearchBar'])[1]/following-sibling::*/*[@viewBox]
...    txt_search_bar=xpath=(//*[@id='txt-searchProductOnSearchBar'])[1]
...    lbl_product_sku_on_search_suggestion=//*[@data-testid="inf-viewSearchPopupContainer"]//*[@id="inf-viewProductOnProductSearchPreview-{$product_sku}"]
...    lbl_product_sku_on_mini_cart=//a[@id='lnk-viewProductNameOnMiniCart-{$product_sku}']
...    btn_mini_cart=//*[@id='mini-cart']
...    btn_view_cart=xpath=//*[@id='lnk-viewCartOnMiniCart']/div
...    btn_facebook_login=xpath=//*[contains(text(),"${login_signup_dto.btn_login_facebook}")]
...    lbl_number_total_products_mini_cart=xpath=//*[@id="btn-viewMiniCartOnMainHeader"]/following-sibling::span
...    btn_delete_product_mini_cart=xpath=//*[starts-with(@id,'btn-removeProductOnMiniCart')]
...    lbl_required_email=xpath=//input[@name='email']//../div/div[2]
...    lbl_required_password=xpath=//input[@name='password']//../div/div[2]
...    lbl_err_login=xpath=(//a[@to='/user/forgot-password']//preceding::div//label)[2]
...    lbl_welcome=xpath=//div[text()='${success_message_dto.login.welcome}']
...    lbl_product_mini_cart=xpath=(//label[@data-product-id='{$skunumber}' and @data-product-name="{$product_name}"])[2]

*** Keywords ***
Verify delete product button should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_header_fragment.btn_delete_product_mini_cart}

Verify delete product button should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_header_fragment.btn_delete_product_mini_cart}
