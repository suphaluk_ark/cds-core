*** Keywords ***
Wait until page loader is not visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_loader_fragment.full_page_loader}

Wait until loader progress bar is not visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_loader_fragment.loader_progress_bar}

Wait until loader t1c is not visible
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_loader_fragment.t1c_loader}
