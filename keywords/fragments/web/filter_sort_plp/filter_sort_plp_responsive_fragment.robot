*** Variables ***
&{dict_filter_sort_plp_fragment}
...    btn_filter=xpath=//*[text()='${product_list_page.filters}']
...    btn_price_range_arrow_down=xpath=(//*[@data-testid="price_range"])[2]//img[contains(@src,'arrow-down')]
...    btn_brand_name_arrow_down=xpath=(//*[@id='brand_name']//img[contains(@src,'arrow-down')])[2]
...    btn_color_arrow_down=xpath=(//*[@id='color_group_name'])[2]//img[contains(@src,'arrow-down')]
...    btn_x=xpath=//*[text()='${product_list_page.filters}']/parent::*/*[@viewBox]
...    lbl_price_rage_min=xpath=//*[contains(@class,'input-range__label--min')]
...    lbl_price_rage_max=xpath=//*[contains(@class,'input-range__label--max')]
...    opt_brand_name=xpath=((//*[@id='brand_name'])[2]/following-sibling::*//*[text()="{$text}"])[last()]
...    btn_clear_filter_color=//*[@id="color_group_name"]/following-sibling::*//button[text()='${product_list_page.btn_clear}']
...    opt_color=xpath=(//*[@id='color_group_name'])[2]/following-sibling::*//*[text()='{$text}']
...    lbl_search_result=xpath=//h3[text()='${search_result_page.show_results_for}']/following-sibling::h1[text()="'{$keyword}'"]