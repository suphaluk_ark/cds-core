*** Keywords ***
#About Filter
Click filter price range
    filter_sort_plp_common_fragment.Click filter button
    filter_sort_plp_common_fragment.Verify 'filter price range' should be displayed
    filter_sort_plp_common_fragment.Click on price range arrow-down button

Click filter brand name
    filter_sort_plp_common_fragment.Click filter button
    filter_sort_plp_common_fragment.Verify 'filter brand name' should be displayed
    filter_sort_plp_common_fragment.Click on brand name arrow-down button

Select filter color option
    [Arguments]    ${color}
    filter_sort_plp_common_fragment.Click filter button
    filter_sort_plp_common_fragment.Click on color arrow-down button
    filter_sort_plp_common_fragment.Click on filter color 'clear' button
    filter_sort_plp_common_fragment.Click on a color    ${color}
    filter_sort_plp_common_fragment.Click X button

Click filter color 'clear' button
    filter_sort_plp_common_fragment.Click filter button
    filter_sort_plp_common_fragment.Click on color arrow-down button
    filter_sort_plp_common_fragment.Click on filter color 'clear' button

Click filter button
    CommonWebKeywords.Scroll And Click Element    ${dict_filter_sort_plp_fragment}[btn_filter]

Verify 'filter price range' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_filter_sort_plp_fragment}[btn_price_range_arrow_down]

Click on price range arrow-down button
    CommonWebKeywords.Click Element    ${dict_filter_sort_plp_fragment}[btn_price_range_arrow_down]

Verify 'filter brand name' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_filter_sort_plp_fragment}[btn_brand_name_arrow_down]

Click on brand name arrow-down button
    CommonWebKeywords.Click Element    ${dict_filter_sort_plp_fragment}[btn_brand_name_arrow_down]

Click on color arrow-down button
    CommonWebKeywords.Scroll And Click Element    ${dict_filter_sort_plp_fragment}[btn_color_arrow_down]

Click on filter color 'clear' button
    CommonWebKeywords.Click Element    ${dict_filter_sort_plp_fragment}[btn_clear_filter_color]

Click on a color
    [Arguments]    ${text}
    ${elem}=    CommonKeywords.Format Text    ${dict_filter_sort_plp_fragment}[opt_color]    $text=${text}
    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click X button
    CommonWebKeywords.Scroll And Click Element    ${dict_filter_sort_plp_fragment}[btn_x]

Verify 'filter price range value' should be displayed correctly
    [Arguments]    ${min}    ${max}
    ${min}=    Evaluate    int(${min})
    ${max}=    Evaluate    int(${max})

    ${min}=    Convert price to total amount int format    ${min}
    ${max}=    Convert price to total amount int format    ${max}

    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_filter_sort_plp_fragment}[lbl_price_rage_min]    ฿${min}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_filter_sort_plp_fragment}[lbl_price_rage_max]    ฿${max}

Verify 'filter brand name value' should be displayed correctly by list of brand name
    [Arguments]    ${list_of_brand_name}
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    FOR    ${brand_name}    IN    @{list_of_brand_name}
        Verify 'filter brand name value' should be displayed correctly    ${brand_name}
    END
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Verify 'filter brand name value' should be displayed correctly
    [Arguments]    ${value}
    ${elem}=    CommonKeywords.Format Text    ${dict_filter_sort_plp_fragment}[opt_brand_name]    $text=${value}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'search keyword label' should be displayed correctly
    [Arguments]    ${keyword}
    ${elem}=    CommonKeywords.Format Text    ${dict_filter_sort_plp_fragment}[lbl_search_result]    $keyword=${keyword}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Select sort discount high-low
    Run Keyword And Ignore Error    Disable Testability Automatic Wait
    normal_boutique_brand_plp_common_page.Click on sorter arrow-down button
    filter_sort_plp_common_fragment.Select discount high-low from sorter
    Run Keyword And Ignore Error    Enable Testability Automatic Wait

Select discount high-low from sorter
    normal_boutique_brand_plp_common_page.Select an option from sorter  ${plp_dto.sorter.lbl_discount_high_low}
