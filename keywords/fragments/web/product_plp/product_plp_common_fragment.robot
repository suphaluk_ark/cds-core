*** Keywords ***
Verify 'product found label' should be displayed correctly
    [Arguments]    ${total_product_found}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_total_product_found]    $styles_found=${total_product_found} ${plp_dto.lbl_total_found_item}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product img' should be displayed correctly
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[img_product_with_sku_via_api]    $sku=${sku}
    CommonWebKeywords.Scroll To Element    ${elem}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product brand name label' should be displayed correctly
    [Arguments]    ${sku}   ${product_brand_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_brand_with_sku_via_api]    $sku=${sku}    $brand_name=${product_brand_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product name label' should be displayed correctly
    [Arguments]    ${sku}   ${product_name}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_name_with_sku_via_api]    $sku=${sku}    $product_name=${product_name}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product sell price' should be displayed correctly
    [Arguments]    ${sku}    ${price}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_price_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
    ${price_ui}    SeleniumLibrary.Get Text    ${elem}
    ${price_ui}    CommonKeywords.Convert price to number format    ${price_ui}
    BuiltIn.Should Be Equal As Numbers    ${price_ui}    ${price}

Verify 'product save price' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_save_price_with_sku_via_api]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product save price percent' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_save_price_percent_with_sku_via_api]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product before discount price' should be displayed correctly
    [Arguments]    ${sku}    ${price}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_before_discount_price_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
    ${before_discount_price_ui}    SeleniumLibrary.Get Text    ${elem}
    ${before_discount_price_ui}    CommonKeywords.Convert price to number format    ${before_discount_price_ui}
    BuiltIn.Should Be Equal As Numbers    ${before_discount_price_ui}    ${price}

Verify 'product save price' should be displayed correctly
    [Arguments]    ${sku}    ${evaluate_save_price}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_save_price_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
    ${save_discount_price_ui}    SeleniumLibrary.Get Text    ${elem}
    ${save_discount_price_ui}    CommonKeywords.Convert price to number format    ${save_discount_price_ui}
    BuiltIn.Should Be Equal As Numbers    ${save_discount_price_ui}    ${evaluate_save_price}

Verify 'product save price percent' should be displayed
    [Arguments]    ${sku}    ${evaluate_percent_discount}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_save_price_percent_with_sku_via_api]    $sku=${sku}
    ${percent_discount_ui}=    SeleniumLibrary.Get Text    ${elem}
    BuiltIn.Should Be Equal As Strings    ${percent_discount_ui}    ${evaluate_percent_discount}%

Get display configurable's simple product by configurable product sku
    [Arguments]    ${product_configurable_sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[img_product_with_sku_via_api]    $sku=${product_configurable_sku}
    ${product_displayed_sku}=    SeleniumLibrary.Get Element Attribute    ${elem}    data-product-id
    [Return]    ${product_displayed_sku}

Verify 'product promo tag' should be displayed correctly
    [Arguments]    ${sku}    ${promo}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_promo_tag]    $sku=${sku}    $promo=${promo}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product new tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_new_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product new tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_new_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product only at tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_only_at_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product online exclusive tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_online_exclusive_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify 'product online exclusive tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_online_exclusive_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product only at tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_only_at_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Click product by product sku
    [Arguments]    ${product_sku}
    ${elem}=    Format Text    ${dict_product_plp_fragment}[img_product_with_sku_via_api]    $sku=${product_sku}
    CommonWebKeywords.Click Element    ${elem}
    common_keywords.Wait until page is completely loaded

Verify 'product with discount' should be displayed
    [Arguments]    ${sku}
    ${lbl_product_save_price_percent_with_sku}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[lbl_product_save_price_percent_with_sku_via_api]    $sku=${sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${lbl_product_save_price_percent_with_sku}    timeout=${GLOBALTIMEOUT}    error=Data issue : no product with discount displayed.

Verify 3hr Badge Icon
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_product_plp_fragment}[3hr_badge_icon]    $sku=${sku}
    Run Keyword If    '${PLP_BADGE_EXP}'=='0'    SeleniumLibrary.Wait Until Element Is Not Visible    ${elem}
    ...    ELSE IF    '${PLP_BADGE_EXP}'=='1'    SeleniumLibrary.Wait Until Element Is Visible    ${elem}
