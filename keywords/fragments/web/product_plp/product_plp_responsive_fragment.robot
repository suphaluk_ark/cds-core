*** Variables ***
&{dict_product_plp_fragment}
...    lbl_total_product_found=xpath=//div[text()='{$styles_found}']
...    img_product_with_sku_via_api=xpath=//*[@data-product-sku="{$sku}"]//img[@data-product-id]
...    lbl_product_brand_with_sku_via_api=xpath=(//*[@data-product-sku="{$sku}"]//a[contains(@id,'lnk-viewBrand')][text()="{$brand_name}"])[1]
...    lbl_product_name_with_sku_via_api=xpath=(//*[@data-product-sku="{$sku}"]//h4[@data-product-id][text()="{$product_name}"])[1]
...    lbl_product_price_with_sku_via_api=xpath=(//*[@data-product-sku="{$sku}"]//div[text()='฿'])[1]
...    lbl_product_promo_tag=xpath=//*[@id="lnk-viewProduct-{$sku}"]//*[text()='{$promo}']
...    lbl_product_new_tag=xpath=//*[@id="lnk-viewProduct-{$sku}"]//*[text()='${product_tag.lbl_new}']
...    lbl_product_only_at_tag=xpath=//*[@id="lnk-viewProduct-{$sku}"]//*[text()='${product_tag.lbl_only_at_central}']
...    lbl_product_online_exclusive_tag=xpath=//*[@id="lnk-viewProduct-{$sku}"]//*[text()='${product_tag.lbl_online_exclusive}']
...    lbl_product_save_price_with_sku_via_api=xpath=(//*[@data-product-sku="{$sku}"]//div[text()='฿'])[3]
...    lbl_product_save_price_percent_with_sku_via_api=xpath=//*[@data-product-sku="{$sku}"]//*[@data-testid="percen-discount"]
...    lbl_product_before_discount_price_with_sku_via_api=xpath=(//*[@data-product-sku="{$sku}"]//div[text()='฿'])[2]