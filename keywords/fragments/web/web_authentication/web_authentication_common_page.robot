*** Keywords ***
Input username 
    [Arguments]    ${username}=${signin_authentication.username}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_web_authentication_page.txt_signin_username}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_web_authentication_page.txt_signin_username}    ${username}

Input password
    [Arguments]    ${password}=${signin_authentication.password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_web_authentication_page.txt_signin_password}    ${password}

Click sign in button
    CommonWebKeywords.Click Element    ${dict_web_authentication_page.btn_signin}
    common_keywords.Wait Until Page Is Completely Loaded    