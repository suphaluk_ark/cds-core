*** Keywords ***
Open responsive web browser
    [Arguments]    ${language}=${LANGUAGE}
    CommonWebKeywords.Open Chrome Browser to page on devices    ${${BU.lower()}_url}/${language}
    ...    ${devicesName}
    ...    ${SELENIUMSPEED}
    ...    headless=${HEADLESS_FLAG}
    ...    extension_full_path=${CURDIR}/../../../resources/extension/${BU.lower()}/${ENV.lower()}
    ...    sleep_loading_extension=2

    SeleniumLibrary.Set Window Size    ${WIDTH_MOB_WEB}    ${HEIGHT_MOB_WEB}
    Run Keyword If    '${BU.lower()}' == '${bu_type.rbs}'    common_responsive_keywords.Signin web authentication
    Run Keyword And Ignore Error    common_keywords.Add cookie for disable capcha
    Run Keyword And Ignore Error    home_common_page.Click close popup
    Run Keyword If    '${BU.lower()}' == '${bu_type.rbs}'    common_keywords.Add cookie to turn on omni feature

Signin web authentication
    web_authentication_common_page.Input username
    web_authentication_common_page.Input password
    web_authentication_common_page.Click sign in button

Open responsive web browser with specific URL path
    [Documentation]    Open test domain in responsive browser with specific path
    [Arguments]    ${URL_path}    ${language}=${LANGUAGE}
    Open Chrome Browser to page on devices    ${${BU.lower()}_url}/${language}/${URL_path}
    ...    ${devicesName}
    ...    ${SELENIUMSPEED}
    ...    headless=${HEADLESS_FLAG}
    ...    extension_full_path=${CURDIR}/../../../resources/extension/${BU.lower()}/${ENV.lower()}
    ...    sleep_loading_extension=2
    SeleniumLibrary.Set Window Size    ${WIDTH_MOB_WEB}    ${HEIGHT_MOB_WEB}
    Run Keyword If    '${BU.lower()}' == '${bu_type.rbs}'    common_responsive_keywords.Signin web authentication
    Run Keyword And Ignore Error    common_keywords.Add cookie for disable capcha
    Run Keyword And Ignore Error    home_common_page.Click close popup
    Run Keyword If    '${BU.lower()}' == '${bu_type.rbs}'    common_keywords.Add cookie to turn on omni feature

Click on hamburger menu
    CommonWebKeywords.Click Element     ${dict_home_page}[btn_hamburger_menu]