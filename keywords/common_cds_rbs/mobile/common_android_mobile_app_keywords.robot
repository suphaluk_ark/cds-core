*** Variables ***
&{dictCommonPage}
...    icon_back_page=android=UiSelector().description("${login_dto.lbl_backpage}")

&{dictCart}
...    icon_cart=android=UiSelector().resourceIdMatches(".*id/imageButton$")

&{dictHeader}
...    txt_search=android=UiSelector().resourceIdMatches(".*id/buttonSearch$")
...    txt_search_after_click=android=UiSelector().resourceIdMatches(".*id/search_src_text$")

*** Keywords ***
android Setup CDS Application by Membership 
    [Arguments]    ${dictMembership}
    common_mobile_app_keywords.Test Setup - Remove all product in shopping cart    ${dictMembership.email}    ${dictMembership.pwd}
    common_mobile_app_keywords.Launch CDS Application
    welcome_android_keywords.Handle initial language, skip tutorial
    login_android_keywords.Login successfully    ${dictMembership}

android Setup CDS Application by Facebook account
    [Arguments]    ${dictMembership}
    common_mobile_app_keywords.Test Setup - Remove all product in shopping cart    ${dictMembership.email}    ${dictMembership.pwd}
    common_mobile_app_keywords.Launch CDS Application
    welcome_android_keywords.Handle initial language, skip tutorial
    login_android_keywords.Login successfully with facebook    ${dictMembership}

android Setup CDS Application by Guest 
    common_mobile_app_keywords.Launch CDS Application
    welcome_android_keywords.Handle initial language, skip tutorial

Click item of dropdown by position
    [Arguments]    ${locator_screen}    ${locator_dropdown}    ${position}
    #Get Screen 
    ${size_of_screen}    AppiumLibrary.Get Element Size    ${locator_screen}
    #Get Element
    ${size_of_dropdown}    AppiumLibrary.Get Element Size    ${locator_dropdown}
    ${location_of_dropdown}    AppiumLibrary.Get Element Location    ${locator_dropdown}
    #Parameters
    ${row_height}    Evaluate    130 * (${size_of_screen}[height]//1395)
    ${location_of_screen}    AppiumLibrary.Get Element Location    ${locator_screen}
    ${x}    BuiltIn.Evaluate    ${location_of_dropdown}[x] + ${size_of_dropdown}[width]/2
    ${position_of_dropdown}    BuiltIn.Evaluate    ${size_of_dropdown}[height] / 2 + ${location_of_dropdown}[y]
    #Check up or down
    ${is_up}    Evaluate    (${position_of_dropdown} - (${size_of_screen}[height] / 2) - ${location_of_screen}[y]) > 0
    #Check position
    ${position}    Convert To Integer    ${position}
    ${y}    Run Keyword If    ${is_up} == ${True}    Evaluate    ${row_height} * ${position}
    ...    ELSE    Evaluate    (${row_height} * ${position}) + ${position_of_dropdown}
    #Click Element by position
    AppiumLibrary.Click Element At Coordinates    ${x}    ${y}

Click a key on keyboard
    [Arguments]    ${keycode}
    AppiumLibrary.Press Keycode    ${keycode}

Click Search key on the virtual keyboard
    AppiumLibrary.Press Keycode    66

Click back button
    [Documentation]    Use it when you are in my profile, wishlist page,..."Navigate Up"
    CommonMobileKeywords.Click Element    ${dictCommonPage}[icon_back_page]