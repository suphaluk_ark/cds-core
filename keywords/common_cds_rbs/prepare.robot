*** Keywords ***
Setup test account
    [Arguments]    ${list_user_template}    ${account}
    [Documentation]    Support format like this:
    ...    Examples template:
    ...     example_user_template:
    ...         email: example_{$template}@mail.com
    ...         password: same_password
    ...         first_name: account_{$template}
    ...         etc: ....
    ...    Keyword calling example
    ...    | [Setup]    Setup test account | ${example_user_template} | $template=new_value |
    ...    result: ${user_email} = example_new_value@mail.com, ${user_password} = same_password, ${user_first_name} = account_new_value

    FOR    ${item}    IN    @{list_user_template}
        ${result}=    CommonKeywords.Format Text    ${list_user_template}[${item}]    $template=${account}
        Set Test Variable    ${user_${item}}    ${result}
    END
