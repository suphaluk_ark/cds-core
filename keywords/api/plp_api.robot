*** Keywords ***
Get random color of filter color by product keyword
    [Arguments]    ${keyword}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${1}
    ${list_of_color}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='color_group_name')]..key
    Should Not Be Empty    ${list_of_color}
    ${color}=    Evaluate    random.choice(${list_of_color})    random
    [Return]    ${color}

Get min, max of filter price range by product keyword
    [Arguments]    ${keyword}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${1}
    ${min_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='min_price')].value
    ${max_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='max_price')].value
    [Return]    ${min_price}[0]    ${max_price}[0]

Get list brand name of filter brand name by product keyword
    [Arguments]    ${keyword}
    ${response}=    search_falcon.Api Search Product By Keyword    ${keyword}    ${language}    ${BU.lower()}    ${1}
    ${list_of_brand_name}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='brand_name')]..key
    Should Not Be Empty    ${list_of_brand_name}
    [Return]    ${list_of_brand_name}

Get random color of filter color by category id
    [Arguments]    ${category_id}
    ${response}=    search_falcon.Api Search Product By Category Id    ${category_id}    ${language}    ${BU.lower()}    ${1}
    ${list_of_color}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='color_group_name')]..key
    Should Not Be Empty    ${list_of_color}
    ${color}=    Evaluate    random.choice(${list_of_color})    random
    [Return]    ${color}

Get list color of filter color by configurable product
    [Documentation]    Return list color by simple products of configurable product
    [Arguments]    ${dict_configurable_product}
    ${list_color}=    JSONLibrary.Get Value From Json    ${dict_configurable_product}    $..configurable_products..color_group_name
    ${result}=    Create List
    FOR    ${item}    IN    @{list_color}
        Continue For Loop If    ${item}==${None}
        ${result}=    Combine Lists    ${result}    ${item}
    END
    ${result}=    Remove Duplicates    ${result}
    Log List    ${result}
    [Return]    ${result}

Get min, max of filter price range by product brand name
    [Arguments]    ${brand_name}
    ${response}=    search_falcon.Api Search Product By Brand Name    ${test_brand_name}    ${language}    ${BU.lower()}    ${1}
    ${min_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='min_price')].value
    ${max_price}=    JSONLibrary.Get Value From Json    ${response}    $..aggregations[?(@.field=='max_price')].value
    [Return]    ${min_price}[0]    ${max_price}[0]

Retry get random price range
    [Documentation]    Return 'min price' and 'max price' between the price range with custom 'different price'.
    [Arguments]    ${min}    ${max}    ${diff}
    ${min}=    Evaluate    math.floor(${min})    math
    ${max}=    Evaluate    math.floor(${max})    math
    ${result_min}    ${result_max}      Wait Until Keyword Succeeds    0.5s    0.001s      Get random price range    ${min}    ${max}    ${diff}
    [Return]    ${result_min}    ${result_max}

Get random price range
    [Arguments]    ${min}    ${max}    ${diff}
    ${list_random}    Evaluate    random.sample(range(${min}, ${max}), 2)    random
    Collections.Sort List    ${list_random}

    ${evaluate_diff}=    Evaluate    ${list_random}[1]-${list_random}[0]
    Return From Keyword If   ${evaluate_diff} >= ${diff}    ${list_random}[0]    ${list_random}[1]
    Fail   Could not find the range price from  ${min}, to ${max}, diff ${diff}