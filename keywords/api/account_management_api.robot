*** Keywords ***
Remove all address by api
    [Arguments]     ${email}    ${password}
    ${token}=    authentication.Get login token    ${email}    ${password}
    ${response}=    customer_details.Get customer details    ${token}
    ${get_address}=    Get Value From Json    ${response}    $..addresses..id
    ${address_length}=    Get Length    ${get_address}
    FOR    ${i}    IN RANGE    ${address_length}
        Delete Address by API    ${get_address}[${i}]
    END
