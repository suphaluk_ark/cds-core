*** Keywords ***
Get product sell price by json object product
    [Documentation]    Return sell price if
    ...                - *product type_id - simple*     : number, product sell price
    ...                - *product type_id - configurable*     : number, product simple lowest sell price
    [Arguments]    ${json_object}
    ${price}=    Run Keyword If    '${json_object}[type_id]'=='simple'    Get product price   ${json_object}
    ...    ELSE IF    '${json_object}[type_id]'=='configurable'    Get configurable product price   ${json_object}
    ...    ELSE       Fail    Incorrect 'type_id'
    [Return]    ${price}

Get product price
    [Arguments]    ${dict_product}
    ${result_price}=    Set Variable If    ${dict_product}[special_price]==${None}    ${dict_product}[price]
    ...    ${dict_product}[special_price] > ${dict_product}[price]    ${dict_product}[price]    # 21 DEC 2020 Added logic for one project - price service
    ...    ${dict_product}[special_price]
    [Return]    ${result_price}

Get configurable product price
    [Arguments]    ${dict_product}
    ${list_of_configurable_product}=    JSONLibrary.Get Value From Json    ${dict_product}    $..configurable_products[*]
    ${lowest_price}=    Get product price    ${list_of_configurable_product}[0]
    FOR    ${dict_configurable_product}    IN    @{list_of_configurable_product}
        ${price}=    Get product price    ${dict_configurable_product}
        ${lowest_price}=    Set Variable If    ${lowest_price} < ${price}    ${lowest_price}    ${price}
    END
    [Return]    ${lowest_price}