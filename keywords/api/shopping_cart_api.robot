*** Keywords ***
Member, clear cart and add multi skus to cart
    [Arguments]    ${user_token}    ${skus}    ${quantities}
    # MDC clear cart
    cart.Delete cart    ${user_token}
    cart.Post customer cart    ${user_token}
    # Falcon
    ${quote_id}=    shopping_cart_falcon.Member, add multi items to cart    ${user_token}    ${skus}    ${quantities}
    [Return]    ${quote_id}
