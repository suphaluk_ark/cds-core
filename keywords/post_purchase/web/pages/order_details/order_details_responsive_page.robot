*** Variables ***
&{dict_order_details_page}
...    url_order_detail_page_member=account/orders/{$order_number}
...    url_order_detail_page_guest=orders/tracking?increment_id={$order_number}&customer_email={$guest_email}
...    lbl_order_status=xpath=//span[@data-testid="txt-viewOrderStatusOnText"]
...    icn_progress_bar_active_order_status=xpath=//li[@id="pbr-viewOrderDetailOnpackaging-undefined-{$order_status}" and @data-active="true"]
...    btn_repayment=css=[data-testid="lnk-viewOrderHeaderOnPayNowMobile"]

*** Keywords ***
Go to order details page for member by direct url
    [Arguments]    ${order_number}
    ${path}=    CommonKeywords.Format Text    ${dict_order_details_page.url_order_detail_page_member}    $order_number=${order_number}
    common_keywords.Go to direct url  ${path}

Go to order details page for guest by direct url
    [Arguments]    ${order_number}    ${guest_email}
    ${path}=    CommonKeywords.Format Text    ${dict_order_details_page.url_order_detail_page_guest}    $order_number=${order_number}    $guest_email=${guest_email}
    common_keywords.Go to direct url  ${path}

Verify order status display correctly
    [Arguments]    ${order_status}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_order_details_page.lbl_order_status}    ${order_status}

Verify progress bar current order status is displayed as active
    [Arguments]    ${order_status}
    ${path}=    CommonKeywords.Format Text    ${dict_order_details_page.icn_progress_bar_active_order_status}    $order_status=${order_status.lower()}
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_order_details_page.lbl_order_status}

Click on repayment button in order detail page
    CommonWebKeywords.Click Element     ${dict_order_details_page.btn_repayment}

Retry verify order status display correctly
    [Arguments]    ${retry_times}    ${with_in_sec}    ${order_status}
    Wait Until Keyword Succeeds    ${retry_times}    ${with_in_sec}    Run Keywords    Reload Page
    ...    AND    loader_common_fragment.Wait Until Page Loader Is Not Visible
    ...    AND    order_details_responsive_page.Verify order status display correctly    ${order_status}

Retry verify progress bar current order status is displayed as active
    [Arguments]    ${retry_times}    ${with_in_sec}    ${order_status}
    Wait Until Keyword Succeeds    ${retry_times}    ${with_in_sec}    Run Keywords    Reload Page
    ...    AND    loader_common_fragment.Wait Until Page Loader Is Not Visible
    ...    AND    order_details_responsive_page.Verify progress bar current order status is displayed as active    ${order_status}
