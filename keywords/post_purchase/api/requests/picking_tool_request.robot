***Keywords***
Verify that order create on pickingtool successfully by destination system
    [Arguments]     ${order_id}
    ${pickingtool_status_response}=     kibana_api.Get status response from pickingtool by destinationSystem     ${order_id}
    ${pickingtool_status_response}=     Convert String To Json    ${pickingtool_status_response}
    ${pickingtool_status}=    Get Value From Json    ${pickingtool_status_response}    $..statusReason
    ${len}=    Get Length    ${pickingtool_status}
    ${pickingtool_status}=    Set Variable If  ${len} > 0    ${pickingtool_status}[0]
    Should Be Equal As Strings    ${pickingtool_status}    Success

Retry until order is created successfully in Pickingtool via ESB
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    picking_tool_request.Verify that order create on pickingtool successfully by destination system    ${order_id}

Verify that order is cancelled on pickingtool successfully by destination system
    [Arguments]     ${order_id}
    ${pickingtool_status_response}=     kibana_api.Get status response from RIS-pickingtool     ${order_id}
    ${pickingtool_status_response}=     Convert String To Json    ${pickingtool_status_response}
    ${pickingtool_status}=    Get Value From Json    ${pickingtool_status_response}    $..statusReason
    ${len}=    Get Length    ${pickingtool_status}
    ${pickingtool_status}=    Set Variable If  ${len} > 0    ${pickingtool_status}[0]
    Should Be Equal As Strings    ${pickingtool_status}    Success

Retry until order is cancelled successfully in Pickingtool via ESB
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    picking_tool_request.Verify that order is cancelled on pickingtool successfully by destination system    ${order_id}
