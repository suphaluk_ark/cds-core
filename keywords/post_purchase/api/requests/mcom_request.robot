***Keywords***
Verify MCOM order status should be 'LOGISTICS'
    [Arguments]    ${order_id}
    ${status}=    search_sales_order.Get order status from MCOM    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status}    LOGISTICS

Retry until MCOM order status should be 'LOGISTICS'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mcom_request.Verify MCOM order status should be 'LOGISTICS'    ${order_id}

Verify MCOM order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_id}
    ${status_reason}=    search_sales_order.Get order status reason from MCOM    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Retry until MCOM order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mcom_request.Verify MCOM order status reason should be 'READYTOSHIP'    ${order_id}

Force cancel order via MCOM directly
    [Arguments]    ${order_id}
    Wait Until Keyword Succeeds     10 x    10 sec    force_shipment.Fully force cancel via MCOM by order number    ${order_id}

Verify MCOM order status should be 'COMPLETE'
    [Arguments]    ${order_id}
    ${status_reason}=    search_sales_order.Get order status from MCOM    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    COMPLETE

Verify MCOM order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_id}
    ${status_reason}=    search_sales_order.Get order status reason from MCOM    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    FULLYCANCELLED

Retry until MCOM order status should be 'COMPLETE'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mcom_request.Verify MCOM order status should be 'COMPLETE'    ${order_id}

Retry until MCOM order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mcom_request.Verify MCOM order status reason should be 'FULLYCANCELLED'    ${order_id}
