***Keywords***
Retry until order number exist in FMS DB
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    fms_db.Verify that order number exist in FMS DB       ${order_id}

Verify order is created successfully in FMS via ESB
    [Arguments]    ${order_no}
    BuiltIn.Wait Until Keyword Succeeds    30 x    15 sec    kibana_api.Get create order response body for FMS to verify order is created successfully    ${order_no}
