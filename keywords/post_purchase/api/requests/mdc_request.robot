**Keywords***
Get status from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[*].extension_attributes.order_status
    [Return]    ${status}

Get status reason from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[?(@.increment_id == '${increment_id}')].extension_attributes.mom_status_reason
    [Return]    ${status}

Verify MDC order status from mdc should be 'LOGISTIC' status
    [Arguments]    ${order_id}
    ${status}=    mdc_request.Get status from order detail response    ${order_id}
    Should Be Equal As Strings    ${status}    MCOM_LOGISTICS

Verify MDC order status from mdc should be 'Processing' status
    [Arguments]    ${order_id}
    ${status}=    mdc_request.Get status from order detail response    ${order_id}
    Should Be Equal As Strings    ${status}    processing

Retry until MDC order status should be 'Processing'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mdc_request.Verify MDC order status from mdc should be 'Processing' status    ${order_id}

Retry until MDC order status should be 'LOGISTICS'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mdc_request.Verify MDC order status from mdc should be 'LOGISTIC' status    ${order_id}

Verify MDC order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_id}
    ${status_reason}=    mdc_request.Get status reason from order detail response    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Retry until MDC status reason should be 'READYTOSHIP'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    Verify MDC order status reason should be 'READYTOSHIP'    ${order_id}

Verify MDC order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_id}
    ${status_reason}=    mdc_request.Get status reason from order detail response    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    PENDING_VALIDATE_PAYMENT_REPLY

Retry until MDC status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mdc_request.Verify MDC order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${order_id}

Verify MDC order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${increment_id}
    ${status_reason}=    mdc_request.Get status reason from order detail response    ${increment_id}
    Should Be True    '${status_reason}'=='PENDING_FIRST_SHIPMENT_REQUEST' or '${status_reason}'=='READYTOSHIP'

Retry until order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mdc_request.Verify MDC order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${order_id}

Verify MDC order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_id}
    ${status_reason}=    mdc_request.Get status reason from order detail response    ${order_id}
    Should Be True    '${status_reason}'=='FULLYCANCELLED'

Retry until MDC order status reason should be 'FULLYCANCELLED'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mdc_request.Verify MDC order status reason should be 'FULLYCANCELLED'    ${order_id}

Verify MDC order status from mdc should be 'CANCELLED' status
    [Arguments]    ${order_id}
    ${status}=    mdc_request.Get status from order detail response    ${order_id}
    Should Be Equal As Strings    ${status.upper()}    CANCELED

Retry until MDC order status should be 'CANCELLED'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    mdc_request.Verify MDC order status from mdc should be 'CANCELLED' status    ${order_id}