***Keywords***
Get status response from wms by destinationSystem
    [Arguments]    ${order_no}
    ${response}=    sms_details.Get Kibana log filter by condition    ${order_no} AND destinationSystem:"ail"
    ${response}=    Get Value From Json    ${response}    $.._source..response.body
    ${len}=    Get Length    ${response}
    ${response}=    Set Variable If  ${len} > 0    ${response}[0]
    [Return]    ${response}

Verify that order create on WMS successfully by destination system
    [Arguments]     ${order_id}
    ${wms_status_response}=     wms_request.Get status response from wms by destinationSystem     ${order_id}
    ${wms_status_response}=     Convert String To Json    ${wms_status_response}
    ${wms_status}=     Get Value From Json    ${wms_status_response}    $..message
    ${len}=    Get Length    ${wms_status}
    ${wms_status}=    Set Variable If  ${len} > 0    ${wms_status}[0]
    Should Be Equal As Strings    ${wms_status}    success

Retry until order is created successfully in WMS via ESB
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_created.Verify that order create on WMS successfully by destination system    ${order_id}

Verify order is created successfully in WMS via ESB
    [Arguments]    ${order_id}
    Wait Until Keyword Succeeds    30 x    10 sec    wms_request.Verify that order create on WMS successfully by destination system    ${order_id}