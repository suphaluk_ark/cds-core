*** Keywords ***
Get status from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[?(@.increment_id == '${increment_id}')].status
    [Return]    ${status}

Get status reason from order detail response
    [Arguments]    ${increment_id}
    ${response}=    order_details.Get order details by increment_id    ${increment_id}
    ${status}=    REST.Output    $.items[?(@.increment_id == '${increment_id}')].extension_attributes.mom_status_reason
    [Return]    ${status}

# Verify status on MDC 
Verify MDC order status from mdc should be 'LOGISTIC' status
    [Arguments]    ${order_id}
    ${status}=    order_status.Get status from order detail response    ${order_id}
    Should Be Equal As Strings    ${status}    MCOM_LOGISTICS

Retry until MDC order status should be 'LOGISTICS'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_status.Verify MDC order status from mdc should be 'LOGISTIC' status    ${order_id}

Verify MDC order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_id}
    ${status_reason}=    order_status.Get status reason from order detail response    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    PENDING_VALIDATE_PAYMENT_REPLY

Retry until MDC status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_status.Verify MDC order status reason should be 'PENDING_VALIDATE_PAYMENT_REPLY'    ${order_id}

Verify MDC order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${increment_id}
    ${status_reason}=    order_status.Get status reason from order detail response    ${increment_id}
    Log To Console    ${status_reason}
    Should Be True    '${status_reason}'=='PENDING_FIRST_SHIPMENT_REQUEST' or '${status_reason}'=='READYTOSHIP'

Retry until MDC order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_status.Verify MDC order status reason should be 'PENDING_FIRST_SHIPMENT_REQUEST'    ${order_id}

# Verify status in MCOM 
Verify MCOM order status should be 'LOGISTICS'
    [Arguments]    ${order_id}
    ${status}=    search_sales_order.Get order status from MCOM    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status}    LOGISTICS

Retry until MCOM order status should be 'LOGISTICS'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_status.Verify MCOM order status should be 'LOGISTICS'    ${order_id}

Verify MCOM order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_id}
    ${status_reason}=    search_sales_order.Get order status reason from MCOM    ${order_id}
    BuiltIn.Should Be Equal As Strings    ${status_reason}    READYTOSHIP

Retry until MCOM order status reason should be 'READYTOSHIP'
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_status.Verify MCOM order status reason should be 'READYTOSHIP'    ${order_id}

Retry until order number exist in FMS DB     
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    fms_db.Verify that order number exist in FMS DB       ${order_id}
