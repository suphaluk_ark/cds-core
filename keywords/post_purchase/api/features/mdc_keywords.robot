***Keywords***
Verify order status should be "Processing/ READYTOSHIP" on MDC
    [Arguments]    ${order_number}
    mdc_request.Retry until MDC order status should be 'Processing'    ${order_number}
    mdc_request.Retry until MDC status reason should be 'READYTOSHIP'    ${order_number}

Get order number from MDC by entity id
    [Arguments]    ${entity_id}
    ${order_detail_response}=    order_details.Get order details by entity_id    ${entity_id}
    ${order_number}=    order_details.Get mdc_increment_id by order detail response    ${order_detail_response}
    [return]    ${order_number}

Verify order status should be "COMPLETE FULLYCANCELLED" on MDC
    [Arguments]    ${order_number}
    mdc_request.Retry until MDC order status should be 'CANCELLED'    ${order_number}
    mdc_request.Retry until MDC order status reason should be 'FULLYCANCELLED'    ${order_number}