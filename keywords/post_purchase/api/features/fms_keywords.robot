*** Keywords ***
Retry until order is created successfully in FMS via ESB
    [Arguments]    ${order_no}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    kibana_api.Get create order request body for FMS to verify order is created successfully    ${order_no}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    kibana_api.Get create order response body for FMS to verify order is created successfully    ${order_no}