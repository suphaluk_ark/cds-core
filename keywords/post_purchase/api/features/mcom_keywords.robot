***Keywords***
Verify order status should be "LOGISTIC/READYTOSHIP" on MCOM
    [Arguments]    ${order_number}
    mcom_request.Retry until MCOM order status should be 'LOGISTICS'    ${order_number}
    mcom_request.Retry until MCOM order status reason should be 'READYTOSHIP'    ${order_number}

Verify order status should be "COMPLETE FULLYCANCELLED" on MCOM
    [Arguments]    ${order_number}
    mcom_request.Retry until MCOM order status should be 'COMPLETE'    ${order_number}
    mcom_request.Retry until MCOM order status reason should be 'FULLYCANCELLED'    ${order_number}