*** Keywords ***
# Pickingtool
Verify that order create on pickingtool successfully by destination system
    [Arguments]     ${order_id}
    ${pickingtool_status_response}=     kibana_api.Get status response from pickingtool by destinationSystem     ${order_id}
    ${pickingtool_status_response}=     Convert String To Json    ${pickingtool_status_response}
    ${pickingtool_status}=    Get Value From Json    ${pickingtool_status_response}    $..statusReason
    ${len}=    Get Length    ${pickingtool_status}
    ${pickingtool_status}=    Set Variable If  ${len} > 0    ${pickingtool_status}[0]
    Should Be Equal As Strings    ${pickingtool_status}    Success

Retry until order is created successfully in Pickingtool via ESB
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_created.Verify that order create on pickingtool successfully by destination system    ${order_id}

# WMS
Get status response from wms by destinationSystem
    [Arguments]    ${order_no}
    ${response}=    sms_details.Get Kibana log filter by condition    ${order_no} AND destinationSystem:"ail"
    ${response}=    Get Value From Json    ${response}    $.._source..response.body
    ${len}=    Get Length    ${response}
    ${response}=    Set Variable If  ${len} > 0    ${response}[0]
    [Return]    ${response}

Verify that order create on WMS successfully by destination system
    [Arguments]     ${order_id}
    ${wms_status_response}=     order_created.Get status response from wms by destinationSystem     ${order_id}
    ${wms_status_response}=     Convert String To Json    ${wms_status_response}
    ${wms_status}=     Get Value From Json    ${wms_status_response}    $..message
    ${len}=    Get Length    ${wms_status}
    ${wms_status}=    Set Variable If  ${len} > 0    ${wms_status}[0]
    Should Be Equal As Strings    ${wms_status}    success

Retry until order is created successfully in WMS via ESB
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    order_created.Verify that order create on WMS successfully by destination system    ${order_id}

# FMS
Retry until order number exist in FMS DB     
    [Arguments]    ${order_id}
    BuiltIn.Wait Until Keyword Succeeds    30 x    10 sec    fms_db.Verify that order number exist in FMS DB       ${order_id}

# Create order
Member Checkout With COD By Standard Shipping
    [Documentation]    Shipping method: Standard Delivery 
    ...                Payment method: COD
    [Arguments]    ${username}    ${password}   ${sku}    ${qty}    ${store}
    ${user_token}    ${quote_id}    cart.Member add skus to cart, return token and cart id    ${username}    ${password}   ${sku}    ${qty}    ${store}
    checkout.Member validate carriers and shippings by estimate shipping methods    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    ${store}    ${api_ver3}
    checkout.Member update shipping information    ${user_token}    ${shipping_methods.standard.carrier}    ${shipping_methods.standard.method}    10    ${store}
    checkout.Member select a specific payment method    ${user_token}    ${payment_methods.cod}    ${store}
    ${entity_id}=    checkout.Member select payment type and create new order    ${user_token}    ${payment_methods.cod}    ${store}
    ${order_number}=    mdc_keywords.Get order number from MDC by entity id     ${entity_id}
    [return]    ${order_number}