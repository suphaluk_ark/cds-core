*** Keywords ***
Click add to cart button
    [Arguments]    ${product_sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page.btn_add_to_cart}    $product_sku=${product_sku}
    Wait Until Keyword Succeeds    3x    1s    CommonWebKeywords.Scroll And Click Element    ${elem}

    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page.btn_loading}    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page.btn_add_success}    $product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Verify add to cart button is displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[btn_add_to_cart_ui]

Verify product details on product PDP page
    [Arguments]    ${product_details}
    SeleniumLibrary.Wait Until Element Is Visible    ${dict_pdp_page}[text_PDP_details]
    SeleniumLibrary.Page Should Contain    ${product_details}

Get product urlkey by current url
    ${current_url}=    SeleniumLibrary.Get Location
    ${urlkey}    Get product urlkey    ${current_url}
    [Return]    ${urlkey}

Get product urlkey
    [Arguments]    ${url}
    ${urlkey}=    Evaluate    urllib.parse.urlparse('${url}')    urllib
    ${urlkey}=   String.Remove String    ${urlkey.path}    /${language}/
    [Return]    ${urlkey}

Verify brand name on product PDP page
    [Arguments]    ${expected_brand_name}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_pdp_page}[text_brand_name]    ${expected_brand_name}

Verify 'pay by installment' should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_pdp_page}[btn_installment]

Verify social icon facebook on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_PDP_facebook]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_PDP_facebook]

Verify social icon twitter on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_PDP_twitter]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_PDP_twitter]

Verify social icon email on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_PDP_email]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_PDP_email]

Verify social icon line on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_PDP_line]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_PDP_line]

Verify 'product id' should be displayed correctly
    [Arguments]    ${product_id}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${dict_pdp_page}[lbl_product_id]    ${product_detail_page.product_id} ${product_id}

Verify 'product name label' should be displayed correctly
    [Arguments]    ${product_sku}    ${product_name}
    ${locator_product_name}=    Format Text    ${dict_pdp_page}[text_PDP_product_name]    product_sku=${product_sku}
    CommonWebKeywords.Verify Web Element Text Should Be Equal    ${locator_product_name}    ${product_name}

Verify 'product only at tag' should be displayed in PDP
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_only_at_tag]
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify discount price on product PDP page
    [Arguments]    ${product_sku}    ${discount_price}
    ${locator_discount_price}=    Format Text  ${dict_pdp_page}[text_discount_price]  product_sku=${product_sku}
    ${get_discount_price}=    SeleniumLibrary.Get Text    ${locator_discount_price}
    ${get_discount_price}=    CommonKeywords.Convert price to number format    ${get_discount_price}
    Should Be Equal As Numbers    ${get_discount_price}    ${discount_price}

Verify 'no review label' should be displayed
    CommonWebKeywords.Scroll To Center Page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[lbl_product_details]
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[lbl_customer_reviews]
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[lbl_no_review]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[lbl_no_review]

Verify 'img product src' display on product PDP page
    [Arguments]    ${img_src}    ${timeout}=5s
    ${locator_img}=    Format Text  ${dict_pdp_page}[img_main_product_src]  $img_src=${img_src}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_img}    ${timeout}

Verify 'add a review link' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[lnk_add_a_review]

Verify full price on product PDP page should not be displayed
    [Arguments]    ${product_sku}
    ${elem}=    Format Text  ${dict_pdp_page}[text_full_price]  product_sku=${product_sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Verify save price percent on product PDP page should not be displayed
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${dict_pdp_page}[lbl_product_save_price_percent]

Click 'delivery & return' tab
    Run Keyword And Ignore Error    CommonWebKeywords.Scroll And Click Element    ${dict_pdp_page}[tab_delivery_and_return]

Verify 'delivery & return tab details' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[tab_delivery_and_return_details]

Verify 'add wishlist button' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[btn_add_wishlist]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Click 'add wishlist' button
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[btn_add_wishlist]    $sku=${sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}

Verify 'please login pop up' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[lbl_please_login_pop_up]    ${dict_pdp_page}[lbl_please_login_to_add_product_to_wish_list_pop_up]

Click 'OK' please login pop up button
    CommonWebKeywords.Click Element    ${dict_pdp_page}[btn_ok_please_login_pop_up]

Add product quantity
    [Arguments]    ${product_sku}    ${quantity}
    Run Keyword If    '${TEST_PLATFORM.lower()}'=='responsive'    Add product quantity for mobile web    ${product_sku}    ${quantity}
    ...    ELSE    Add product quantity for desktop web    ${product_sku}    ${quantity}

Add product quantity for desktop web
    [Arguments]    ${product_sku}    ${quantity}=${1}
    Set Test Variable    ${product_quantity}    ${quantity}
    Return From Keyword If    ${quantity}==${1}    ${True}

    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page.ddl_quantity}    $product_sku=${product_sku}
    CommonWebKeywords.Scroll And Click Element    ${elem}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page.list_qty}    $product_sku=${product_sku}    $number=${quantity}
    CommonWebKeywords.Scroll And Click Element    ${elem}
    loader_common_fragment.Wait until page loader is not visible

Add product quantity for mobile web
    [Arguments]    ${product_sku}    ${quantity}=${1}
    Set Test Variable    ${product_quantity}    ${quantity}
    Return From Keyword If    ${quantity}==${1}    ${True}

    Set Test Variable    ${product_quantity}    ${quantity}
    ${elem}=  CommonKeywords.Format Text    ${dict_pdp_page.ddl_quantity}    $product_sku=${product_sku}
    ${quantity}    BuiltIn.Convert To String    ${quantity}
    SeleniumLibrary.Select From List By Value    ${elem}    ${quantity}
    loader_common_fragment.Wait until page loader is not visible

Click on add to cart button
    [Arguments]    ${sku_number}
    Run Keyword And Ignore Error    Enable Testability Automatic Wait
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[btn_addToCart]    $skunumber=${sku_number}
    Wait Until Keyword Succeeds    3x    1s    CommonWebKeywords.Scroll And Click Element    ${elem}    
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[btn_loading]    $skunumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[btn_add_success]    $skunumber=${sku_number}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}
    common_keywords.Wait Until Page Is Completely Loaded

Click view product from sku
    [Arguments]    ${sku}
    ${elem}=       Replace String    ${dict_pdp_page}[lbl_product]      {$sku}     ${sku} 
    CommonWebKeywords.Click Element           ${elem}

Verify icon standard delivery on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_std_delivery]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_std_delivery]

Verify icon express delivery on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_exp_delivery]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_exp_delivery]

Verify icon three hour delivery on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_sfs_delivery]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_sfs_delivery]


Verify icon standard pickup delivery on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_cc_delivery]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_cc_delivery]

Verify icon one hour pickup delivery on product PDP page
    CommonWebKeywords.Scroll To Element    ${dict_pdp_page}[icon_hp_delivery]
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[icon_hp_delivery]

Click 'promotions' tab
    CommonWebKeywords.Scroll And Click Element    ${dict_pdp_page}[tab_promotions]

Verify 'promotions tab details' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[tab_promotions_details]

Verify redeem point text display on product PDP page
    [Arguments]    ${product_sku}    ${expected_redeem_point}
    ${locator_product_redeem}=    Format Text    ${dict_pdp_page}[text_PDP_redeem_point]    product_sku=${product_sku}
    SeleniumLibrary.Wait Until Element Is Visible    ${locator_product_redeem}
    ${actual_redeem_point}=    SeleniumLibrary.Get Text    ${locator_product_redeem}
    ${actual_redeem_point}=    CommonKeywords.Convert price to number format    ${actual_redeem_point}
    Should Be Equal As Integers    ${actual_redeem_point}    ${expected_redeem_point}

Verify 'pay by installment' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[btn_installment]

Click 'pay by installment' button
    CommonWebKeywords.Scroll And Click Element    ${dict_pdp_page}[btn_installment]

Verify 'installment pop up' should be displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_pdp_page}[lbl_installment_pop_up]

Click 'OK' installment pop up button
    CommonWebKeywords.Click Element    ${dict_pdp_page}[btn_ok_installment_pop_up]

Verify 'remove wishlist button' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[btn_remove_wishlist]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product promo tag' should be displayed correctly
    [Arguments]    ${sku}    ${promo}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_promo_tag]    $promo=${promo}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify 'product new tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_new_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}

Verify 'product new tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_new_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}   

Verify 'product only at tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_only_at_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}   

Verify 'product online exclusive tag' should be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_online_exclusive_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Visible    ${elem}   

Verify 'product online exclusive tag' should not be displayed
    [Arguments]    ${sku}
    ${elem}=    CommonKeywords.Format Text    ${dict_pdp_page}[lbl_product_online_exclusive_tag]    $sku=${sku}
    CommonWebKeywords.Verify Web Elements Are Not Visible    ${elem}

Get first display priority product cart price rule overlay
    [Arguments]    ${list_cart_price_rule_overlays}
    ${list_display_priority}=    JSONLibrary.Get Value From Json    ${list_cart_price_rule_overlays}    $..display_priority[*]
    ${first_priority}=    Set Variable    ${list_display_priority}[0]

    FOR    ${cart_price_rule_overlay}   IN    @{list_cart_price_rule_overlays}
        ${is_priority}=    Run Keyword And Return Status    Should Be True    ${cart_price_rule_overlay}[display_priority] == ${first_priority}
        Exit For Loop If    ${is_priority}
    END
    Should Be True    ${is_priority}    msg=Incorrect product cart price rule overlays

    ${dict_result}=    BuiltIn.Create Dictionary    is_display=${True}
    ...   overlay_type=cart_price_rule_overlays
    ...   overlay_image=${cart_price_rule_overlay}[overlay_image]
    ...   priority=${cart_price_rule_overlay}[display_priority]
    [Return]    ${dict_result}

Verify 'img product overlay src' display on product PDP page
    [Arguments]    ${img_src}    ${timeout}=5s
    ${is_img_box_thumnail_visible}=     Run Keyword And Return Status    SeleniumLibrary.Element Should Be Visible    ${img_box_thumbnail}
    ${locator_overlay_img}    Set Variable If    ${is_img_box_thumnail_visible}    ${dict_pdp_page}[img_product_overlay_src_with_slide]    ${dict_pdp_page}[img_product_overlay_src]

    ${locator_img}=    Format Text  ${locator_overlay_img}  $img_src=${img_src}
    SeleniumLibrary.Wait Until Element Is Visible     ${locator_img}    ${timeout}
