*** Keywords ***
Get product price data by product sku
    [Documentation]    Return dictionary of product price
    ...                - *is_product_no_discount*     : ${True}, product has no discount
    ...                - *price*     : number, Product price
    ...                - *special_price*     : number, Product special price
    ...                - *evaluate_save_price*     : number, price - special price
    ...                - *evaluate_percent_discount*     : number, calculation 'evaluate_save_price' with percent
    [Arguments]    ${json_object_by_product_sku}
    ${price}=    Set Variable    ${json_object_by_product_sku}[price]
    ${special_price}=    Set Variable    ${json_object_by_product_sku}[special_price]

    ${is_product_no_discount}=    Set Variable If
    ...    ${special_price}==${None}    ${True}
    ...    ${special_price}==0    ${True}
    ...    ${price}<=${special_price}    ${True}    ${False}    # 21 DEC 2020 Added logic for one project - price service
    
    ${evaluate_save_price}=    Run Keyword If    ${is_product_no_discount}    Set Variable    ${None}
    ...   ELSE    calculation.Evaluate save price    ${price}    ${special_price}
    ${evaluate_percent_discount}=    Run Keyword If    ${is_product_no_discount}    Set Variable    ${None}
    ...   ELSE    calculation.Evaluate percent discount    ${price}    ${special_price}
    
    ${dict_result}=    Create Dictionary
    Set To Dictionary    ${dict_result}    is_product_no_discount    ${is_product_no_discount}
    Set To Dictionary    ${dict_result}    price    ${price}
    Set To Dictionary    ${dict_result}    special_price    ${special_price}
    Set To Dictionary    ${dict_result}    evaluate_save_price    ${evaluate_save_price}
    Set To Dictionary    ${dict_result}    evaluate_percent_discount    ${evaluate_percent_discount}
    [Return]    ${dict_result}

Get configurable option by product id
    [Documentation]    Return dictionary of configurable product
    ...                - *key*      : string, label configurable option, ex. shade, size
    ...                - *value*    : data dictionary, value matched with product id, ex. label, value_index
    [Arguments]    ${json_object}    ${product_id}
    ${product_id}=   Convert To Integer    ${product_id}
    ${list_label}=    JSONLibrary.Get Value From Json    ${json_object}    $..configurable_product_options[?(@.label)].label
    
    ${dict_product_configurable_options}=    Create Dictionary
    FOR   ${label}    IN    @{list_label}
        ${list_configurable_options_values}    JSONLibrary.Get Value From Json    ${json_object}    $..configurable_product_options[?(@.label=='${label}')].values[*]
        ${dict_configurable_options_values_matched_product_id}    Get configurable option values by product id    ${list_configurable_options_values}    ${product_id}

        ${result_label}=    Set Variable    ${dict_configurable_options_values_matched_product_id}[extension_attributes][label]
        ${result_value_index}=    Set Variable    ${dict_configurable_options_values_matched_product_id}[value_index]
        
        ${temp_dict}=    BuiltIn.Create Dictionary    label=${result_label}    value_index=${result_value_index}
        Set To Dictionary    ${dict_product_configurable_options}   ${label}    ${temp_dict}
    END
    [Return]    ${dict_product_configurable_options}

Get configurable option values by product id
    [Arguments]    ${list_configurable_options_values}    ${product_id}
    FOR    ${dict_configurable_options_values}    IN    @{list_configurable_options_values}
        ${list_products}=    Set Variable    ${dict_configurable_options_values}[extension_attributes][products]
        ${is_contains_product_id}=    Run Keyword And Return Status    List Should Contain Value    ${list_products}    ${product_id}
        Exit For Loop If    ${is_contains_product_id}==${True}
    END
    Should Be True    ${is_contains_product_id}    Product id : ${product_id}, did not match any value of configurable_product_options
    [Return]    ${dict_configurable_options_values}

Get simple product data
    [Documentation]    Return dictionary of simple product
    [Arguments]    ${response}
    ${json_object_simple_product}=    JSONLibrary.Get Value From Json    ${response}    $..product
    ${json_object_simple_product}=    Set Variable    ${json_object_simple_product}[0]
    ${simple_product_id}=    Set Variable    ${json_object_simple_product}[id]
    ${simple_product_sku}=    Set Variable    ${json_object_simple_product}[sku]
    ${simple_product_name}=    Set Variable    ${json_object_simple_product}[name]
    
    ${dict_result}=    BuiltIn.Create Dictionary 
    ...    json_object=${json_object_simple_product}
    ...    sku=${simple_product_sku}
    ...    id=${simple_product_id}
    ...    name=${simple_product_name}
    [Return]    ${dict_result}

Get display product overlays by product sku
    [Arguments]    ${json_object_by_product_sku}
    [Documentation]    Return dictionary of product overlays 
    ...                - *If product has* _*overlays*_ *and* _*cart price rule overlays*_   :   return cart_price_rule_overlays
    ...                - *key:*  is_display         : boolean
    ...                - *key:*  overlay_type       : string, product_overlay / cart_price_rule_overlay
    ...                - *key:*  overlay_image      : string, img
    ...                - *key:*  priority           : integer
    # cart_price_rule_overlays
    ${list_cart_price_rule_overlays_img}=    JSONLibrary.Get Value From Json    ${json_object_by_product_sku}    $..cart_price_rule_overlays[?(@.overlay_image)]
    ${len_list_cart_price_rule_overlays_img}=    Get Length    ${list_cart_price_rule_overlays_img}
    ${dict_result}=    Run Keyword And Return If    ${len_list_cart_price_rule_overlays_img} > ${0}    Get first display priority product cart price rule overlay    ${list_cart_price_rule_overlays_img}

    # product overlay 
    ${dict_overlay}=    JSONLibrary.Get Value From Json    ${json_object_by_product_sku}    $..overlay
    ${dict_overlay}=    Set Variable   ${dict_overlay}[0]
    ${is_dict_overlay_none}=    Run Keyword And Return Status    Should Be Equal As Strings    ${dict_overlay}    ${None}

    ${dict_result}=    Run Keyword If    not ${is_dict_overlay_none}    BuiltIn.Create Dictionary    is_display=${True}    overlay_type=product_overlay    overlay_image=${dict_overlay}[image]    priority=${dict_overlay}[status]
    ...    ELSE    BuiltIn.Create Dictionary    is_display=${False}    overlay_type=${None}    overlay_image=${None}    priority=${None}
    [Return]    ${dict_result}
