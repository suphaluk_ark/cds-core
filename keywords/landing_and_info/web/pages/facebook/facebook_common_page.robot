*** Keywords ***
Input facebook email
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_facebook_page.txt_facebook_email}    ${email}

Input facebook password
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_facebook_page.txt_facebook_password}    ${password}

Click login button
    CommonWebKeywords.Click Element    ${dict_facebook_page.btn_facebook_login}