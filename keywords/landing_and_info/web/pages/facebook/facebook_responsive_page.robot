*** Variables ***
&{dict_facebook_page}
...    txt_facebook_email=xpath=//input[@id='m_login_email']
...    txt_facebook_password=xpath=//input[@id='m_login_password']
...    btn_facebook_login=xpath=//button[@name='login']
...    btn_facebook_confirm=css=button[name='__CONFIRM__']

*** Keywords ***
Click confirm facebook new account button
    CommonWebKeywords.Click Element    ${dict_facebook_page.btn_facebook_confirm}
