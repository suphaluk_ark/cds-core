*** Variables ***
&{dict_register_page}
...    txt_login_email=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//*[@name="email"]
...    txt_login_password=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//*[@name='password']
...    btn_login=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//div[text()='${login_signup_dto.btn_login}']
...    btn_submit_login=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//button[@type='submit']
...    btn_register=xpath=//*[./*[text()='${login_signup_dto.lbl_register_for_special_promotion}']]/following-sibling::*//button[@type='submit']
