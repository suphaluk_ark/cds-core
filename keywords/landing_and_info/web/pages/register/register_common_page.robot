*** Keywords ***
Input login email
    [Arguments]    ${email}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_register_page.txt_login_email}    ${email}

Input login password
    [Arguments]    ${password}
    CommonWebKeywords.Input Text And Verify Input For Web Element    ${dict_register_page.txt_login_password}    ${password}

Click login button
    CommonWebKeywords.Scroll And Click Element    ${dict_register_page.btn_login}
    common_keywords.Wait Until Page Is Completely Loaded

Click submit login button
    CommonWebKeywords.Scroll And Click Element    ${dict_register_page.btn_submit_login}
    common_keywords.Wait Until Page Is Completely Loaded

Verify PDPA Checkbox is unselected as a defaut
    ${value}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dict_register_page.chkbox_pdpa}    ${common_style.background}
    Should Not Contain    ${value}    ${common_pdpd.checkbox_value}

Verify PDPA Checkbox is able to clickable
    SeleniumLibrary.Wait Until Element Is Visible    ${dict_register_page.chkbox_pdpa}
    CommonWebKeywords.Click Element Using Javascript    ${dict_register_page.chkbox_pdpa}
    ${value}=    CommonWebKeywords.Get Web Element CSS Property Value    ${dict_register_page.chkbox_pdpa}    ${common_style.background}
    Should Contain    ${value}    ${common_pdpd.checkbox_value}

Verify pada text message is dislay correctly
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_register_page}[txt_pdpa_privilage]
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_register_page}[txt_pdpa_company_name]
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_register_page}[txt_pdpa_company_partner_specified]
    SeleniumLibrary.Wait Until Page Contains Element    ${dict_register_page}[txt_pdpa_privacy]

Click login, register label
    header_common_fragment.Click login, register label

Click register now link
    header_common_fragment.Click register now link   

