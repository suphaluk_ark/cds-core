*** Variables ***
&{dict_register_page}
...    txt_login_email=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//*[@name="email"]
...    txt_login_password=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//*[@name='password']
...    btn_login=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//div[text()='${login_signup_dto.btn_login}']
...    btn_submit_login=xpath=//*[./*[text()='${login_signup_dto.lbl_registered_customer}']]/following-sibling::*//button[@type='submit']
...    btn_register=xpath=//*[./*[text()='${login_signup_dto.lbl_register_for_special_promotion}']]/following-sibling::*//button[@type='submit']
...    chkbox_pdpa=xpath=//*[@id="consent-data"]//preceding::div[1]
...    txt_pdpa_privilage=xpath=//*[@id="consent-data" and contains(.,"${register_page.msg_pdpa_receive_my_privileges}")]
...    txt_pdpa_company_name=xpath=//*[@id="company-name" and contains(.,"${company_name}")]
...    txt_pdpa_company_partner_specified=xpath=//*[contains(.,"${register_page.msg_pdpa_partners_specified}")]
...    txt_pdpa_privacy=xpath=//*[@id="consent-data"]//a[@to="/information/privacy-policy" and contains(.,"${register_page.msg_pdpa_privacy_policy}")]
