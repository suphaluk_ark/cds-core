*** Keywords ***
Login on header by email, password successfully
    [Arguments]    ${email}    ${password}    ${display_name}
    header_common_fragment.Click login, register label
    header_common_fragment.Input email    ${email}
    header_common_fragment.Input password    ${password}
    header_common_fragment.Click login button
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}

Login on register page by email, password
    [Arguments]    ${email}    ${password}    ${display_name}
    header_common_fragment.Click login, register label
    header_common_fragment.Click register now link
    register_common_page.Input login email    ${email}
    register_common_page.Input login password    ${password}
    register_common_page.Click submit login button
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}

Login on header by email, password unsuccessfully
    [Arguments]    ${email}    ${password}    ${error_message}
    header_common_fragment.Click login, register label
    header_common_fragment.Input email    ${email}
    header_common_fragment.Input password    ${password}
    header_common_fragment.Click login button
    login_desktop_keywords.Login on header - Verify Error Message When Login Failed    ${error_message}

Login via facebook 
    [Arguments]    ${email}    ${password}    ${display_name}
    header_common_fragment.Click login, register label
    header_common_fragment.Click login with facebook button
    common_keywords.Select new window
    facebook_common_page.Input facebook email    ${email}
    facebook_common_page.Input facebook password    ${password}
    facebook_common_page.Click login button
    common_keywords.Select main window
    loader_common_fragment.Wait until loader progress bar is not visible
    loader_common_fragment.Wait until page loader is not visible
    common_keywords.Wait until page is completely loaded
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}

Login on header - Email field Should Be Visible Error
    [Arguments]    ${expect_message}
    header_common_fragment.Email Field Display Error Message    ${expect_message}

Login on header - Password field Should Be Visible Error
    [Arguments]    ${expect_message}
    header_common_fragment.Password Field Display Error Message    ${expect_message}

Login on header - Verify Error Message When Login Failed
    [Arguments]    ${expect_message}
    header_common_fragment.Verify Error Message After Login Failed    ${expect_message}

Verify Display Name With Welcome Message Should Be Displayed
    CommonWebKeywords.Verify Web Elements Are Visible    ${dict_header_fragment.lbl_welcome}

Log Out Success
    [Arguments]    ${display_name}
    header_common_fragment.Click on account name label    ${display_name}
    header_common_fragment.Click on log out link
    header_common_fragment.Verify register label should be displayed

Login via falcon successfully
    [Arguments]    ${email}    ${password}    ${display_name}
    ${token}=    account_management_falcon.Get customer login token    ${email}    ${password}
    Add Cookie    user_token    ${token}
    Reload Page
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}
