*** Keywords ***
Login on header by email, password successfully
    [Arguments]    ${email}    ${password}    ${display_name}
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Click login, register label
    header_common_fragment.Input email    ${email}
    header_common_fragment.Input password    ${password}
    header_common_fragment.Click login button
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}
    header_responsive_fragment.Click x button on hamburger menu

Login on register page by email, password
    [Arguments]    ${email}    ${password}    ${display_name}
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Click login, register label
    header_common_fragment.Click register now link
    register_common_page.Click login button
    register_common_page.Input login email    ${email}
    register_common_page.Input login password    ${password}
    register_common_page.Click submit login button
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}
    header_responsive_fragment.Click x button on hamburger menu

Login on header by email, password unsuccessfully
    [Arguments]    ${email}=${email}    ${password}=${password}    ${error_message}=${error_message}
    header_common_fragment.Input email    ${email}
    header_common_fragment.Input password    ${password}
    header_common_fragment.Click login button
    login_responsive_keywords.Login on header - Verify Error Message When Login Failed    ${error_message}

Login on header - Email field Should Be Visible Error
    [Arguments]    ${expect_message}
    header_responsive_fragment.Email Field Display Error Message    ${expect_message}

Login on header - Password field Should Be Visible Error
    [Arguments]    ${expect_message}
    header_responsive_fragment.Password field Should Be Visible Error    ${expect_message}

Login on header - Verify Error Message When Login Failed
    [Arguments]    ${expect_message}
    header_responsive_fragment.Verify Error Message After Login Failed    ${expect_message}

Login on header - Verify Display Name With Welcome Message Should Be Displayed
    header_responsive_fragment.Verify display name with welcome message should be displayed

Login via facebook 
    [Arguments]    ${email}    ${password}    ${display_name}
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Click login, register label
    header_common_fragment.Click login with facebook button
    common_keywords.Select new window
    facebook_common_page.Input facebook email    ${email}
    facebook_common_page.Input facebook password    ${password}
    facebook_common_page.Click login button
    Run Keyword And Ignore Error    facebook_responsive_page.Click confirm facebook new account button 
    common_keywords.Select main window
    loader_common_fragment.Wait until loader progress bar is not visible
    loader_common_fragment.Wait until page loader is not visible
    common_keywords.Wait until page is completely loaded
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}
    header_responsive_fragment.Click x button on hamburger menu

Login Keywords - Sign Out Success
    header_responsive_fragment.Click Hamburger Menu
    header_responsive_fragment.Click on Hi First Name label
    header_responsive_fragment.Click on Sign Out
    header_common_fragment.Verify homepage page should be displayed

Navigate to Login page
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Click login, register label

Login via falcon successfully
    [Arguments]    ${email}    ${password}    ${display_name}
    ${token}=    account_management_falcon.Get customer login token    ${email}    ${password}
    Add Cookie    user_token    ${token}
    Reload Page
    header_responsive_fragment.Click Hamburger Menu
    header_common_fragment.Verify display name should be displayed correctly    ${display_name}
    header_responsive_fragment.Click x button on hamburger menu
