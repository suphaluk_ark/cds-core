*** Keywords ***
Login successfully
    [Arguments]    ${dict_membership}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    home_common_page.Click user img
    user_profile_common_page.Click login button
    # login_ios_keywords.Retry login and Verify    ${dict_membership}
    login_common_page.Input text email    ${dict_membership.email}
    common_ios_mobile_app_keywords.Click keyboard done
    login_common_page.Input text password    ${dict_membership.pwd}
    common_ios_mobile_app_keywords.Click keyboard done
    login_common_page.Click lbl email
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    login_common_page.Click login button
    ...    AND    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${dict_membership.first_name}    ${dict_membership.last_name}
    # click x btn
    user_profile_common_page.Click x button

Retry login and Verify
    [Arguments]    ${dict_membership}
    BuiltIn.Wait Until Keyword Succeeds    3 x    1 sec    BuiltIn.Run Keywords    
    ...    login_common_page.Input text email    ${dict_membership.email}
    ...    AND    login_common_page.Input text password    ${dict_membership.pwd}
    # click lbl email for hide keyboard for ios
    ...    AND    login_common_page.Click lbl email
    ...    AND    login_common_page.Click login button
    ...    AND    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${dict_membership.first_name}    ${dict_membership.last_name}

Login successfully with facebook
    [Arguments]    ${dict_membership}
    home_common_page.Click user img
    user_profile_common_page.Click login button
    login_common_page.Click login with facebook button
    login_common_page.Click english language
    login_common_page.Input text email facebook    ${dict_membership.email}
    login_common_page.Input text password facebook    ${dict_membership.pwd}
    login_common_page.Click submit login facebook button
    user_profile_ios_page.Verify 'firstname and lastname' displays correctly    ${dict_membership.first_name}    ${dict_membership.last_name}
    user_profile_common_page.Click x button