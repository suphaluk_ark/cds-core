*** Keywords ***
Login successfully
    [Arguments]    ${dict_membership}
    home_common_page.Click user img
    user_profile_common_page.Click login button
    login_common_page.Input text email    ${dict_membership.email}
    login_common_page.Input text password    ${dict_membership.pwd}
    login_common_page.Click login button
    user_profile_android_page.Verify 'firstname and lastname' displays correctly    ${dict_membership.first_name}    ${dict_membership.last_name}
    # click x btn
    user_profile_common_page.Click x button

Login successfully with facebook
    [Arguments]    ${dict_membership}
    home_common_page.Click user img
    user_profile_common_page.Click login button
    login_common_page.Click login with facebook button
    login_common_page.Input text email facebook    ${dict_membership.email}
    login_common_page.Input text password facebook    ${dict_membership.pwd}
    login_common_page.Click submit login facebook button
    user_profile_android_page.Verify 'firstname and lastname' displays correctly    ${dict_membership.first_name}    ${dict_membership.last_name}
    user_profile_common_page.Click x button