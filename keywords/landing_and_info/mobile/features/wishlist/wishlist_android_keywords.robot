*** Keywords ***
Add wishlist pdp page
    Wait Until Keyword Succeeds    3x    10s    Run Keywords    product_pdp_android_page.Click wishlist button
    ...    AND    product_pdp_android_page.Check wishlist button is clicked

Remove wishlist pdp page
    product_pdp_android_page.Click heart icon to remove wishlist item

Add wishlist cart page
    product_pdp_common_page.Wait until click add to cart button with product successfully
    shopping_cart_common_page.Click Add to wishlist
    shopping_cart_common_page.Click icon back page

Go to back page
    product_pdp_common_page.Click icon back page
    home_android_page.Click icon back homePage
    home_android_page.Click icon back homePage

Go to homePage
    wishlist_common_page.Click icon back page
    wishlist_common_page.Click icon back page

Verify wishlist is visible
    home_common_page.Click user img
    user_profile_common_page.Click menu wishlist
    wishlist_common_page.Verify product name is visible

Verify wishlist is not visible
    home_common_page.Click user img
    user_profile_common_page.Click menu wishlist
    wishlist_common_page.Verify product name is not visible

Remove wishlist item on wishlist page
    [Arguments]    ${product_sku}
    checkout_android_keywords.Search product by product sku    ${product_sku}
    wishlist_android_keywords.Add wishlist pdp page
    wishlist_android_keywords.Go to back page
    wishlist_android_keywords.Verify wishlist is visible
    wishlist_common_page.Click heart icon
    wishlist_common_page.Verify product name is not visible

Add wishlist item on pdp page
    [Arguments]    ${product_sku}
    checkout_android_keywords.Search product by product sku    ${product_sku}
    wishlist_android_keywords.Add wishlist pdp page
    wishlist_android_keywords.Go to back page
    user_profile_common_page.Click x button
    wishlist_android_keywords.Verify wishlist is visible

Add wishlist item on cart page
    [Arguments]    ${product_sku}
    checkout_android_keywords.Search product by product sku    ${product_sku}
    wishlist_android_keywords.Add wishlist cart page
    wishlist_android_keywords.Go to back page
    wishlist_android_keywords.Verify wishlist is visible

Remove wishlist item on pdp page
    [Arguments]    ${product_sku}
    common_android_mobile_app_keywords.Click back button
    user_profile_common_page.Click x button
    checkout_android_keywords.Search product by product sku    ${product_sku}
    wishlist_android_keywords.Remove wishlist pdp page
    wishlist_android_keywords.Go to back page
    home_android_page.Click icon back homepage
    wishlist_android_keywords.Verify wishlist is not visible

Add product to cart on wishlist page
    [Arguments]    ${product_sku}
    Wait Until Keyword Succeeds    3x    10s    checkout_android_keywords.Search product by product sku    ${product_sku}
    wishlist_android_keywords.Add wishlist pdp page
    wishlist_android_keywords.Go to back page
    wishlist_android_keywords.Verify wishlist is visible
    wishlist_common_page.Click add to bag
    common_mobile_app_keywords.Click icon cart