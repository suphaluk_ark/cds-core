*** Keywords ***
Change password sucessfully
    [Arguments]    ${pwd}    ${new_pwd}    ${confirm_pwd}
    home_common_page.Click user img
    user_profile_common_page.Click change my password tab
    change_password_common_page.Input current password    ${pwd}
    change_password_common_page.Input new password    ${new_pwd}
    change_password_common_page.Input confirm new password    ${confirm_pwd}
    change_password_common_page.Click confirm change button