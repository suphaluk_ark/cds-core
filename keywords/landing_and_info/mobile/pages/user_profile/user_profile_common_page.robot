*** Keywords ***
Click login button
    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[btn_login]

Click register button
    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[btn_register]

Click x button
    Wait Until Keyword Succeeds    3 x    1 sec    Run Keywords    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[btn_x]    AND    AppiumLibrary.Wait Until Page Contains Element    ${dictHomePage}[img_user_profile]

Click on customer name
    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[lbl_first_name_last_name]    timeout=${GLOBALtimeout}

Scroll to save button
    CommonMobileKeywords.Scroll Up Until Element Is Found    ${dict_user_profile_page}[view_scrollable_edit_profile]    ${dict_user_profile_page}[btn_save]

Click change my password tab
    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[lbl_change_my_password]

Click menu wishlist
    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[txt_menu_wishlist]    timeout=${GLOBALtimeout}