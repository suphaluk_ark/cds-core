*** Variables ***
&{dict_user_profile_page}
...    view_scrollable_profile=chain=**/XCUIElementTypeTable   #scrollable element
...    view_scrollable_edit_profile=chain=**/XCUIElementTypeScrollView   #scrollable element
...    btn_login=chain=**/*[`name=="${login_dto.btn_login}"`]
...    btn_register=chain=**/XCUIElementTypeButton[`label == "${register_dto.lbl_register}"`]
...    lbl_first_name_last_name=chain=**/XCUIElementTypeStaticText[`label == "{full_name}"`]
...    btn_x=chain=**/XCUIElementTypeButton[`name == 'ic close'`]
...    txt_customer_first_name=chain=**/XCUIElementTypeTextField[3]
...    txt_customer_last_name=chain=**/XCUIElementTypeTextField[4]
...    txt_customer_email=chain=**/XCUIElementTypeTextField[2]
...    txt_customer_phone=chain=**/XCUIElementTypeTextField[5]
...    txt_customer_birthday=chain=**/XCUIElementTypeTextField[1]
...    txt_customer_gender=chain=**/XCUIElementTypeButton[`value == "1"`]    #1=active
...    btn_save=chain=**/XCUIElementTypeButton[3]
...    lbl_change_my_password=id=${my_account_page_dto.txt_change_my_password_placeholder}
...    txt_menu_wishlist=chain=**/XCUIElementTypeStaticText[$label == '${wishlist_page_dto.lbl_menu_wishlist}'$]

*** Keywords ***
Verify 'firstname and lastname' displays correctly
    [Arguments]    ${firstname}    ${lastname}
    ${locator}=    CommonKeywords.Format Text    ${dict_user_profile_page}[lbl_first_name_last_name]    full_name=${firstname} ${lastname}
    AppiumLibrary.Wait Until Element Is Visible    ${locator}    timeout=${GLOBALTIMEOUT}

Click on customer name
    [Arguments]    ${firstname}    ${lastname}
    ${locator}=    CommonKeywords.Format Text    ${dict_user_profile_page}[lbl_first_name_last_name]    full_name=${firstname} ${lastname}
    AppiumLibrary.Wait Until Page Contains Element    ${locator}
    CommonMobileKeywords.Click Element    ${locator}