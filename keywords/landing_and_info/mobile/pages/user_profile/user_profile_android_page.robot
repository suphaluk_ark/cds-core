*** Variables ***
&{dict_user_profile_page}
...    view_scrollable_profile=android=UiSelector().resourceIdMatches(".*id/recyclerView$")
...    view_scrollable_edit_profile=android=UiSelector().resourceIdMatches(".*id/nestedScrollView$")
...    btn_login=android=UiSelector().resourceIdMatches(".*id/buttonLogin$")
...    btn_register=android=UiSelector().resourceIdMatches(".*id/buttonRegister$")
...    lbl_first_name_last_name=android=UiSelector().resourceIdMatches(".*id/buttonName$")
...    btn_x=android=UiSelector().className("android.widget.ImageButton")
...    lbl_change_my_password=android=UiSelector().text("${my_account_page_dto.txt_change_my_password_placeholder}")
...    txt_customer_first_name=android=UiSelector().text("{first_name}")
...    txt_customer_last_name=android=UiSelector().text("{last_name}")
...    txt_customer_email=android=UiSelector().text("{email}")
...    txt_customer_phone=android=UiSelector().text("{phone}")
...    txt_customer_birthday=android=UiSelector().text("{date_of_birth}")
...    txt_customer_gender=android=UiSelector().resourceIdMatches(".*id/buttonMale$").text("${my_account_page_dto.btn_male}")
...    btn_save=android=UiSelector().resourceIdMatches(".*id/buttonSave$")
...    txt_menu_wishlist=android=UiSelector().text("${login_dto.lbl_wishlist}")

*** Keywords ***
Verify 'firstname and lastname' displays correctly
    [Arguments]    ${firstname}    ${lastname}
    AppiumLibrary.Wait Until Page Contains Element    ${dict_user_profile_page}[lbl_first_name_last_name]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Element Text Should Be    ${dict_user_profile_page}[lbl_first_name_last_name]    ${firstname} ${lastname}

Click on customer name
    AppiumLibrary.Wait Until Page Contains Element    ${dict_user_profile_page}[lbl_first_name_last_name]
    CommonMobileKeywords.Click Element    ${dict_user_profile_page}[lbl_first_name_last_name]