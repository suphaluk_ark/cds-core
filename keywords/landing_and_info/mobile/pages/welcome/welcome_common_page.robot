*** Keywords ***
Select preferred language and skip button
    [Arguments]    ${language}
    Run Keyword And Ignore Error    CommonMobileKeywords.Click Element    ${language}
    Run Keyword And Ignore Error    CommonMobileKeywords.Click Element    ${dictWelcome}[btn_skip]
    home_common_page.Verify that home screen is shown