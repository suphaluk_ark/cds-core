*** Keywords ***
Input text password
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dict_login_page}[txt_password]
    AppiumLibrary.Clear Text    ${dict_login_page}[txt_password]
    # AppiumLibrary.Input Text    ${dict_login_page}[txt_password]    ${pwd}
    AppiumLibrary.Input Password    ${dict_login_page}[txt_password]    ${pwd}

Input text email
    [Arguments]    ${user}
    AppiumLibrary.Wait Until Element Is Visible    ${dict_login_page}[txt_email]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Clear Text    ${dict_login_page}[txt_email]
    AppiumLibrary.Input Text    ${dict_login_page}[txt_email]    ${user}

Click login button
    CommonMobileKeywords.Click Element    ${dict_login_page}[btn_login]

Click lbl email
    CommonMobileKeywords.Click Element    ${dict_login_page}[lbl_email]

Click login with facebook button
    CommonMobileKeywords.Click Element    ${dict_login_page}[btn_login_with_facebook]

Click english language
    AppiumLibrary.Wait Until Element Is Visible    ${dict_login_page}[btn_english_language]    timeout=${GLOBALTIMEOUT}
    CommonMobileKeywords.Click Element    ${dict_login_page}[btn_english_language]

Input text email facebook
    [Arguments]    ${user}
    AppiumLibrary.Wait Until Element Is Visible    ${dict_login_page}[txt_email_facebook]    timeout=${GLOBALTIMEOUT}
    AppiumLibrary.Input Text    ${dict_login_page}[txt_email_facebook]    ${user}

Input text password facebook
    [Arguments]    ${pwd}
    AppiumLibrary.Wait Until Element Is Visible    ${dict_login_page}[txt_password_facebook]
    AppiumLibrary.Input Password    ${dict_login_page}[txt_password_facebook]    ${pwd}

Click submit login facebook button
    CommonMobileKeywords.Click Element    ${dict_login_page}[btn_submit_login_facebook]