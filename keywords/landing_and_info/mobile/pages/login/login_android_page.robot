*** Variables ***
&{dict_login_page}
...    btn_login=android=UiSelector().resourceIdMatches(".*id/buttonLogin$")
...    txt_email=android=UiSelector().resourceIdMatches(".*id/editTextEmail$")
...    txt_password=android=UiSelector().resourceIdMatches(".*id/editTextPassword$")
...    btn_login_with_facebook=android=UiSelector().resourceIdMatches(".*id/buttonFacebook$")
...    txt_email_facebook=android=UiSelector().resourceId("m_login_email")
...    txt_password_facebook=android=UiSelector().resourceId("m_login_password")
...    btn_submit_login_facebook=android=UiSelector().textMatches("^(เข้าสู่ระบบ|Login|Đăng nhập)$")
