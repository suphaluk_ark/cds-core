*** Settings ***
# cds-core
Resource    ${CURDIR}/library.robot
Variables   ${CURDIR}/../resources/common_variables.yaml
Variables   ${CURDIR}/../resources/test_configs.yaml
Variables   ${CURDIR}/../resources/configs/${BU.lower()}/${ENV.lower()}/env_configs.yaml

Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/test_data.yaml
Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/products_data.yaml
Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/customers_data.yaml
Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/cart_price_rules.yaml
Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/experiment_id.yaml

Variables   ${CURDIR}/../translation/${BU.lower()}/web/translation_${LANGUAGE.lower()}.yaml

# qa-common
Resource    ${CURDIR}/../keywords/qa-common/CommonKeywords.robot
Resource    ${CURDIR}/../keywords/qa-common/CommonWebKeywords.robot
Resource    ${CURDIR}/../keywords/qa-common/api/kibana/sms_details.robot
Resource    ${CURDIR}/../keywords/qa-common/fms/fms_db.robot

# qa-magento
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/order_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/search_sales_order.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/get_order_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/etc/set_product_file.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/force_shipment.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/payment_validated.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/address_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/authentication.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/banner_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/cart.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/categories_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/checkout.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/cmsblock_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/consent.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/customer_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/customers.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/product.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/promotion_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/search.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/store_location.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/wishlist.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/auto_display_overlay.robot

# common
Resource    ${CURDIR}/../keywords/common_cds_rbs/calculation.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/prepare.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/web/common_keywords.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/web/common_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/qa-common/api/kibana/sms_details.robot
Resource    ${CURDIR}/../keywords/qa-common/api/kibana/kibana_api.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/order_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/search_sales_order.robot

# fragments
Resource    ${CURDIR}/../keywords/fragments/web/header/header_common_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/header/header_${TEST_PLATFORM.lower()}_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/loader/loader_common_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/loader/loader_${TEST_PLATFORM.lower()}_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/product_plp/product_plp_common_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/product_plp/product_plp_${TEST_PLATFORM.lower()}_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/web_authentication/web_authentication_common_page.robot
Resource    ${CURDIR}/../keywords/fragments/web/web_authentication/web_authentication_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/fragments/web/filter_sort_plp/filter_sort_plp_common_fragment.robot
Resource    ${CURDIR}/../keywords/fragments/web/filter_sort_plp/filter_sort_plp_${TEST_PLATFORM.lower()}_fragment.robot

# keywords each tribe: landing and info
Resource    ${CURDIR}/../keywords/landing_and_info/web/features/login/login_${TEST_PLATFORM.lower()}_keywords.robot

Resource    ${CURDIR}/../keywords/landing_and_info/web/pages/facebook/facebook_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/web/pages/facebook/facebook_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/web/pages/home/home_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/web/pages/home/home_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/web/pages/register/register_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/web/pages/register/register_${TEST_PLATFORM.lower()}_page.robot



# keywords each tribe: discovery
Resource    ${CURDIR}/../keywords/discovery/web/features/search/search_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/discovery/web/features/verify_product/verify_product_plp_${TEST_PLATFORM.lower()}_keywords.robot

Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/normal_boutique_brand_plp_page/normal_boutique_brand_plp_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/normal_boutique_brand_plp_page/normal_boutique_brand_plp_${TEST_PLATFORM.lower()}_page.robot

Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/category_plp_page/category_plp_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/category_plp_page/category_plp_${TEST_PLATFORM.lower()}_page.robot

Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/full_boutique_brand_plp_page/full_boutique_brand_plp_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/full_boutique_brand_plp_page/full_boutique_brand_plp_${TEST_PLATFORM.lower()}_page.robot

Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/search_result_plp_page/search_result_plp_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/search_result_plp_page/search_result_plp_${TEST_PLATFORM.lower()}_page.robot

Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/standard_boutique_brand_plp_page/standard_boutique_brand_plp_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/web/pages/plp_page/standard_boutique_brand_plp_page/standard_boutique_brand_plp_${TEST_PLATFORM.lower()}_page.robot

# keywords each tribe: consideration
Resource    ${CURDIR}/../keywords/consideration/web/pages/pdp/pdp_common_page.robot
Resource    ${CURDIR}/../keywords/consideration/web/pages/pdp/pdp_${TEST_PLATFORM.lower()}_page.robot

# keywords each tribe: cart and checkout
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/features/delivery/delivery_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/features/checkout/checkout_${TEST_PLATFORM.lower()}_keyword.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/features/delivery/delivery_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/features/payment/payment_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/features/shopping_bag/shopping_bag_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/features/thank_you/thank_you_${TEST_PLATFORM.lower()}_keywords.robot

Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/2c2p/2c2p_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/2c2p/2c2p_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/bank_transfer/bank_transfer_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/bank_transfer/bank_transfer_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/delivery/delivery_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/delivery/delivery_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/payment/payment_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/payment/payment_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/shopping_bag/shopping_bag_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/shopping_bag/shopping_bag_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/thank_you/thank_you_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/web/pages/thank_you/thank_you_${TEST_PLATFORM.lower()}_page.robot

# keywords each tribe: post purchase
Resource    ${CURDIR}/../keywords/post_purchase/api/order_status.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/order_created.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/mcom_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/mdc_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/wms_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/picking_tool_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/fms_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/features/mcom_keywords.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/features/mdc_keywords.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/features/fms_keywords.robot

Resource    ${CURDIR}/../keywords/post_purchase/web/pages/order_details/order_details_${TEST_PLATFORM.lower()}_page.robot

*** Variables ***
${mdc_access_token}    %{MDC_ACCESS_TOKEN}
${key_mdc}    %{MDC_ACCESS_TOKEN}
