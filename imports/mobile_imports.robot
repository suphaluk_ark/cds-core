*** Settings ***
# cds-core
Resource     ${CURDIR}/library.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/mobile/common_mobile_app_keywords.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/mobile/common_${TEST_PLATFORM.lower()}_mobile_app_keywords.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/calculation.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/prepare.robot
Variables    ${CURDIR}/../resources/common_variables.yaml
Variables    ${CURDIR}/../resources/test_configs.yaml
Variables    ${CURDIR}/../resources/configs/${BU.lower()}/${ENV.lower()}/env_configs.yaml
Variables    ${CURDIR}/../resources/mobile_devices.yaml

Variables    ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/test_data.yaml
Variables    ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/products_data.yaml
Variables    ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/customers_data.yaml
Variables    ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/products_stock.yaml

Variables    ${CURDIR}/../translation/${BU.lower()}/mobile/translation_${LANGUAGE}.yaml

# submodules
Resource    ${CURDIR}/../keywords/qa-common/CommonKeywords.robot
Resource    ${CURDIR}/../keywords/qa-common/CommonMobileKeywords.robot
Resource    ${CURDIR}/../keywords/qa-common/pCloudy.robot
Resource    ${CURDIR}/../keywords/qa-common/api/kibana/sms_details.robot
Resource    ${CURDIR}/../keywords/qa-common/api/kibana/kibana_api.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/order_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/search_sales_order.robot

# keywords each tribe: landing and info
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/features/login/login_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/features/change_password/change_password_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/features/logout/logout_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/features/welcome/welcome_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/features/wishlist/wishlist_${TEST_PLATFORM.lower()}_keywords.robot

Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/change_password/change_password_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/change_password/change_password_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/login/login_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/login/login_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/user_profile/user_profile_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/user_profile/user_profile_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/welcome/welcome_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/welcome/welcome_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/wishlist/wishlist_common_page.robot
Resource    ${CURDIR}/../keywords/landing_and_info/mobile/pages/wishlist/wishlist_${TEST_PLATFORM.lower()}_page.robot

# keywords cart and checkout
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/checkout/checkout_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/consent/consent_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/delivery/delivery_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/my_save_cards/my_save_cards_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/payment/payment_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/promotion/promotion_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/register/register_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/T1/T1_${TEST_PLATFORM.lower()}_keywords.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/product_pdp/product_pdp_${TEST_PLATFORM.lower()}_keywords.robot

Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/delivery/delivery_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/delivery/delivery_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/my_save_cards/my_save_cards_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/my_save_cards/my_save_cards_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/payment/payment_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/payment/payment_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/product_pdp/product_pdp_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/product_pdp/product_pdp_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/product_plp/product_plp_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/product_plp/product_plp_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/register/register_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/register/register_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/shopping_cart/shopping_cart_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/shopping_cart/shopping_cart_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/checkout_completed/checkout_completed_common_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/pages/checkout_completed/checkout_completed_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/cart_and_checkout/mobile/features/cart/cart_${TEST_PLATFORM.lower()}_keywords.robot


# keywords discovery
Resource    ${CURDIR}/../keywords/discovery/mobile/features/search_filter/search_filter_${TEST_PLATFORM.lower()}_keywords.robot

Resource    ${CURDIR}/../keywords/discovery/mobile/pages/home/home_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/mobile/pages/home/home_${TEST_PLATFORM.lower()}_page.robot
Resource    ${CURDIR}/../keywords/discovery/mobile/pages/search_product/search_product_common_page.robot
Resource    ${CURDIR}/../keywords/discovery/mobile/pages/search_product/search_product_${TEST_PLATFORM.lower()}_page.robot

# post purchase
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/mcom_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/mdc_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/wms_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/picking_tool_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/requests/fms_request.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/features/mcom_keywords.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/features/mdc_keywords.robot
Resource    ${CURDIR}/../keywords/post_purchase/api/features/fms_keywords.robot