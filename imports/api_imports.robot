*** Settings ***
# cds-core
Resource    ${CURDIR}/library.robot
Variables   ${CURDIR}/../resources/common_variables.yaml
Variables   ${CURDIR}/../resources/test_configs.yaml
Variables   ${CURDIR}/../resources/configs/${BU.lower()}/${ENV.lower()}/env_configs.yaml

Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/test_data.yaml
Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/products_data.yaml
Variables   ${CURDIR}/../testdata/${BU.lower()}/${ENV.lower()}/products_stock.yaml

# cds-core keyword
Resource    ${CURDIR}/../keywords/api/account_management_api.robot
Resource    ${CURDIR}/../keywords/api/shopping_cart_api.robot

# qa-common
Resource    ${CURDIR}/../keywords/qa-common/CommonKeywords.robot
Resource    ${CURDIR}/../keywords/qa-common/api/kibana/sms_details.robot
Resource    ${CURDIR}/../keywords/qa-common/fms/fms_db.robot

# qa-magento
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/order_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/search_sales_order.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/get_order_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/etc/set_product_file.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/force_shipment.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/payment_validated.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/address_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/authentication.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/banner_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/cart.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/categories_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/checkout.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/cmsblock_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/consent.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/customer_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/customers.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/product.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/promotion_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/search.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/store_location.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/wishlist.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/auto_display_overlay.robot

# common
Resource    ${CURDIR}/../keywords/common_cds_rbs/calculation.robot
Resource    ${CURDIR}/../keywords/common_cds_rbs/prepare.robot

# falcon
Resource    ${CURDIR}/../keywords/falcon/api/account_management/account_management_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/account_management/address_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/shopping_cart_page/shopping_cart_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/homepage/homepage_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/payment_page/app_payment_page_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/pdp/pdp_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/plp/app_search_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/plp/search_falcon.robot
Resource    ${CURDIR}/../keywords/falcon/api/shopping_cart_page/app_shoppinp_cart_falcon.robot

#api
Resource    ${CURDIR}/../keywords/discovery/api/plp_api.robot
Resource    ${CURDIR}/../keywords/discovery/api/product_api.robot

*** Variables ***
${mdc_access_token}    %{MDC_ACCESS_TOKEN}
${key_mdc}    %{MDC_ACCESS_TOKEN}
