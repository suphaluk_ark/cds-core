*** Settings ***
# cds-core
Resource    ${CURDIR}/library.robot
Variables   ${CURDIR}/../resources/common_variables.yaml
Variables   ${CURDIR}/../resources/test_configs.yaml
Variables   ${CURDIR}/../resources/configs/${BU.lower()}/${ENV.lower()}/env_configs.yaml

# qa-magento
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/order_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/search_sales_order.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/get_order_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/etc/set_product_file.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/force_shipment.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mcom/payment_validated.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/address_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/authentication.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/banner_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/cart.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/categories_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/checkout.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/cmsblock_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/consent.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/customer_details.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/customers.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/product.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/promotion_keywords.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/search.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/store_location.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/wishlist.robot
Resource    ${CURDIR}/../keywords/qa-magento/api/mdc/auto_display_overlay.robot

*** Variables ***
${mdc_access_token}    %{MDC_ACCESS_TOKEN}
${key_mdc}    %{MDC_ACCESS_TOKEN}
